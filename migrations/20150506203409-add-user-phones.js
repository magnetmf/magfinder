"use strict";

var _ = require('lodash');
var Q = require('q');
var multiline = require('multiline');
var dbm = dbm || require('db-migrate');
var type = dbm.dataType;

function addStoredProcedure(options){
  var deferred = Q.defer();

  var query = multiline(function(){ /*
    CREATE OR REPLACE FUNCTION set_phone_verification_code()
    RETURNS TRIGGER AS
    $body$
      BEGIN
        IF
           TG_OP = 'INSERT' OR
           NEW.phone != OLD.phone OR
           NEW.verification_code != OLD.verification_code
        THEN

          -- verification code has the format timestamp__randominteger
          NEW.verification_code = ROUND(DATE_PART('epoch', NOW())) || '__' ||  ((900000 * RANDOM() + 100000)::int)::text;

          -- reset verified_at
          NEW.verified_at = NULL;

        END IF;

        RETURN NEW;
      END;
    $body$
    LANGUAGE 'plpgsql';
  */});

  options.db.connection.query(query, function(err){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(); }
  });

  return deferred.promise;
}

function addTrigger(options){
  var deferred = Q.defer();

  var query = multiline(function(){/*
    CREATE TRIGGER set_verification_code BEFORE INSERT OR UPDATE
    ON user_phones FOR EACH ROW
      EXECUTE PROCEDURE set_phone_verification_code();
  */});

  options.db.connection.query(query, function(err){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(); }
  });

  return deferred.promise;
}

function createUserPhonesTable(options){
  var deferred = Q.defer();

  options.db.createTable('user_phones', {
    user_id: {
      type: 'int',
      foreignKey: {
        name: 'user_phones_user_id_fk',
        table: 'users',
        mapping: 'id',
        rules: { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
      },
      primaryKey: true
    },
    phone: {
      type: 'text'
    },
    verification_code: {
      type: 'text',
      notNull: true
    },
    verified_at: {
      type: 'datetime'
    },
  }, function(err){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(); }
  });

  return deferred.promise;
}

exports.up = function(db, callback) {
  createUserPhonesTable({ db: db })
    .then(_.partial(addStoredProcedure, { db: db }))
    .then(_.partial(addTrigger, { db: db }))
    .then(function(){ callback(); })
    .fail(function(err){ callback(err); });
};

exports.down = function(db, callback) {
  db.dropTable('user_phones', callback);
};
