ALTER TABLE communication_preferences
DROP CONSTRAINT communication_preferences_user_id_fkey;

ALTER TABLE favorite_magazines
DROP CONSTRAINT favorite_magazines_user_id_fkey;

ALTER TABLE feedback
DROP CONSTRAINT feedback_user_id_fkey;

ALTER TABLE social_accounts
DROP CONSTRAINT social_accounts_user_id_fkey;

ALTER TABLE user_phones
DROP CONSTRAINT user_phones_user_id_fkey;

ALTER TABLE user_emails
DROP CONSTRAINT user_emails_uniq_email;

ALTER TABLE user_emails
DROP CONSTRAINT user_emails_user_id_fkey;

ALTER TABLE user_emails
DROP CONSTRAINT user_emails_pkey;

ALTER TABLE user_emails
ADD PRIMARY KEY (user_id);
