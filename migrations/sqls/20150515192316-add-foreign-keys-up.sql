-- Db-migrate has a foreign key clause, but postgres is not currently supported,
-- so now I have to create foreign keys manually
-- https://github.com/db-migrate/node-db-migrate/issues/237

ALTER TABLE communication_preferences
ADD FOREIGN KEY (user_id) REFERENCES users(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

ALTER TABLE favorite_magazines
ADD FOREIGN KEY (user_id) REFERENCES users(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

ALTER TABLE feedback
ADD FOREIGN KEY (user_id) REFERENCES users(id)
  ON UPDATE CASCADE
  ON DELETE SET NULL;

ALTER TABLE social_accounts
ADD FOREIGN KEY (user_id) REFERENCES users(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

ALTER TABLE user_phones
ADD FOREIGN KEY (user_id) REFERENCES users(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

ALTER TABLE user_emails
DROP CONSTRAINT user_emails_pkey;

ALTER TABLE user_emails
ADD PRIMARY KEY (email);

ALTER TABLE user_emails
ADD FOREIGN KEY (user_id) REFERENCES users(id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

ALTER TABLE user_emails
ADD CONSTRAINT user_emails_uniq_email UNIQUE (user_id);
