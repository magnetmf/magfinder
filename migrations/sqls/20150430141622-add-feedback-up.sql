CREATE TYPE feedback_type AS ENUM ('invalid_info', 'magazine_request');

CREATE TABLE feedback (
  id              SERIAL PRIMARY KEY,
  feedback_type   feedback_type NOT NULL,
  magazine_title  text NOT NULL,
  store_name      text NOT NULL,
  comments        text
);
