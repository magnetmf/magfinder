"use strict";

var _ = require('lodash');
var dbm = global.dbm || require('db-migrate');
var type = dbm.dataType;
var Q = require('q');

function addUserID(db){
  var deferred = Q.defer();

  db.addColumn('feedback', 'user_id', {
    type: 'int',
    foreignKey: {
      name: 'feedback_user_id_fk',
      table: 'users',
      mapping: 'id',
      rules: { onDelete: 'SET NULL', onUpdate: 'SET NULL' }
    },
  }, function(err){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(); }
  });

  return deferred.promise;
}

function addLatitude(db){
  var deferred = Q.defer();

  db.addColumn('feedback', 'latitude', { type: 'decimal' },
  function(err){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(); }
  });

  return deferred.promise;
}

function addLongitude(db){
  var deferred = Q.defer();

  db.addColumn('feedback', 'longitude', { type: 'decimal' },
  function(err){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(); }
  });

  return deferred.promise;
}

function removeColumn(db, columnName){
  var deferred = Q.defer();

  db.removeColumn('feedback', columnName,
  function(err){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(); }
  });

  return deferred.promise;
}

exports.up = function(db, callback) {
  addUserID(db)
    .then(_.partial(addLatitude, db))
    .then(_.partial(addLongitude, db))
    .then(callback)
    .fail(function(err){
      console.log(err);
      callback(err);
    });
};

exports.down = function(db, callback) {
  removeColumn(db, 'user_id')
    .then(_.partial(removeColumn, db, 'latitude'))
    .then(_.partial(removeColumn, db, 'longitude'))
    .then(callback)
    .fail(function(err){
      console.log(err);
      callback(err);
    });
};
