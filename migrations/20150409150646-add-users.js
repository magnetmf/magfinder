"use strict";

var dbm = global.dbm || require('db-migrate');
var _ = require('lodash');
var createTimestamps = require('../utils/createTimestamps');

exports.up = function(db, callback) {
  db.createTable('users', {
    id:   { type: 'int', primaryKey: true, autoIncrement: true },
    name: { type: 'text', notNull: true }
  }, function(err){
    if(err){ callback(err); return; }

    createTimestamps(['created_at', 'updated_at'],
                     { db: db, table: 'users' })
    .then(function(){ callback(); })
    .fail(function(err){ console.log(err); callback(err); });
  });
};

exports.down = function(db, callback) {
  db.dropTable('users', callback);
};
