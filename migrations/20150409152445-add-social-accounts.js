"use strict";

var dbm = global.dbm || require('db-migrate');
var _ = require('lodash');
var createTimestamps = require('../utils/createTimestamps');

exports.up = function(db, callback) {
  db.createTable('social_accounts', {
    social_network: {
      type: 'text',
      notNull: true,
      primaryKey: true
    },
    social_account_id: {
      type: 'text',
      notNull: true,
      primaryKey: true
    },
    user_id: {
      type: 'int',
      foreignKey: {
        name: 'social_accounts_user_id_fk',
        table: 'users',
        mapping: 'id',
        rules: { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
      },
    }
  }, function(err){
    if(err){ callback(err); return; }

    createTimestamps(['created_at', 'updated_at'],
                     { db: db, table: 'social_accounts' })
    .then(function(){ callback(); })
    .fail(function(err){ console.log(err); callback(err); });
  });
};

exports.down = function(db, callback) {
  db.dropTable('social_accounts', callback);
};
