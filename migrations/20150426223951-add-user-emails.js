"use strict";

var _ = require('lodash');
var Q = require('q');
var multiline = require('multiline');
var dbm = dbm || require('db-migrate');
var type = dbm.dataType;

function addStoredProcedure(options){
  var deferred = Q.defer();

  // verification code has the format timestamp__randomstring
  var query = multiline(function(){ /*
    CREATE OR REPLACE FUNCTION set_verification_code()
    RETURNS TRIGGER AS
    $body$
      BEGIN
        NEW.VERIFICATION_CODE = ROUND(DATE_PART('epoch', NOW())) || '__' || md5(random()::text);
        RETURN NEW;
      END;
    $body$
    LANGUAGE 'plpgsql';
  */});

  options.db.connection.query(query, function(err){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(); }
  });

  return deferred.promise;
}

function addTrigger(options){
  var deferred = Q.defer();

  var query = multiline(function(){/*
    CREATE TRIGGER TRIGGER_NAME BEFORE EVENT
    ON user_emails FOR EACH ROW EXECUTE PROCEDURE
    set_verification_code();
  */})
    .replace(/TRIGGER_NAME/g, options.event + '_verification_code')
    .replace(/EVENT/g, options.event);

  options.db.connection.query(query, function(err){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(); }
  });

  return deferred.promise;
}

function addInsertTrigger(options){
  return addTrigger({ db: options.db, event: 'INSERT' });
}

function addUpdateTrigger(options){
  return addTrigger({ db: options.db, event: 'UPDATE' });
}

function createUserEmailsTable(options){
  var deferred = Q.defer();

  options.db.createTable('user_emails', {
    user_id: {
      type: 'int',
      foreignKey: {
        name: 'user_emails_user_id_fk',
        table: 'users',
        mapping: 'id',
        rules: { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
      },
      primaryKey: true
    },
    email: {
      type: 'text',
      primaryKey: true
    },
    verification_code: {
      type: 'text',
      notNull: true
    },
    verified_at: {
      type: 'datetime'
    },
  }, function(err){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(); }
  });

  return deferred.promise;
}

exports.up = function(db, callback) {
  createUserEmailsTable({ db: db })
    .then(_.partial(addStoredProcedure, { db: db }))
    .then(_.partial(addInsertTrigger, { db: db }))
    .then(_.partial(addUpdateTrigger, { db: db }))
    .then(function(){ callback(); })
    .fail(function(err){ console.log(err); callback(err); });
};

exports.down = function(db, callback) {
  db.dropTable('user_emails', callback);
};
