"use strict";

var dbm = global.dbm || require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
  db.createTable('favorite_magazines', {
    user_id: {
      type: 'int',
      foreignKey: {
        name: 'favorite_magazines_user_id_fk',
        table: 'users',
        mapping: 'id',
        rules: { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
      },
      primaryKey: true
    },
    magazine_id: {
      type: 'int',
      primaryKey: true
    },
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('favorite_magazines', callback);
};
