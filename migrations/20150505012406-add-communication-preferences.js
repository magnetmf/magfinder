"use strict";

var dbm = global.dbm || require('db-migrate');
var _ = require('lodash');

exports.up = function(db, callback) {
  db.createTable('communication_preferences', {
    user_id: {
      type: 'int',
      foreignKey: {
        name: 'communication_preferences_user_id_fk',
        table: 'users',
        mapping: 'id',
        rules: { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
      },
      primaryKey: true
    },
    receive_email: {
      type: 'boolean',
      notNull: true,
      defaultValue: false
    },
    receive_sms: {
      type: 'boolean',
      notNull: true,
      defaultValue: false
    }
  }, function(err){
    if(err){ callback(err); return; }
    callback();
  });
};

exports.down = function(db, callback) {
  db.dropTable('communication_preferences', callback);
};
