"use strict";

var multiline = require('multiline');
var dbm = global.dbm || require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {

  // make (user_id) the primary key

  db.runSql('ALTER TABLE user_emails DROP CONSTRAINT user_emails_pkey',
  function(err){
    if(err){ return callback(err); }
    db.runSql(multiline(function(){/*
      ALTER TABLE user_emails
      ADD CONSTRAINT user_emails_pkey PRIMARY KEY (user_id)
    */}), callback);
  });
};

exports.down = function(db, callback) {

  // make (user_id, email) the primary key

  db.runSql('ALTER TABLE user_emails DROP CONSTRAINT user_emails_pkey',
  function(err){
    if(err){ return callback(err); }
    db.runSql(multiline(function(){/*
      ALTER TABLE user_emails
      ADD CONSTRAINT user_emails_pkey PRIMARY KEY (user_id, email)
    */}), callback);
  });
};
