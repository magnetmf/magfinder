"use strict";

var _ = require('lodash');
var expect = require('chai').expect;

exports.compareObjects = function compareObjects(obj1, obj2){
  if(_.isArray(obj1)){
    expect(_.isArray(obj2)).to.be.true;
    expect(obj1.length).to.equal(obj2.length);
    _.each(obj1, function(element, i){
      compareObjects(element, obj2[i]);
    });
  }
  else if(_.isObject(obj1)){
    _.each(obj1, function(val, key){
      compareObjects(val, obj2[key]);
    });
  }
  else if(_.isString(obj1) || _.isNumber(obj1) || _.isBoolean(obj1)){
    expect(obj1).to.equal(obj2);
  }
  else {
    throw "Don't know how to compare objects";
  }
}
