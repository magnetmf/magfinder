"use strict";

var dbWrapper = require('../../utils/db_wrapper');
var Q = require('q');
var _ = require('lodash');
var multiline = require('multiline');
var User = require('../../api/models/User');

var userCounter = 0;

function createUser(data){
  userCounter += 1;
  return User.create({
    facebookId: 'fbuser' + userCounter,
    name: data.name
  })
}

function createUserWithRelatedModels(data){
  return createUser(data).then(function(user){
    return Q.all(
      // set user communication preferences
      [User.updateCommunicationPreferences(user.id, data)]

      // set favorite magazines
      .concat(_.map(data.favoriteMagazines, function(magazineID){
        return User.addFavoriteMagazine(user.id, magazineID);
      }))

      // set user phone
      .concat([
        User.updatePhone(user.id, data.phone.number)
          .then(function(){
            return User.getPhone({userID: user.id,
                                 includeVerificationCode: true});
          })
          .then(function parseVerificationCode(phone){
            return parseInt(phone.verificationCode.split('__')[1], 10);
          })
          .then(function(code){
            if(data.phone.verified){
              return User.verifyPhone(user.id, code);
            }
          })
      ])
    );
  });
}

module.exports = function(){

  this.Given(/^the following users exist:$/, function (string, callback) {
    var users;
    try { users = JSON.parse(string); }
    catch(err){
      return callback('Error trying to parse string as JSON\n' + string);
    }

    var promise = new Q(true);
    _.each(users, function(user){
      promise = promise.then(function(){
        return createUserWithRelatedModels(user);
      });
    });

    promise
      .then(function(){ callback(); })
      .fail(function(err){ callback('error creating users\n' + err); });
  });

};
