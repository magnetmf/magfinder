"use strict";

var dbWrapper = require('../../utils/db_wrapper');
var Q = require('q');
var _ = require('lodash');
var multiline = require('multiline');

var dealerCounter = 0;
var magCounter = 0;

function lipsum(){
  return multiline(function(){/*
Lorem ipsum neque porro quisquam est qui dolorem ipsum quia dolor sit amet,
consectetur, adipisci velit
  */});
}

function createDealer(values){
  dealerCounter += 1;

  var query = multiline(function(){/*
    INSERT INTO dim_dealer (
      dealerid, dealername, latitude, longitude, chainhq, chaindiv,
      chainstorenumber, address, city, postalcode, state, phoneareacode,
      phonenumber
    )
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)
    RETURNING dealerid
  */});

  var params = [
    values.id || dealerCounter,
    values.name,
    values.latitude,
    values.longitude,
    values.chainhq || 'foo',
    values.chaindiv || 'foo',
    values.chainstorenumber || 'foo',
    values.address || 'foo',
    values.city || 'foo',
    values.postalcode || 'foo',
    values.state || 'NY',
    values.phoneareacode || '503',
    values.phonenumber || '9999999',
  ];

  return dbWrapper.query(query, params);
}

function getMagazineByName(name){
  return dbWrapper.query(
    'SELECT titleid, bipadname FROM dim_title WHERE bipadname=$1', [name]);
}

function createMagazine(values){
  magCounter += 1;

  var query = multiline(function(){/*
    INSERT INTO dim_title (
      titleid, bipadname, category, title_description, coverpriceus,
      coverpriceca, coverimageurl, titlecategory_popindex, is_new_title
    )
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
    RETURNING titleid, bipadname
  */});

  var params = [
    values.id || magCounter,
    values.name,
    values.category || 'default category',
    values.description || lipsum(),
    values.priceUS || 1.50,
    values.priceCA || 1.50,
    values.coverImageUrl || 'http://foo/bar.png',
    values.popularityInCategory || 99,
    values.isNewTitle ? 1 : 0
  ];

  return dbWrapper.query(query, params);
}

function getOrCreateMagazine(attrs){
  return getMagazineByName(attrs.name).then(function(result){
    if(parseInt(result.rowCount, 10) > 0){ return result; }
    else { return createMagazine(attrs); }
  });
}

function createDealerWithMagazines(dealer){
  return createDealer(dealer).then(function(result){
    return createDealerMagazines(result.rows[0].dealerid, dealer.magazines);
  });
}

function createDealerMagazines(dealerid, magazines){
  var promise = new Q(true);
  _.each(magazines, function(magazineName){
    promise = promise
      .then(function(result){
        return getOrCreateMagazine({ name: magazineName });
      })
      .then(function(result){
        return attachMagazineToDealer({
          magazineId: result.rows[0].titleid,
          dealerId: dealerid
        });
      });
  });
  return promise;
}

function attachMagazineToDealer(values){
  var query = multiline(function(){/*
    INSERT INTO fact_titledealerxr (titleid, dealerid, titlestore_popindex)
    VALUES ($1, $2, $3)
  */});

  var params = [
    values.magazineId,
    values.dealerId,
    values.popularityInStore || 99
  ];

  return dbWrapper.query(query, params);
}

module.exports = function(){

  this.Given(/^the following dealers exist:$/, function (string, callback) {
    var dealers;
    try { dealers = JSON.parse(string); }
    catch(err){
      return callback('Error trying to parse string as JSON\n' + string);
    }

    var promise = new Q(true);
    _.each(dealers, function(dealer){
      promise = promise.then(function(){
        return createDealerWithMagazines(dealer);
      });
    });

    promise
      .then(function(){ callback(); })
      .fail(function(err){ callback('error creating dealers\n' + err); });
  });

  this.Given(/^the following magazines exist:$/, function (string, callback) {
    var magazines;
    try { magazines = JSON.parse(string); }
    catch(err){
      return callback('Error trying to parse string as JSON\n' + string);
    }

    var promise = new Q(true);
    _.each(magazines, function(magazine){
      promise = promise.then(function(){
        return createMagazine(magazine);
      });
    });

    promise
      .then(function(){ callback(); })
      .fail(function(err){ callback('error creating magazines\n' + err); });
  });

};
