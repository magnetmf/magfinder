'use strict';

var superagent = require('superagent');
var _ = require('lodash');
var Q = require('q');
var expect = require('chai').expect;
var config = require('../../configs/public');
var parseJSON = require('../../utils/parseJSON');
var MediaType = require('../support/media_type');
var compareObjects = require('./utils').compareObjects;
var requests = [];


function addRequest(request, callback){
  var httpOperation = { request: request, response: null };
  requests.push(httpOperation);

  request.end(function(err, res){
    if(err){ return callback(err); }

    httpOperation.response = res;
    callback();
  });
}

module.exports = function(){

  this.When(/^a GET request is made to "([^"]*)"$/,
  function(path, callback){
    addRequest(superagent.get(this.serverURL + path), callback);
  });

  this.When(/^a GET request is made to "([^"]*)" with query params:$/,
  function(path, string, callback) {
    parseJSON(string)
      .then(_.bind(function(queryParams){
        addRequest(
          superagent.get(this.serverURL + path).query(queryParams),
          callback
        );
      }, this))
      .fail(callback);
  });

  this.Then(/^a (\d+) status code is returned$/,
  function(statusCode, callback){
    expect(_.last(requests).response.status).to.equal(parseInt(statusCode, 10));
    callback();
  });

  this.Then(/^the following response body is returned:$/,
  function(string, callback){
    parseJSON(string)
      .then(function(expectedResult){
        expect(_.last(requests).response.body).to.deep.equal(expectedResult);
      })
      .then(callback, callback);
  });

  this.Then(/^the response body matches:$/, function(string, callback) {
    parseJSON(string)
      .then(function(expectedResult){
        compareObjects(expectedResult, _.last(requests).response.body);
      })
      .then(callback, callback);
  });

  this.Then(/^the response body matches the "([^"]*)" media type$/,
  function(mediaTypeName, callback){
    return MediaType.validate({
      mediaTypeName: mediaTypeName,
      data: _.last(requests).response.body
    })
    .then(callback, callback);
  });

};
