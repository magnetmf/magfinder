"use strict";

var parseJSON = require('../../utils/parseJSON');
var compareObjects = require('./utils').compareObjects;
var Q = require('q');
var _ = require('lodash');
var User = require('../../api/models/User');

module.exports = function(){

  this.Then(/^User\.getNewMagazineNotifications should match$/, function(string, callback) {
    Q.all([
      parseJSON(string),
      User.getNewMagazineNotifications()
    ])
      .then(function(results){
        return compareObjects(results[0], results[1]);
      })
      .then(_.partial(callback, null))
      .fail(callback);
  });

};
