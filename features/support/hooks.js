"use strict";

// load env variables from .env file
require('dotenv').load({
  path: require('path').join(__dirname, '../../.env.test')
});

var _ = require('lodash');
var Q = require('q');
var exec = require('child_process').exec;
var dbWrapper = require('../../utils/db_wrapper');
var dbCon = 'postgres://' + process.env.TEST_DB_USER + ':' +
            process.env.TEST_DB_PWD + '@localhost/';

// use the process id to generate a random port
var port = 8000 + parseInt(process.pid, 10);
var dbName = 'magnet_test_' + port;

// modify privateConf so it points to test db
var privateConf = require('../../configs/private');
privateConf.setDbConString('magnet', dbCon + dbName);
privateConf.setDbConString('app', dbCon + dbName);

var queryOptions = {
  conString: dbCon + 'template1',
  destroyClient: true
};

var server;

var myHooks = function(){

  this.Before(function(callback){
    this.port = port;
    this.serverURL = 'http://localhost:' + port;

    (function dropTestDb(){
      return dbWrapper
        .query("DROP DATABASE IF EXISTS " + dbName, [], queryOptions);
    })()

    .then(function createTestDb(){
      return dbWrapper.query(
        "CREATE DATABASE " + dbName + " TEMPLATE magnet_test_template",
        [], queryOptions);
    })

    .then(function startServer(){
      if(!server){
        console.log('starting server on port ' + port);
        server = require('../../server').listen(port);
      }
    })

    .then(callback, callback);
  });

  this.After(function(callback){
    // close any open db connections
    dbWrapper.end();
    callback();
  });

  this.registerHandler('AfterFeatures', function (event, callback) {
    // close server
    if(server){
      server.close();
    }

    // drop test db
    dbWrapper.query("DROP DATABASE IF EXISTS " + dbName, [], queryOptions)
      .then(callback)
      .fail(function(err){
        console.log(err);
        callback(err);
      });
  });

};

module.exports = myHooks;
