'use strict';

var path = require('path');
var fs = require('fs');
var Q = require('q');
var _ = require('lodash');
var tv4 = require('tv4');

tv4.addFormat(require('tv4-formats'));

// load all media type files into tv4

Q.denodeify(fs.readdir)(__dirname)

  .then(function onlyKeepJSONFiles(files){
    return _.filter(files, function(name){ return name.match(/\.json$/); });
  })

  .then(function createReadFileFunctions(files){
    return _.map(files, function(name){
      return Q.denodeify(fs.readFile)(path.join(__dirname, name), 'utf8')
        .then(function(content){
          return { name: name, data: JSON.parse(content) };
        });
    });
  })

  .then(Q.all)

  .then(function addFilesToTv4(files){
    _.each(files, function(file){
      tv4.addSchema(file.name.split('.json')[0], file.data);
    });
  })

  .fail(function(err){
    throw err;
  });

exports.validate = function(options){
  var deferred = Q.defer();

  var valid = tv4.validate(options.data, options.mediaTypeName);
  if(!valid){
    console.log('######## INVALID MEDIATYPE ' + options.mediaTypeName + ' #############');
    console.log(JSON.stringify(options.data, null, '  '));
    console.log('#####################');
    var err = _.pick(tv4.error, ['message', 'dataPath', 'schemaPath']);
    console.log(err);
    deferred.reject(err);
  }
  else if(!_.isEmpty(tv4.missing)){
    deferred.reject("Missing schemas: " + tv4.missing);
  }
  else {
    deferred.fulfill();
  }

  return deferred.promise;
};
