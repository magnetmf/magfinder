Feature: Search magazines by category

  Use the API to search magazines sold near your current location, filtering
  by category.

  Background:
    Given the following magazines exist:
    """
    [
      { "name": "MENS HEALTH", "category": "FITNESS" },
      { "name": "WOMENS HEALTH", "category": "FITNESS" },
      { "name": "HOUSE & HOME", "category": "HOME" },
      { "name": "ROMANTIC HOMES", "category": "HOME" }
    ]
    """
    And the following dealers exist:
    """
    [
      {
        "name": "Carls Liquor Market",
        "latitude": 50.00,
        "longitude": -61.00,
        "magazines": [ "ROMANTIC HOMES", "WOMENS HEALTH" ]
      },
      {
        "name": "Ingles Supermarket",
        "latitude": 50.00,
        "longitude": -60.00,
        "magazines": [ "MENS HEALTH" ]
      },
      {
        "name": "Walgreens",
        "latitude": 50.00,
        "longitude": -59.00,
        "magazines": [ "HOUSE & HOME", "WOMENS HEALTH" ]
      }
    ]
    """

  Scenario: User is less than 10 miles from store
    By default, all results within a 10 mile radius will be returned.

    The user is at [50,-60], which is
    - 0 miles from Ingles Supermarket
    - 44 miles from Carls Liquor Market
    - 44 miles from Walgreens

    Since Men's Health is sold at Ingles Supermarket (within 10 miles), only
    that should be returned when searching for Fitness magazines.

    When a GET request is made to "/api/categories/FITNESS/magazines" with query params:
    """
    { "latitude": 50.0, "longitude": -60.0 }
    """
    Then a 200 status code is returned
    And the response body matches the "magazines" media type
    And the response body matches:
    """
    [{ "name": "MENS HEALTH" }]
    """

  Scenario: User is more than 10 miles from store
    If no result is found within 10 miles, the api should then search for
    results within 50 miles.

    The user is at [50,-60], which is
    - 0 miles from Ingles Supermarket
    - 44 miles from Carls Liquor Market
    - 44 miles from Walgreens

    Since no magazine in the "Home" category is sold at Ingles, the search
    will extend to 50 miles and include results from Carls and Walgreens.

    When a GET request is made to "/api/categories/HOME/magazines" with query params:
    """
    { "latitude": 50.0, "longitude": -60.0 }
    """
    Then a 200 status code is returned
    And the response body matches the "magazines" media type
    And the response body matches:
    """
    [{ "name": "HOUSE & HOME" }, { "name": "ROMANTIC HOMES" }]
    """

  Scenario: User is more than 50 miles from the store
    If no result is found within 50 miles, the api should return an empty result

    The user is at [51,-60], which is
    - 69 miles from Ingles Supermarket
    - 82 miles from Carls Liquor Market
    - 82 miles from Walgreens

    When a GET request is made to "/api/categories/HOME/magazines" with query params:
    """
    { "latitude": 51.0, "longitude": -60.0 }
    """
    Then a 200 status code is returned
    And the response body matches the "magazines" media type
    And the response body matches:
    """
    []
    """
