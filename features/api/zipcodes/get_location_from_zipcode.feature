Feature: Get Location from Zipcode

  Use the API to send a zip code and obtain a location with country,
  state, county, city, latitude, and longitude.

  Scenario: US zipcode
    9001 is the zip code for Florence-Graham, CA

    When a GET request is made to "/api/zipcodes/90001"
    Then a 200 status code is returned
    And the following response body is returned:
    """
    {
      "zipCode": "90001",
      "country": "US",
      "state": "California",
      "county": "Los Angeles",
      "city": "Los Angeles",
      "latitude": 33.9731,
      "longitude": -118.2479
    }
    """

  Scenario: CA zipcode
    T5A 0A7 is a postal code for Edmonton, Alberta, Canada

    When a GET request is made to "/api/zipcodes/T5A 0A7"
    Then a 200 status code is returned
    And the response body matches the "location" media type
    And the following response body is returned:
    """
    {
      "zipCode": "T5A",
      "country": "CA",
      "state": "Alberta",
      "county": "",
      "city": "Edmonton (West Clareview / East Londonderry)",
      "latitude": 53.5931,
      "longitude": -113.4077
    }
    """

  Scenario: Incomplete CA zipcode
    Passing only the first 3 characters of the CA zipcode should be enough.
    When a GET request is made to "/api/zipcodes/T5A"
    Then a 200 status code is returned
    And the response body matches the "location" media type
    And the response body matches:
    """
    { "city": "Edmonton (West Clareview / East Londonderry)" }
    """

  Scenario: Case insensitive CA zipcode
    Zipcode match should be case insensitive.

    When a GET request is made to "/api/zipcodes/t5a"
    Then a 200 status code is returned
    And the response body matches the "location" media type
    And the response body matches:
    """
    { "city": "Edmonton (West Clareview / East Londonderry)" }
    """
