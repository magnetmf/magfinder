Feature: Search popular magazines

  Use the API to search magazines sold near your current location, sorted
  by popularity index.

  Background:
    Given the following magazines exist:
    """
    [
      { "name": "MENS HEALTH", "popularityInCategory": 3 },
      { "name": "WOMENS HEALTH", "popularityInCategory": 1 },
      { "name": "SKIN & INK", "popularityInCategory": 2 },
      { "name": "ANIMAL TALES", "popularityInCategory": 5,
        "coverImageUrl": "http://covers.magnetdata.net/thumbnails/DefaultCover.gif" }
    ]
    """
    And the following dealers exist:
    """
    [
      {
        "name": "Ingles Supermarket",
        "latitude": 50.00,
        "longitude": -60.00,
        "magazines": [ "MENS HEALTH", "WOMENS HEALTH", "SKIN & INK", "ANIMAL TALES" ]
      }
    ]
    """

  Scenario: Without max distance
    By default, all results within a 10 mile radius will be returned, except magazines
    having the default cover image.

    When a GET request is made to "/api/magazines" with query params:
    """
    {
      "popular": "true",
      "latitude": 50.0,
      "longitude": -60.0
    }
    """
    Then a 200 status code is returned
    And the response body matches the "magazines" media type
    And the response body matches:
    """
    [{ "name": "MENS HEALTH" }, { "name": "SKIN & INK" }, { "name": "WOMENS HEALTH" }]
    """

  Scenario: User is more than 10 miles from the store
    If no result is found within 10 miles, the api should then search for
    results within 50 miles, except magazines having the default cover image.

    User is at [50,-61], which is 44.46 miles from Ingles Supermarket

    When a GET request is made to "/api/magazines" with query params:
    """
    {
      "popular": "true",
      "latitude": 50.0,
      "longitude": -61.0
    }
    """

    Then a 200 status code is returned
    And the response body matches the "magazines" media type
    And the response body matches:
    """
    [{ "name": "MENS HEALTH" }, { "name": "SKIN & INK" }, { "name": "WOMENS HEALTH" }]
    """

  Scenario: User is more than 50 miles from any store
    If no result is found within 10 miles, the api should then search for
    results within 50 miles, If no result is found within 50 miles it should
    return an empty array.

    User is at [50,-61.5], which is 66.82 miles from Ingles Supermarket

    When a GET request is made to "/api/magazines" with query params:
    """
    {
      "popular": "true",
      "latitude": 50.0,
      "longitude": -61.5
    }
    """
    Then a 200 status code is returned
    And the response body matches the "magazines" media type
    And the response body matches:
    """
    []
    """
