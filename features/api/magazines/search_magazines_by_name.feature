Feature: Search magazines by name

  Use the API to search magazines sold near your current location, filtering
  by name.

  Background:
    Given the following dealers exist:
    """
    [
      {
        "name": "Ingles Supermarket",
        "latitude": 50.00,
        "longitude": -60.00,
        "magazines": [ "NEBRASKA LIFE CALENDAR", "PHOTO CLASSIFIED", "SKIN & INK", "MENS HEALTH" ]
      },
      {
        "name": "Walgreens",
        "latitude": 50.00,
        "longitude": -59.00,
        "magazines": [ "HOUSE & HOME", "SKIN & INK", "WOMENS HEALTH" ]
      },
      {
        "name": "Carls Liquor Market",
        "latitude": 50.00,
        "longitude": -58.00,
        "magazines": [ "CAKE & WHISKEY", "PHOTO CLASSIFIED", "WILMINGTON MAGAZINE" ]
      }
    ]
    """

  Scenario: Without required params
    If required params are not sent, an error is returned

    When a GET request is made to "/api/magazines"
    Then a 400 status code is returned

  Scenario: Without max distance
    By default, all results within a 10 mile radius will be returned.
    - 0 miles from Ingles Supermarket[50,-60]
    - 44.55 miles from Walgreens[50,59]
    - 89.10 miles from Carls Liquor Market[50,-58]

    When a GET request is made to "/api/magazines" with query params:
    """
    {
      "query": "health",
      "latitude": 50.0,
      "longitude": -60.0
    }
    """
    Then a 200 status code is returned
    And the response body matches the "magazines" media type
    And the response body matches:
    """
    [{ "name": "MENS HEALTH" }]
    """

  Scenario: User is more than 10 miles from any store
    If no result is found within 10 miles, the api should then search for
    results within 50 miles.
    - 22.27 miles from Ingles Supermarket[50,-60]
    - 22.27 miles from Walgreens[50,59]
    - 66.82 miles from Carls Liquor Market[50,-58]

    When a GET request is made to "/api/magazines" with query params:
    """
    {
      "query": "health",
      "latitude": 50.0,
      "longitude": -59.5
    }
    """
    Then a 200 status code is returned
    And the response body matches the "magazines" media type
    And the response body matches:
    """
    [{ "name": "WOMENS HEALTH" }, { "name": "MENS HEALTH" }]
    """

  Scenario: User is more than 50 miles from any store
    If no result is found within 10 miles, the api should then search for
    results within 50 miles, If no result is found within 50 miles it should return an empty array.
    - 66.82 miles from Ingles Supermarket[50,-60]
    - 91.32 miles from Walgreens[50,59]
    - 135.87 miles from Carls Liquor Market[50,-58]

    When a GET request is made to "/api/magazines" with query params:
    """
    {
      "query": "health",
      "latitude": 50.0,
      "longitude": -61.5
    }
    """
    Then a 200 status code is returned
    And the response body matches the "magazines" media type
    And the response body matches:
    """
    []
    """

  Scenario: Setting a max distance
    By specifying `distance`, you can expand or reduce the search radius.
    Distance must be provided in miles.
    - 0 miles from Ingles Supermarket[50,-60]
    - 44.55 miles from Walgreens[50,59]
    - 89.10 miles from Carls Liquor Market[50,-58]

    When a GET request is made to "/api/magazines" with query params:
    """
    {
      "query": "health",
      "latitude": 50.0,
      "longitude": -60.0,
      "distance": 0.5
    }
    """
    Then a 200 status code is returned
    And the response body matches the "magazines" media type
    And the response body matches:
    """
    [{ "name": "MENS HEALTH" }]
    """
