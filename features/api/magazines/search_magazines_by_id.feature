Feature: Search magazines by id

  Use the API to search magazines sold near your current location, filtering
  by id.

  Background:
    Given the following magazines exist:
    """
    [
      { "id": 1, "name": "MENS HEALTH" },
      { "id": 2, "name": "WOMENS HEALTH" },
      { "id": 3, "name": "SKIN & INK" }
    ]
    """

    And the following dealers exist:
    """
    [
      {
        "name": "Ingles Supermarket",
        "latitude": 50.00,
        "longitude": -60.00,
        "magazines": [ "SKIN & INK", "MENS HEALTH" ]
      }
    ]
    """

  Scenario: User is less than 10 miles from store
    By default, all results within a 10 mile radius will be returned.

    When a GET request is made to "/api/magazines/1" with query params:
    """
    { "latitude": 50.0, "longitude": -60.0 }
    """
    Then a 200 status code is returned
    And the response body matches the "magazines" media type
    And the response body matches:
    """
    [{ "name": "MENS HEALTH" }]
    """

  Scenario: User is more than 10 miles from the store
    If no result is found within 10 miles, the api should then search for
    results within 50 miles.

    [50,-61] is 44.46 miles from Ingles Supermarket [50,-60]

    When a GET request is made to "/api/magazines/1" with query params:
    """
    { "latitude": 50.0, "longitude": -61.0 }
    """
    Then a 200 status code is returned
    And the response body matches the "magazines" media type
    And the response body matches:
    """
    [{ "name": "MENS HEALTH" }]
    """

  Scenario: User is more than 50 miles from the store
    If no result is found within 50 miles, the api should return an empty result

    [50,-62] is 88.92 miles from Ingles Supermarket [50,-60]

    When a GET request is made to "/api/magazines/1" with query params:
    """
    { "latitude": 50.0, "longitude": -62.0 }
    """
    Then a 200 status code is returned
    And the response body matches the "magazines" media type
    And the response body matches:
    """
    []
    """
