Feature: Search dealer by id

  Use the API to search a dealer by id.

  Background:
    Given the following dealers exist:
    """
    [
      {
        "id": 1,
        "name": "Ingles Supermarket",
        "latitude": 50.00,
        "longitude": -60.00
      },
      {
        "id": 2,
        "name": "Walgreens",
        "latitude": 50.00,
        "longitude": -60.00
      }
    ]
    """

  Scenario: User is less than 10 miles from dealer
    By default, all results within a 10 mile radius will be returned.

    When a GET request is made to "/api/dealers/2" with query params:
    """
    { "latitude": 50.0, "longitude": -60.0 }
    """
    Then a 200 status code is returned
    And the response body matches the "dealers" media type
    And the response body matches:
    """
    [{ "name": "Walgreens" }]
    """
