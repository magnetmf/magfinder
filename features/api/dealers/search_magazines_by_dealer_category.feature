Feature: Search magazines by category in a specific dealer

  Use the API to search magazines sold near your current location, filtering
  both by dealer and category.

  Background:
    Given the following magazines exist:
    """
    [
      { "name": "MENS HEALTH", "category": "FITNESS" },
      { "name": "WOMENS HEALTH", "category": "FITNESS" },
      { "name": "SKIN & INK", "category": "ALTERNATE LIFESTYLE" }
    ]
    """
    And the following dealers exist:
    """
    [
      {
        "id": 1,
        "name": "Ingles Supermarket",
        "latitude": 50.00,
        "longitude": -60.00,
        "magazines": [ "MENS HEALTH", "WOMENS HEALTH", "SKIN & INK" ]
      },
      {
        "id": 2,
        "name": "Walgreens",
        "latitude": 50.00,
        "longitude": -60.00,
        "magazines": [ "SKIN & INK", "WOMENS HEALTH" ]
      }
    ]
    """

  Scenario: Querying categories for a dealer
    If you specify a dealer, all of its categories will be returned, irrelevant
    of your current location (your location is used to calculate the distance
    to the dealer, but not for filtering)

    When a GET request is made to "/api/dealers/2/categories/FITNESS/magazines" with query params:
    """
    {
      "latitude": 51.0,
      "longitude": -61.0
    }
    """
    Then a 200 status code is returned
    And the response body matches the "magazines" media type
    And the response body matches:
    """
    [{ "name": "WOMENS HEALTH" }]
    """
