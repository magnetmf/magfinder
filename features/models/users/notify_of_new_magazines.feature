Feature: Notify Users of New Magazines

  Given an array of magazines, the user model should provide all users that want
  to be notified by SMS when those magazines get new editions

  Scenario: No new magazines
    Given the following magazines exist:
    """
    [{ "id": 99, "name": "MENS HEALTH", "isNewTitle": false }]
    """
    And the following users exist:
    """
    [{ "name": "John", "phone": { "number": "123", "verified": true }, "receiveSMS": true, "favoriteMagazines": [99] }]
    """
    Then User.getNewMagazineNotifications should match
    """
    []
    """

  Scenario: User does not want to be notified
    Given the following magazines exist:
    """
    [{ "id": 99, "name": "MENS HEALTH", "isNewTitle": true }]
    """
    And the following users exist:
    """
    [{ "name": "John", "phone": { "number": "123", "verified": true }, "receiveSMS": false, "favoriteMagazines": [99] }]
    """
    Then User.getNewMagazineNotifications should match
    """
    []
    """

  Scenario: User has not confirmed phone
    Given the following magazines exist:
    """
    [{ "id": 99, "name": "MENS HEALTH", "isNewTitle": true }]
    """
    And the following users exist:
    """
    [{ "name": "John", "phone": { "number": "123", "verified": false }, "receiveSMS": true, "favoriteMagazines": [99] }]
    """
    Then User.getNewMagazineNotifications should match
    """
    []
    """

  Scenario: User is notified
    Given the following magazines exist:
    """
    [
      { "id": 98, "name": "SKIN & INK", "isNewTitle": true },
      { "id": 99, "name": "MENS HEALTH", "isNewTitle": false },
      { "id": 100, "name": "HOME & GARDEN", "isNewTitle": true }
    ]
    """
    And the following users exist:
    """
    [
      { "name": "John", "phone": { "number": "123", "verified": true }, "receiveSMS": true, "favoriteMagazines": [98, 99] },
      { "name": "Mary", "phone": { "number": "456", "verified": true }, "receiveSMS": true, "favoriteMagazines": [98, 100] },
      { "name": "Luke", "phone": { "number": "789", "verified": true }, "receiveSMS": false, "favoriteMagazines": [99, 100] },
      { "name": "Anne", "phone": { "number": "101", "verified": false}, "receiveSMS": true, "favoriteMagazines": [98] }
    ]
    """
    Then User.getNewMagazineNotifications should match
    # John (123) should receive messages for: SKIN & INK
    # Mary (456) should receive messages for: SKIN & INK, HOME & GARDEN
    # Luke (789) should receive no messages (receiveSMS is false)
    # Anne (101) should receive no messages (phone is not verified)
    """
    [
      {
        "phoneNumber": "123",
        "magazines": [{ "id": 98, "name": "SKIN & INK" }]
      },
      {
        "phoneNumber": "456",
        "magazines": [{ "id": 98, "name": "SKIN & INK" }, { "id": 100, "name": "HOME & GARDEN" }]
      }
    ]
    """
