/*
  This mixin provides a 'redirectIfNoUser()' function, which redirects to the
  login page if there's no logged in user.

  ** Requires the component to load the StoreMixin as well
*/

"use strict";

var _ = require('lodash');
var UserStore = require('../stores/UserStore');
var navigateAction = require('flux-router-component').navigateAction;

module.exports = {
  redirectIfNoUser: function(options){
    options = _.extend({
      url: this.props.context.makePath('signup')
    }, options);
    if(!this.getStore(UserStore).getState().user){
      this.props.context.executeAction(navigateAction, { url: options.url });
    }
  },

  redirectIfUser: function(options){
    options = _.extend({
      url: this.props.context.makePath('account')
    }, options);
    if(this.getStore(UserStore).getState().user){
      this.props.context.executeAction(navigateAction, { url: options.url });
    }
  }
};
