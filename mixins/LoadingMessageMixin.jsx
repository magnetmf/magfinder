/*
  This mixin provides a 'renderLoadingMessage()' function, which returns a
  <Message> object if search results are still being loaded, or false.
*/

"use strict";

var React = require('react');
var _ = require('lodash');
var LocationStore = require('../stores/LocationStore');
var SearchStore = require('../stores/SearchStore');
var DirectionsStore = require('../stores/DirectionsStore');
var Message = require('../components/general/Message.jsx');
var StoreMixin = require('fluxible').StoreMixin;
var debug = require('debug')('geolocation');
var ZipCodeForm = require('../components/general/ZipCodeForm.jsx');

var LoadingMessage = React.createClass({
  mixins: [StoreMixin],

  statics: {
    storeListeners: [LocationStore, SearchStore, DirectionsStore]
  },

  getStateFromStores: function(){
    return _.extend(
      this.getStore(LocationStore).getState(),
      this.getStore(SearchStore).getState(),
      this.getStore(DirectionsStore).getState()
    );
  },

  getInitialState: function(){
    return this.getStateFromStores();
  },

  onChange: function(){
    this.setState(this.getStateFromStores());
  },

  render: function(){
    var msg = renderAnyLoadingMessage(this.props.context,
                                      this.props.shouldHaveLoaded);
    return <div>{ msg }</div>;
  }

});

function renderLocationLoadingMessage(context){
  var state = _.pick(context.getStore(LocationStore).getState(),
                    ['zipCode', 'geolocationSupport', 'currentLocation']);
  var location = state.currentLocation;

  if(location && location.isFulfilled()){ return; }

  if(!location || location.isPending()){
    // location is currently being loaded

    if(state.zipCode){
      return (
        <Message type="loading">
          Loading information for zip code {state.zipCode}...
        </Message>
      );
    }

    if(state.geolocationSupport === false){
      return (
        <Message type="error">
          Geolocation is not supported on your device.<br/><br/>
          Please provide a zip code.<br/><br />
          <ZipCodeForm context={context} />
        </Message>
      );
    }

    return (
      <Message type="loading">
        Waiting for geolocation information...<br /><br />
        Please make sure to provide geolocation permissions for the app.
      </Message>
    );
  }

  if(location.isRejected()){
    if(state.zipCode){
      return (
        <Message type="error">
          Zip code {state.zipCode} could not be found.<br/><br/>
          Please provide a different zip code.
          <ZipCodeForm context={context} />
        </Message>
      );
    }

    var error = location.valueOf();
    var positionError = error.exception;
    if(positionError && positionError.code){
      switch(positionError.code){
        case positionError.PERMISSION_DENIED:
          debug('User denied the request for Geolocation');
          break;
        case positionError.POSITION_UNAVAILABLE:
          debug('Location information is unavailable.');
          break;
        case positionError.TIMEOUT:
          debug('The request to get user location timed out.');
          break;
        case positionError.UNKNOWN_ERROR:
          debug('An unknown error occurred when getting user location.');
          break;
      }

      return (
        <Message>
          We’re unable to detect your location.
          <br /><br />
          Please verify you have location services enabled and refresh the page.
          <br /><br />
          Or, you may provide a zip code.
          <br />
          <ZipCodeForm context={context} />
        </Message>
      );
    }

    debug('Error when getting user location: ', positionError);

    return (
      <Message>
        There was an error when trying to detect your location.<br /><br />
        Please refresh the page.
      </Message>
    );
  }
}

function renderSearchResultsLoadingMessage(context){
  var state = context.getStore(SearchStore).getState();
  var searchResults = state.results;

  if(!searchResults || searchResults.isPending()){
    return <Message type="loading">Loading...</Message>;
  }

  if(searchResults.isRejected()){
    if(_.isString(state.searchStr)){
      return (
        <Message type="error">
          There was an error fetching magazines with title {state.searchStr}.
        </Message>
      );
    }

    return (
      <Message type="error">
        There was an error fetching magazine with id {state.searchStr}.
      </Message>
    );
  }
}

function renderDirectionsLoadingMessage(context){
  var directions = context.getStore(DirectionsStore).getState().directions;

  if(!directions || directions.isPending()){
    return <Message type="loading">Looking for directions...</Message>;
  }

  if(directions.isRejected()) {
    return <Message type="error">Could not find directions.</Message>;
  }
}

function renderAnyLoadingMessage(context, shouldHaveLoaded){
  if(!_.contains(
    ['currentLocation', 'searchResults', 'directions'], shouldHaveLoaded))
  {
    throw 'invalid value for shouldHaveLoad param:' + shouldHaveLoaded;
  }

  var loadingMsg;

  // currentLocation
  loadingMsg = renderLocationLoadingMessage(context);
  if(loadingMsg){ return loadingMsg; }
  else if(shouldHaveLoaded === 'currentLocation'){ return; }

  // searchResults
  loadingMsg = renderSearchResultsLoadingMessage(context);
  if(loadingMsg){ return loadingMsg; }
  else if(shouldHaveLoaded === 'searchResults'){ return; }

  // directions
  loadingMsg = renderDirectionsLoadingMessage(context);
  if(loadingMsg){ return loadingMsg; }

  return false;
}

module.exports = {
  renderLoadingMessage: function(context, shouldHaveLoaded){
    var msg = renderAnyLoadingMessage(context, shouldHaveLoaded);
    if(msg){
      return <LoadingMessage context={context}
              shouldHaveLoaded={shouldHaveLoaded} />;
    }
  }
};
