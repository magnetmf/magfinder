"use strict";

var Q = require('q');
var config = require('../configs/private');
var client = require('twilio')(config.twilioAccountSid, config.twilioAuthToken);

module.exports = function sendPhoneVerificationCode(options){
  var deferred = Q.defer();

  client.sendMessage({
    to: options.phoneNumber,
    from: config.twilioPhoneNumber,
    body: 'YOUR VERIFICATION CODE IS: ' + options.codeValue
  },function(err, response){
    if(err){ console.log(err); deferred.reject(err); }
    else { deferred.fulfill(response); }
  });

  return deferred.promise;
};
