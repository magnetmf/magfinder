"use strict";

exports.milesToMeters = function(miles){
  return miles * 1609.34;
};

exports.metersToMiles = function(meters){
  return meters / 1609.34;
};
