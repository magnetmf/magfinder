"use strict";

var multiline = require('multiline');
var _ = require('lodash');
var Q = require('q');

function addColumn(options, err){
  var deferred = Q.defer();

  options.db.addColumn(options.table, options.columnName, {
    type: 'datetime',
    notNull: true
  }, function(err){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(); }
  });

  return deferred.promise;
}

function addProcedure(options){
  var deferred = Q.defer();

  var query = multiline(function(){ /*
    CREATE OR REPLACE FUNCTION set_COLUMN_NAME()
      RETURNS TRIGGER AS '
      BEGIN
        NEW.COLUMN_NAME = NOW();
        RETURN NEW;
      END;
    ' LANGUAGE 'plpgsql';
  */}).replace(/COLUMN_NAME/g, options.columnName);

  options.db.connection.query(query, function(err){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(); }
  });

  return deferred.promise;
}

function addTrigger(options){
  var deferred = Q.defer();

  var query = multiline(function(){/*
    CREATE TRIGGER TRIGGER_NAME BEFORE EVENT
    ON MY_TABLE FOR EACH ROW EXECUTE PROCEDURE
    set_COLUMN_NAME();
  */})
    .replace(/TRIGGER_NAME/g, options.event + '_' + options.columnName)
    .replace(/COLUMN_NAME/g, options.columnName)
    .replace(/EVENT/g, options.event)
    .replace(/MY_TABLE/g, options.table);

  options.db.connection.query(query, function(err){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(); }
  });

  return deferred.promise;
}

function addTriggers(options){
  // `created_at` and `updated_at` need to be set on INSERT, but only
  // `updated_at` needs to be set on UPDATE

  var promise = addTrigger(_.extend({}, options, { event: 'INSERT' }));

  if(options.columnName === 'updated_at') {
    promise = promise.then(function(){
      return addTrigger(_.extend({}, options, { event: 'UPDATE' }));
    });
  }

  return promise;
}

module.exports = function createTimestamps(columns, options){
  var promise = Q.when();

  _.each(columns, function(columnName){
    if(!_.include(['created_at', 'updated_at'], columnName)){
      throw 'unknown column ' + columnName;
    }

    var newOptions = _.extend({}, options, {columnName: columnName});
    promise = promise
      .then(_.partial(addColumn, newOptions))
      .then(_.partial(addProcedure, newOptions))
      .then(_.partial(addTriggers, newOptions));
  });

  return promise;
};
