"use strict";

var pg = require('pg');
var Q = require('q');
var _ = require('lodash');
var debug = require('debug')('dbqueries');
var conf = require('../configs/private');

function constructDbOptions(options){
  var dbOptions = _.extend({
    db: 'magnet',
    destroyClient: false
  }, options);

  if(!dbOptions.conString){
    dbOptions.conString = conf.getDbConString(dbOptions.db);
  }

  return dbOptions;
}

function runQuery(client, queryStr, args){
  var deferred = Q.defer();

  debug(queryStr, args);

  client.query(queryStr, args, function(err, result){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(result); }
  });

  return deferred.promise;
}

exports.transaction = function dbTransaction(queriesConstructor, options){
  var deferred = Q.defer();
  var dbOptions = constructDbOptions(options);

  pg.connect(dbOptions.conString, function(err, client, done){
    if(err){
      console.error('error fetching client from pool', err);
      return deferred.reject(err);
    }

    debug('BEGIN');
    client.query('BEGIN', function(err) {
      if(err){
        console.error('error starting transaction', err);
        done(true);
        return deferred.reject(err);
      }

      queriesConstructor(_.partial(runQuery, client))
        .then(function(result){
          debug('COMMIT');
          client.query('COMMIT', function(err){
            if(err){
              console.error('error commiting transaction', err);
              done(true);
              return deferred.reject(err);
            }
            done(dbOptions.destroyClient);
            return deferred.resolve(result);
          });
        })
        .fail(function(err){
          debug('ROLLBACK', err);
          client.query('ROLLBACK', function(rollbackErr){
            if(rollbackErr){
              console.error('error rolling back transaction', rollbackErr);
            }
            done(true);
            return deferred.reject(err);
          });
        });
    });
  });

  return deferred.promise;
};

exports.query = function dbQuery(queryStr, args, options){
  var deferred = Q.defer();
  var dbOptions = constructDbOptions(options);

  pg.connect(dbOptions.conString, function(err, client, done){
    if(err) {
      console.error('error fetching client from pool', err);
      return deferred.reject(err);
    }

    runQuery(client, queryStr, args)
      .then(function(result){
        done(dbOptions.destroyClient);
        deferred.resolve(result);
      })
      .fail(function(err){
        done(true);
        return deferred.reject(err);
      });
  });

  return deferred.promise;
};

exports.end = function(){
  pg.end();
};
