"use strict";

module.exports = {
  split: function(price){
    var priceArr = price.toFixed(2).split('.');
    return {
      dollars: '$' + priceArr[0],
      cents: '.' + priceArr[1]
    };
  }
};
