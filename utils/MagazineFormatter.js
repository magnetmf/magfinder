"use strict";

var _ = require('lodash');

function getMagazinePrice(magazine, location){
  var priceAttr = {
    'US': 'priceUS',
    'CA': 'priceCA'
  }[location.country] || 'priceUS';

  var price = magazine[priceAttr];

  if(!_.isNumber(price) ||  price <= 0){
    // sometimes the Canada price is empty, so we use the US price
    price = magazine.priceUS;
  }

  return price;
}

function findNearest(dealers){
  return _.sortBy(dealers, 'distance')[0];
}

exports.formatMagazines = function formatMagazines(magazines, location){
  return _.map(magazines, function(magazine){
    var nearestDealer = findNearest(magazine.dealers);
    magazine.nearestDealer = nearestDealer;
    magazine.nearestDistance = nearestDealer.distance;
    magazine.price = getMagazinePrice(magazine, location);
    return magazine;
  });
};
