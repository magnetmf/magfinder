"use strict";

var Q = require('q');

module.exports = function parseJSON(string){
  var deferred = Q.defer();
  try { deferred.resolve(JSON.parse(string)); }
  catch(err){
    deferred.reject('Error trying to parse string as JSON\n' + string);
  }
  return deferred.promise;
};
