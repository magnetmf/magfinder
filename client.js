/* jshint browser: true */
'use strict';
var React = require('react');
var app = require('./fluxible-app');
var dehydratedState = window.App; // Sent from the server
var loadRouteComponent = require('./actions/loadRouteComponent');

window.React = React; // For chrome dev tool support

app.rehydrate(dehydratedState, function (err, context) {
  if(err) { throw err; }

  context.getActionContext().executeAction(loadRouteComponent, {},
  function(err){
    if(err){ throw err; }

    React.render(app.getAppComponent()({
      context: context.getComponentContext()
    }), document.getElementById('app'));
  });

  window.addEventListener('resize', function(event){
    context.getActionContext().dispatch('WINDOW_RESIZE');
  });
});
