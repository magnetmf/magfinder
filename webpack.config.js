"use strict";
var webpack = require("webpack");

module.exports = {
    resolve: {
        extensions: ["", ".js", ".jsx"]
    },
    entry: "./client.js",
    output: {
        path: __dirname + "/build",
        publicPath: "/public/",
        filename: "client.js"
    },
    module: {
        loaders: [
            { test: /\.jsx$/, loader: "jsx-loader" },
            { test: /\.json$/, loader: "json-loader" },
            { test: /\.css$/, loader: "style-loader!css-loader" },
            { test: /\.less$/, loader: "style-loader!css-loader!less-loader" },
            { test: /\.jpg/, loader: "url-loader?mimetype=image/jpg" },
            { test: /\.png$/, loader: "url-loader?mimetype=image/png" },
            { test: /\.svg$/, loader: "url-loader?mimetype=image/svg+xml" },
            { test: /\.eot$/, loader: "url-loader?mimetype=application/vnd.ms-fontobject" },
            { test: /\.ttf$/, loader: "url-loader?mimetype=application/octet-stream" },
            { test: /\.woff$/, loader: "url-loader?mimetype=application/x-woff" }
        ]
    }
};
