"use strict";

var _ = require('lodash');
var Q = require('q');
var config = require('../configs/public');
var request = require('superagent');

function apiCall(attrs){
  var deferred = Q.defer();

  var url = config.apiURL + '/feedback';

  request
    .post(url)
    .set('Content-Type', 'application/json')
    .send(JSON.stringify(attrs))
    .end(function(err, res){
      if(err || res.status !== 200){
        deferred.reject(err || res.status);
      }
      else {
        deferred.resolve(res.body);
      }
    });

  return deferred.promise;
}

function submitFeedback(context, payload, done){
  done = done || function(){};

  var user = context.getStore('UserStore').getState().user || {};
  var location = context.getStore('LocationStore')
                        .getState().currentLocation;

  var coords = {};
  if(location && location.isFulfilled()){
    coords =  location.valueOf().coords;
  }

  var search = apiCall(_.extend({
    userID: user.id,
    latitude: coords.latitude,
    longitude: coords.longitude
  }, payload));

  context.dispatch('SUBMIT_FEEDBACK_START', { attrs: payload, search: search });

  search
    .then(function(res){
      context.dispatch('SUBMIT_FEEDBACK_SUCCESS',
                       { attrs: payload, result: res });
      done(null);
    })
    .fail(function(err){
      context.dispatch('SUBMIT_FEEDBACK_FAILURE',
                       { attrs: payload, error: err });
      done(err);
    });
}

module.exports = submitFeedback;
