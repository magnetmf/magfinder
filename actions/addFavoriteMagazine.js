'use strict';

var _ = require('lodash');
var UserStore = require('../stores/UserStore');
var Q = require('q');
var request = require('superagent');
var addIntent = require('./addIntent');
var navigateAction = require('flux-router-component').navigateAction;
var config = require('../configs/public');
var LocationStore = require('../stores/LocationStore');
var getCurrentLocationAction = require('../actions/getCurrentLocation');
var formatMagazines = require('../utils/MagazineFormatter').formatMagazines;

function getCurrentLocation(context){
  var currentLocation = context.getStore(LocationStore).getState()
                          .currentLocation;

  if(Q.isPromiseAlike(currentLocation)){
    return currentLocation;
  }

  var deferred = Q.defer();

  context.executeAction(getCurrentLocationAction, {}, function(err, location){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(location); }
  });

  return deferred.promise;
}

function doNothing(){}

function apiCall(options){
  var deferred = Q.defer();

  var url = config.apiURL + '/users/' + options.userID +
            '/favorite_magazines/' + options.magazineID;
  var queryParams = _.pick(options.location.coords, ['latitude', 'longitude']);

  request
    .put(url)
    .query(queryParams)
    .end(function(err, res){
      if(err || res.status !== 200){
        deferred.reject(err || res.status);
      }
      else {
        deferred.resolve(formatMagazines([res.body], options.location)[0]);
      }
    });

  return deferred.promise;
}

var addFavoriteMagazine = function addFavoriteMagazine(context, payload, done){
  done = done || function(){};

  var user = context.getStore(UserStore).getState().user;

  if(user){
    getCurrentLocation(context).then(function(location){

      context.dispatch('FAVORITE_MAGAZINE_START', payload.id);
      apiCall({ userID: user.id, magazineID: payload.id, location: location })
        .then(function(result){
          context.dispatch('FAVORITE_MAGAZINE_SUCCESS', result);
          done(null, result);
        })
        .fail(function(err){
          context.dispatch('FAVORITE_MAGAZINE_FAILURE', err);
          done(err);
        });
    });
  }
  else {
    context.executeAction(addIntent, {
      action: 'addFavoriteMagazine',
      data: { id: payload.id }
    }, doNothing);
    context.executeAction(navigateAction, {
      url: context.router.makePath('signup')
    }, doNothing);
    done();
  }
};

module.exports = addFavoriteMagazine;
