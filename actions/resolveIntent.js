"use strict";

var cookies = require('cookies-js');
var UserStore = require('../stores/UserStore');
var Q = require('q');
var addFavoriteMagazine = require('./addFavoriteMagazine');
var navigateAction = require('flux-router-component').navigateAction;

function doNothing(){}

var INTENT_RESOLVERS = {

  addFavoriteMagazine: function(context, payload){
    var deferred = Q.defer();
    context.executeAction(navigateAction, {
      url: context.router.makePath('magazine', {id: payload.id})
    }, doNothing);
    context.executeAction(addFavoriteMagazine, {id: payload.id}, function(err){
      if(err){ deferred.reject(err); }
      else { deferred.resolve(); }
    });
    return deferred.promise;
  }

};

function executeIntentAction(context){
  var user = context.getStore(UserStore).getState().user;

  var intent;
  try {
    intent = JSON.parse(cookies.get('intent'));
  } catch(e){
    intent = null;
  }

  if(user && intent){
    return INTENT_RESOLVERS[intent.action](context, intent.data);
  }
  else {
    var deferred = Q.defer();
    deferred.resolve();
    return deferred.promise;
  }
}

var resolveIntent = function resolveIntent(context, payload, done){
  done = done || function(){};

  executeIntentAction(context)
    .fin(function(){
      cookies.set('intent', undefined);
      done();
    });
};

module.exports = resolveIntent;
