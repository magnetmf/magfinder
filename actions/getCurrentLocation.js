/* jshint browser: true */
/* globals google: false */

'use strict';

var Q = require('q');
var _ = require('lodash');
var LocationStore = require('../stores/LocationStore');
var request = require('superagent');
var config = require('../configs/public');

function getLocationByGeoLocation(context, deferred, done){
  if(typeof navigator === 'undefined' || !navigator.geolocation){
    deferred.reject('geolocation not supported');
    context.dispatch('GEOLOCATION_SUPPORTED', false);
    return done();
  }

  context.dispatch('GEOLOCATION_SUPPORTED', true);

  navigator.geolocation.getCurrentPosition(
    function onSuccess(location){
      // we have coords, but we need address info (country, state, city, etc)
      var geocoder = new google.maps.Geocoder();
      var latlng = new google.maps.LatLng(location.coords.latitude,
                                          location.coords.longitude);

      geocoder.geocode({'latLng': latlng}, function(results, status) {
        if(status == google.maps.GeocoderStatus.OK && results[0]) {
          _.each(results[0].address_components, function(comp){
            switch(comp.types[0]){
              case 'country':
                location.country = comp.short_name;
                break;
              case 'locality':
                location.city = comp.long_name;
                break;
              case 'administrative_area_level_1':
                location.state = comp.long_name;
                break;
              case 'administrative_area_level_2':
                location.county = location.county || comp.long_name;
                break;
              case 'administrative_area_level_3':
                location.county = comp.long_name;
                break;
              case 'postal_code':
              case 'postal_code_prefix':
                location.zipCode = comp.long_name;
                break;
            }
          });
          deferred.resolve(location);
          context.dispatch('GET_CURRENT_LOCATION_SUCCESS', location);
          done(null, location);
        }
        else {
          var reason = 'Reverse geocoding failed with status ' + status;
          deferred.reject(reason);
          context.dispatch('GET_CURRENT_LOCATION_FAILURE', reason);
          done();
        }
      });
    },
    function onError(reason){
      deferred.reject(reason);
      context.dispatch('GET_CURRENT_LOCATION_FAILURE', reason);
      done();
    }
  );
}

function getLocationByZipCode(context, zipCode, deferred, done){
  var url = config.apiURL + '/zipcodes/' + zipCode;
  request.get(url).end(function(err, res){
    if(err || res.status !== 200){
      var reason = err || res.status;
      deferred.reject(reason);
      done();
      context.dispatch('GET_CURRENT_LOCATION_FAILURE', reason);
    }
    else {
      var location = res.body;
      location.coords = _.pick(location, ['latitude', 'longitude']);
      deferred.resolve(location);
      done(null, location);
      context.dispatch('GET_CURRENT_LOCATION_SUCCESS', location);
    }
  });
}

module.exports = function getCurrentLocation(context, payload, done){
  var state = context.getStore(LocationStore).getState();

  if(state.currentLocation){
    return done(null, state.currentLocation);
  }

  var deferred = Q.defer();

  context.dispatch('GET_CURRENT_LOCATION_START', deferred.promise);

  var zipCode = state.zipCode;
  if(_.isEmpty(zipCode)){
    getLocationByGeoLocation(context, deferred, done);
  }
  else {
    getLocationByZipCode(context, zipCode, deferred, done);
  }
};
