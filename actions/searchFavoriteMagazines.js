"use strict";

var _ = require('lodash');
var Q = require('q');
var config = require('../configs/public');
var request = require('superagent');
var UserStore = require('../stores/UserStore');
var LocationStore = require('../stores/LocationStore');
var getCurrentLocationAction = require('../actions/getCurrentLocation');
var formatMagazines = require('../utils/MagazineFormatter').formatMagazines;

function getCurrentLocation(context){
  var currentLocation = context.getStore(LocationStore).getState()
                          .currentLocation;

  if(Q.isPromiseAlike(currentLocation)){
    return currentLocation;
  }

  var deferred = Q.defer();

  context.executeAction(getCurrentLocationAction, {}, function(err, location){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(location); }
  });

  return deferred.promise;
}

function apiCall(options){
  var deferred = Q.defer();

  var url = config.apiURL + '/users/' + options.user.id + '/favorite_magazines';
  var queryParams = _.pick(options.location.coords, ['latitude', 'longitude']);

  request
    .get(url)
    .query(queryParams)
    .end(function(err, res){
      if(err || res.status !== 200){
        deferred.reject(err || res.status);
      }
      else {
        deferred.resolve(formatMagazines(res.body, options.location));
      }
    });

  return deferred.promise;
}

var searchFavoriteMagazines = function searchFavoriteMagazines(context, payload, done){
  done = done || function(){};

  var user = context.getStore(UserStore).getState().user;
  if(!user){ return done(); }

  getCurrentLocation(context).then(function(location){

    if(context.getStore(UserStore).getFavoriteMagazines()){
      return done();
    }

    var search = apiCall({ user: user, location: location });

    context.dispatch('SEARCH_FAVORITE_MAGAZINES_START', search);

    search
      .then(function(){
        context.dispatch('SEARCH_FAVORITE_MAGAZINES_SUCCESS', search);
        done(null);
      })
      .fail(function(err){
        context.dispatch('SEARCH_FAVORITE_MAGAZINES_FAILURE', err);
        done(err);
      });

  });
};

module.exports = searchFavoriteMagazines;
