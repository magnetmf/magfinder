'use strict';

var LocationStore = require('../stores/LocationStore');
var config = require('../configs/public');

var expireCurrentLocation = function expireCurrentLocation(
  context, payload, done
){
  var state = context.getStore(LocationStore).getState();

  if(!state.zipCode &&
    state.currentLocation && state.currentLocation.updatedAt &&
    Date.now() - state.currentLocation.updatedAt > config.locationExpirationTime
  ){
    context.dispatch('EXPIRE_CURRENT_LOCATION', {});
  }

  done();
};

module.exports = expireCurrentLocation;
