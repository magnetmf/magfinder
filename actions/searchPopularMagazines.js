'use strict';

var Q = require('q');
var _ = require('lodash');
var request = require('superagent');
var LocationStore = require('../stores/LocationStore');
var getCurrentLocationAction = require('../actions/getCurrentLocation');
var config = require('../configs/public');
var formatMagazines = require('../utils/MagazineFormatter').formatMagazines;

function getCurrentLocation(context){
  var currentLocation = context.getStore(LocationStore).getState()
                          .currentLocation;

  if(Q.isPromiseAlike(currentLocation)){
    return currentLocation;
  }

  var deferred = Q.defer();

  context.executeAction(getCurrentLocationAction, {}, function(err, location){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(location); }
  });

  return deferred.promise;
}

var searchApiCall = _.memoize(
  function searchApiCall(options){
    var deferred = Q.defer();

    var url = config.apiURL + '/magazines';
    var queryParams = _.extend(
      { popular: true },
      _.pick(options.location.coords, ['latitude', 'longitude'])
    );

    request
      .get(url)
      .query(queryParams)
      .end(function(err, res){
        if(err || res.status !== 200){
          deferred.reject(err || res.status);
        }
        else {
          deferred.resolve(formatMagazines(res.body, options.location));
        }
      });

    return deferred.promise;
  },
  function resolver(options){
    return JSON.stringify(options);
  }
);

var searchPopularMagazines = function searchPopularMagazines(context, payload, done){
  var dispatch = context.dispatch;
  var event = 'SEARCH_POPULAR_MAGAZINES';

  payload = payload || {};

  getCurrentLocation(context).then(function(location){
    var search = searchApiCall({ location: location });

    dispatch(event + '_START', { results: search });

    search
      .then(function(){
        var result = { results: search };
        dispatch(event + '_SUCCESS', result);
        done(null, result);
      })
      .fail(function(err){
        dispatch(event + '_FAILURE', err);
        done(err);
      });
  });
};

module.exports = searchPopularMagazines;
