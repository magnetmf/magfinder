"use strict";

var Q = require('q');
var config = require('../configs/public');
var request = require('superagent');
var UserStore = require('../stores/UserStore');

function apiCall(attrs){
  var deferred = Q.defer();

  var url = config.apiURL + '/users/';

  request
    .post(url)
    .set('Content-Type', 'application/json')
    .send(JSON.stringify(attrs))
    .end(function(err, res){
      if(err){
        deferred.reject({ status: 500, message: 'Internal Server Error' });
      }
      else if(res.status !== 200){
        deferred.reject({ status: res.status, message: res.text });
      }
      else {
        deferred.resolve(res.body);
      }
    });

  return deferred.promise;
}

function createUser(context, payload, done){
  done = done || function(){};

  var user = context.getStore(UserStore).getUser();
  if(user){
    return done();
  }

  var search = apiCall(payload);

  context.dispatch('CREATE_USER_START', search);

    search.then(function(user){
      context.dispatch('CREATE_USER_SUCCESS', user);
      done(null);
    })
    .fail(function(err){
      context.dispatch('CREATE_USER_FAILURE', err);
      done(err);
    });
}

module.exports = createUser;
