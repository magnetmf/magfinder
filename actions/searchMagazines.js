'use strict';

var Q = require('q');
var _ = require('lodash');
var request = require('superagent');
var LocationStore = require('../stores/LocationStore');
var getCurrentLocationAction = require('../actions/getCurrentLocation');
var config = require('../configs/public');
var formatMagazines = require('../utils/MagazineFormatter').formatMagazines;
var currentSearch;

function getCurrentLocation(context){
  var currentLocation = context.getStore(LocationStore).getState()
                          .currentLocation;

  if(Q.isPromiseAlike(currentLocation)){
    return currentLocation;
  }

  var deferred = Q.defer();

  context.executeAction(getCurrentLocationAction, {}, function(err, location){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(location); }
  });

  return deferred.promise;
}

function searchApiCall(options){
  var deferred = Q.defer();

  var url = config.apiURL;
  var queryParams = _.extend(
    { distance: options.distance },
    _.pick(options.location.coords, ['latitude', 'longitude'])
  );

  if(options.searchStr){
    url += '/magazines';
    queryParams.query = options.searchStr;
  }
  else if(options.magazineId){
    url += '/magazines/' + options.magazineId;
  }
  else if(options.categoryName){
    var categoryName = options.categoryName.replace(/\//g, '%2F');
    if(options.dealerId){
      url += '/dealers/' + options.dealerId + '/categories/' + categoryName +
             '/magazines';
    }
    else {
      url += '/categories/' + categoryName + '/magazines';
    }
  }
  else {
    throw('searchStr, magazineId or categoryName should be supplied.');
  }

  currentSearch = request
    .get(url)
    .query(queryParams)
    .end(function(err, res){
      currentSearch = null;
      if(err || res.status !== 200){
        deferred.reject(err || res.status);
      }
      else {
        deferred.resolve(formatMagazines(res.body, options.location));
      }
    });

  return deferred.promise;
}

function cancelPreviousSearch(){
  if(currentSearch){
    currentSearch.abort();
    currentSearch = null;
  }
}

var searchMagazines = function searchMagazines(context, payload, done){
  payload = _.extend({ trigger: true }, payload);

  var dispatch = (payload.trigger) ? context.dispatch : function(){};
  var event = 'SEARCH_MAGAZINES';

  payload = payload || {};

  if(!payload.searchStr && !payload.magazineId && !payload.categoryName) {
    dispatch(event + '_SUCCESS', {
      searchStr: '',
      results: Q.resolve([])
    });
    return done(null, []);
  }

  getCurrentLocation(context).then(function(location){
    cancelPreviousSearch();

    var search = searchApiCall({
      location: location,
      searchStr: payload.searchStr,
      magazineId: payload.magazineId,
      categoryName: payload.categoryName,
      dealerId: payload.dealerId,
      distance: payload.distance
    });

    var searchStr = payload.searchStr || payload.magazineId ||
                    payload.categoryName;

    dispatch(event + '_START', { searchStr: searchStr, results: search });

    search
      .then(function(){
        var result = { searchStr: searchStr, results: search };
        dispatch(event + '_SUCCESS', result);
        done(null, result);
      })
      .fail(function(err){
        dispatch(event + '_FAILURE', err);
        done(err);
      });
  });
};

module.exports = searchMagazines;
