"use strict";

var Q = require('q');
var config = require('../configs/public');
var request = require('superagent');
var UserStore = require('../stores/UserStore');

function apiCall(user, attrs){
  var deferred = Q.defer();

  var url = config.apiURL + '/users/' + user.id + '/phone';

  request
    .get(url)
    .end(function(err, res){
      if(err || res.status !== 200){
        deferred.reject(err || res.status);
      }
      else {
        deferred.resolve(res.body);
      }
    });

  return deferred.promise;
}

var searchUserPhone = function searchUserPhone(context, payload, done){
  done = done || function(){};

  var user = context.getStore(UserStore).getState().user;

  if(!user || !user.id){ return done(); }

  var search = apiCall(user, payload);

  context.dispatch('SEARCH_USER_PHONE_START', search);

  search
    .then(function(){
      context.dispatch('SEARCH_USER_PHONE_SUCCESS', search);
      done(null);
    })
    .fail(function(err){
      context.dispatch('SEARCH_USER_PHONE_FAILURE', err);
      done(err);
    });
};

module.exports = searchUserPhone;
