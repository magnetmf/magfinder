"use strict";

var Q = require('q');
var config = require('../configs/public');
var request = require('superagent');
var UserStore = require('../stores/UserStore');

function apiCall(user, attrs){
  var deferred = Q.defer();

  var url = config.apiURL + '/users/' + user.id + '/refresh_phone_verification_code';

  request
    .post(url)
    .set('Content-Type', 'application/json')
    .end(function(err, res){
      if(err || res.status !== 200){
        deferred.reject(err || res.status);
      }
      else {
        deferred.resolve(res.body);
      }
    });

  return deferred.promise;
}

function refreshPhoneVerificationCode(context, payload, done){
  done = done || function(){};

  var user = context.getStore(UserStore).getState().user;

  if(!user){ return done(); }

  var req = apiCall(user);

  context.dispatch('GENERATE_PHONE_VERIFICATION_CODE_START',
                   { request: req });

  req
    .then(function(res){
      context.dispatch('GENERATE_PHONE_VERIFICATION_CODE_SUCCESS');
      done(null);
    })
    .fail(function(err){
      context.dispatch('GENERATE_PHONE_VERIFICATION_CODE_FAILURE');
      done(err);
    });
}

module.exports = refreshPhoneVerificationCode;
