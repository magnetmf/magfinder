/* jshint browser: true */
/* globals google: false */

'use strict';

var _ = require('lodash');
var Q = require('q');
var LocationStore = require('../stores/LocationStore');
var DealerStore = require('../stores/DealerStore');
var getCurrentLocationAction = require('./getCurrentLocation');
var getDealerAction = require('./getDealer');

function getCurrentLocation(context){
  var currentLocation = context.getStore(LocationStore).getState()
                          .currentLocation;

  if(Q.isPromiseAlike(currentLocation)){
    return currentLocation;
  }

  var deferred = Q.defer();

  context.executeAction(getCurrentLocationAction, {}, function(err, location){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(location); }
  });

  return deferred.promise;
}

function getDealer(context, dealerId){
  var deferred = Q.defer();
  var dealer = context.getStore(DealerStore).getDealer(dealerId);
  if(dealer){
    deferred.resolve(dealer);
  }
  else {
    context.executeAction(getDealerAction, dealerId, function(err, dealer){
      if(err){ deferred.reject(err); }
      else { deferred.resolve(dealer); }
    });
  }
  return deferred.promise;
}

function getDirections(context, payload, done){
  var currentLocation;
  getCurrentLocation(context).then(function(location){
    currentLocation = location;
    return getDealer(context, payload.dealerId);
  })
    .then(function(dealer){
      var deferred = Q.defer();
      context.dispatch('GET_DIRECTIONS_START', deferred.promise);

      var DirectionsService = new google.maps.DirectionsService();
      var origin = new google.maps.LatLng(currentLocation.coords.latitude,
                                          currentLocation.coords.longitude);
      var destination = new google.maps.LatLng(dealer.latitude,
                                               dealer.longitude);

      DirectionsService.route({
        origin: origin,
        destination: destination,
        travelMode: google.maps.TravelMode.DRIVING
      }, function(result, status){
        result.origin = origin;
        result.destination = destination;

        if(status == google.maps.DirectionsStatus.OK) {
          deferred.resolve(result);
          context.dispatch('GET_DIRECTIONS_SUCCESS', result);
          done(null, result);
        }
        else {
          deferred.reject(result);
          context.dispatch('GET_DIRECTIONS_FAILURE', result);
          done(result);
        }
      });
    })
    .fail(function(err){
      var msg = 'error fetching location or dealer';
      context.dispatch('GET_DIRECTIONS_FAILURE', msg);
      return done(msg);
    });
}

module.exports = getDirections;
