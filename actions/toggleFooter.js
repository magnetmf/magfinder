'use strict';

var toggleFooter = function toggleFooter(context, payload, done){
  context.dispatch('TOGGLE_FOOTER', {});
  done();
};

module.exports = toggleFooter;
