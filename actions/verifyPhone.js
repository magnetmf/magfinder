"use strict";

var Q = require('q');
var config = require('../configs/public');
var request = require('superagent');
var UserStore = require('../stores/UserStore');

function apiCall(user, attrs){
  var deferred = Q.defer();

  var url = config.apiURL + '/users/' + user.id + '/verify_phone';

  request
    .post(url)
    .set('Content-Type', 'application/json')
    .send(JSON.stringify(attrs))
    .end(function(err, res){
      if(err || res.status !== 200){
        deferred.reject(err || res.status);
      }
      else {
        deferred.resolve(res.body);
      }
    });

  return deferred.promise;
}

function updateUserPhone(context, payload, done){
  done = done || function(){};

  var user = context.getStore(UserStore).getState().user;

  if(!user){ return done(); }

  var search = apiCall(user, payload);

  context.dispatch('VERIFY_PHONE_START', {
    verificationCode: payload.verificationCode,
    search: search
  });

  search
    .then(function(res){
      context.dispatch('VERIFY_PHONE_SUCCESS', {
        verificationCode: payload.verificationCode,
        result: res
      });
      done(null);
    })
    .fail(function(err){
      context.dispatch('VERIFY_PHONE_FAILURE', {
        verificationCode: payload.verificationCode,
        error: err
      });
      done(err);
    });
}

module.exports = updateUserPhone;
