"use strict";

var Q = require('q');
var config = require('../configs/public');
var request = require('superagent');
var UserStore = require('../stores/UserStore');

function apiCall(user, attrs){
  var deferred = Q.defer();

  var url = config.apiURL + '/users/' + user.id + '/email';

  request
    .post(url)
    .set('Content-Type', 'application/json')
    .send(JSON.stringify(attrs))
    .end(function(err, res){
      if(err || res.status !== 200){
        deferred.reject(err || res.status);
      }
      else {
        deferred.resolve(res.body);
      }
    });

  return deferred.promise;
}

function updateUserEmail(context, payload, done){
  done = done || function(){};

  context.dispatch('UPDATE_USER_EMAIL_START');

  var user = context.getStore(UserStore).getState().user;

  apiCall(user, payload)
    .then(function(res){
      context.dispatch('UPDATE_USER_EMAIL_SUCCESS', res);
      done(null);
    })
    .fail(function(err){
      context.dispatch('UPDATE_USER_EMAIL_FAILURE', err);
      done(err);
    });
}

module.exports = updateUserEmail;
