'use strict';

var Q = require('q');
var _ = require('lodash');
var request = require('superagent');
var config = require('../configs/public');
var LocationStore = require('../stores/LocationStore');
var getCurrentLocationAction = require('../actions/getCurrentLocation');
var currentSearch;

function getCurrentLocation(context){
  var currentLocation = context.getStore(LocationStore).getState()
                          .currentLocation;

  if(Q.isPromiseAlike(currentLocation)){
    return currentLocation;
  }

  var deferred = Q.defer();

  context.executeAction(getCurrentLocationAction, {}, function(err, location){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(location); }
  });

  return deferred.promise;
}

function cancelPreviousSearch(){
  if(currentSearch){
    currentSearch.abort();
    currentSearch = null;
  }
}

function searchApiCall(options){
  var deferred = Q.defer();

  var url = config.apiURL + '/dealers';
  var queryParams = _.pick(options.location.coords, ['latitude', 'longitude']);

  currentSearch = request
    .get(url)
    .query(queryParams)
    .end(function(err, res){
      currentSearch = null;
      if(err || res.status !== 200){
        deferred.reject(err || res.status);
      }
      else {
        deferred.resolve(res.body);
      }
    });

  return deferred.promise;
}

var searchDealers = function searchDealers(context, payload, done){
  getCurrentLocation(context).then(function(location){
    cancelPreviousSearch();

    var search = searchApiCall({
      location: location
    });

    context.dispatch('SEARCH_DEALERS_START', search);

    search
      .then(function(){
        context.dispatch('SEARCH_DEALERS_SUCCESS', search);
        done(null, search);
      })
      .fail(function(err){
        context.dispatch('SEARCH_DEALERS_FAILURE', err);
        done(err);
      });
  });
};

module.exports = searchDealers;
