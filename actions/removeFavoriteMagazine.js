'use strict';

var _ = require('lodash');
var UserStore = require('../stores/UserStore');
var Q = require('q');
var request = require('superagent');
var addIntent = require('./addIntent');
var config = require('../configs/public');
var navigateAction = require('flux-router-component').navigateAction;

function doNothing(){}

function apiCall(options){
  var deferred = Q.defer();

  var url = config.apiURL + '/users/' + options.userID +
            '/favorite_magazines/' + options.magazineID;

  request
    .del(url)
    .end(function(err, res){
      if(err || res.status !== 204){
        deferred.reject(err || res.status);
      }
      else {
        deferred.resolve();
      }
    });

  return deferred.promise;
}

var removeFavoriteMagazine = function removeFavoriteMagazine(
  context, payload, done
){
  done = done || doNothing;

  var user = context.getStore(UserStore).getState().user;

  if(user){
    context.dispatch('REMOVE_FAVORITE_MAGAZINE_START', payload.id);
    apiCall({ userID: user.id, magazineID: payload.id })
      .then(function(result){
        context.dispatch('REMOVE_FAVORITE_MAGAZINE_SUCCESS', payload.id);
        done(null, result);
      })
      .fail(function(err){
        context.dispatch('REMOVE_FAVORITE_MAGAZINE_FAILURE', err);
        done(err);
      });
  }
  else {
    context.executeAction(addIntent, {
      action: 'removeFavoriteMagazine',
      data: { id: payload.id }
    }, doNothing);
    context.executeAction(navigateAction, {
      url: context.router.makePath('signup')
    }, doNothing);
    done();
  }
};

module.exports = removeFavoriteMagazine;
