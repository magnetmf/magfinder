'use strict';

var Q = require('q');
var _ = require('lodash');
var request = require('superagent');
var config = require('../configs/public');
var LocationStore = require('../stores/LocationStore');
var getCurrentLocationAction = require('./getCurrentLocation');
var currentSearch;

function getCurrentLocation(context){
  var currentLocation = context.getStore(LocationStore).getState()
                          .currentLocation;

  if(Q.isPromiseAlike(currentLocation)){
    return currentLocation;
  }

  var deferred = Q.defer();

  context.executeAction(getCurrentLocationAction, {}, function(err, location){
    if(err){ deferred.reject(err); }
    else { deferred.resolve(location); }
  });

  return deferred.promise;
}

function searchIsRunning(options){
  return currentSearch && _.isEqual(currentSearch.dealerId, options.dealerId) &&
         _.isEqual(currentSearch.location.coords, options.location.coords);
}

function searchApiCall(options){
  var deferred = Q.defer();

  var url = config.apiURL + '/categories';
  if(options.dealerId){
    url += '?dealerId=' + options.dealerId;
  }
  var queryParams = _.pick(options.location.coords, ['latitude', 'longitude']);

  currentSearch = request
    .get(url)
    .query(queryParams)
    .end(function(err, res){
      currentSearch = null;
      if(err || res.status !== 200){
        deferred.reject(err || res.status);
      }
      else {
        deferred.resolve(res.body);
      }
    });

  currentSearch.location = options.location;
  currentSearch.dealerId = options.dealerId;

  return deferred.promise;
}

var searchCategories = function searchCategories(context, payload, done){
  getCurrentLocation(context).then(function(location){
    var options = {
      location: location,
      dealerId: payload.dealerId
    };

    if(searchIsRunning(options)){ return done(null, currentSearch); }

    var search = searchApiCall(options);
    search.dealerId = options.dealerId;

    context.dispatch('SEARCH_CATEGORIES_START', search);

    search
      .then(function(){
        context.dispatch('SEARCH_CATEGORIES_SUCCESS', search);
        done(null, search);
      })
      .fail(function(err){
        context.dispatch('SEARCH_CATEGORIES_FAILURE', err);
        done(err);
      });
  });
};

module.exports = searchCategories;
