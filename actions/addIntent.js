"use strict";

var cookies = require('cookies-js');

var addIntent = function addIntent(context, payload, done){
  done = done || function(){};

  // store intent as a cookie
  cookies.set('intent', JSON.stringify({
    action: payload.action,
    data: payload.data
  }));

  done();
};

module.exports = addIntent;
