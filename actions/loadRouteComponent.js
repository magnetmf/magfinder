/* jshint browser: true */

'use strict';

// polyfill require.ensure
if(typeof require.ensure !== "function"){
  require.ensure = function(d, c) { return c(require); };
}

var routeComponents = {
  home: function(route, context, cb){
    require.ensure([], function(require){
      cb(null, require('../components/home/Home.jsx'));
    });
  },

  video: function(route, context, cb){
    require.ensure([], function(require){
      cb(null, require('../components/video/Video.jsx'));
    });
  },

  about: function(route, context, cb){
    require.ensure([], function(require){
      cb(null, require('../components/about/About.jsx'));
    });
  },

  browse: function(route, context, cb){
    require.ensure([], function(require){
      cb(null, require('../components/browse/Browse.jsx'));
    });
  },

  magazine: function(route, context, cb){
    require.ensure([], function(require){
      cb(null, require('../components/magazine/Magazine.jsx'));
    });
  },

  category: function(route, context, cb){
    require.ensure([], function(require){
      cb(null, require('../components/category/Category.jsx'));
    });
  },

  dealers: function(route, context, cb){
    require.ensure([], function(require){
      cb(null, require('../components/dealers/Dealers.jsx'));
    });
  },

  directions: function(route, context, cb){
    require.ensure([], function(require){
      cb(null, require('../components/directions/Directions.jsx'));
    });
  },

  search: function(route, context, cb){
    require.ensure([], function(require){
      cb(null, require('../components/search/SearchResults.jsx'));
    });
  },

  feedback: function(route, context, cb){
    require.ensure([], function(require){
      cb(null, require('../components/feedback/Feedback.jsx'));
    });
  },

  signup: function(route, context, cb){
    require.ensure([], function(require){
      cb(null, require('../components/user/SignupIndex.jsx'));
    });
  },

  signupWithEmail: function(route, context, cb){
    require.ensure([], function(require){
      cb(null, require('../components/user/SignupWithEmail.jsx'));
    });
  },

  account: function(route, context, cb){
    require.ensure([], function(require){
      cb(null, require('../components/user/Account.jsx'));
    });
  },

  verifyPhone: function(route, context, cb){
    require.ensure([], function(require){
      cb(null, require('../components/user/VerifyPhone.jsx'));
    });
  }
};

module.exports = function getRouteComponent(context, route, done){
  if(!route || !route.name){
    route = context.getStore('RouteStore').getState().route;
  }

  context.dispatch('LOAD_ROUTE_COMPONENT_START', {route: route});

  routeComponents[route.name].call(null, route, context,
  function(err, component){
    if(err){ return done(err); }

    if(component){
      context.dispatch('LOAD_ROUTE_COMPONENT_SUCCESS', component);
      done();
    }
    else {
      done('component not found');
    }
  });

};
