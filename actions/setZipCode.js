'use strict';

var LocationStore = require('../stores/LocationStore');

var setZipCode = function setZipCode(context, payload, done){
  var currentZipCode = context.getStore(LocationStore).getState().zipCode;
  if(currentZipCode !== payload.zipCode){
    context.dispatch('NEW_ZIP_CODE', payload.zipCode);
  }
  done();
};

module.exports = setZipCode;
