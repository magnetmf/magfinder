'use strict';

var resetNotifications = function resetNotifications(context, payload, done){
  context.dispatch('RESET_NOTIFICATIONS');
  done();
};

module.exports = resetNotifications;
