function moduleCalledDirectly(){
  // returns true if module was called directly
  // (ie not required by another module)
  return require.main === module;
}

if(moduleCalledDirectly()){
  // load env variables from .env file
  var path = require('path');
  require('dotenv').load({ path: path.join(__dirname, '../.env') });
}

var Q = require('q');
var _ = require('lodash');
var CronJob = require('cron').CronJob;
var debug = require('debug')('cronjs');
var config = require('../configs/private');
var User = require('../api/models/User');
var client = require('twilio')(config.twilioAccountSid, config.twilioAuthToken);

function sendNotification(notificationData){
  var deferred = Q.defer();
  var magazines = _.pluck(notificationData.magazines, 'name');
  var msg = "Exciting news!\n\nThe title(s) you asked to be notified about now have new issues available in store.\n\nPlease check out MagFinder to pick up the new issues for:\n" + magazines.join('\n');

  // send SMS
  client.sendMessage({
    to: notificationData.phoneNumber,
    from: config.twilioPhoneNumber,
    body: msg
  },function(err, responseData){
    if(err){
      deferred.reject(err);
    } else {
      debug('New magazine notification sent.', JSON.stringify(responseData));
      deferred.resolve();
    }
  });

  return deferred.promise;
};

function sendNewMagazineNotifications(){
  debug('Starting sendNewMagazineNotifications task');

  return User.getNewMagazineNotifications()
    .then(function(notifications){
      debug('Amount of users to notify:', notifications.length);
      return Q.all(_.map(notifications, sendNotification));
    })
    .then(function(){
      debug('Finished sendNewMagazineNotifications task successfully');
    })
    .fail(function(err){
      debug('Error trying to sendNewMagazineNotifications', err);
      throw err;
    });
}

exports.run = function(options){
  var options = _.extend({}, options);
  if(!options.cronTime){
    // just run once
    sendNewMagazineNotifications();
  }
  else {
    try {
      new CronJob({
        cronTime: options.cronTime,
        onTick: sendNewMagazineNotifications,
        start: true,
        timeZone: 'America/Los_Angeles'
      });
      debug('sendNewMagazineNotifications task started with schedule: ',
                options.cronTime);
    } catch(ex){
      debug('cron pattern not valid', ex);
    }
  }
}

if(moduleCalledDirectly()){
  sendNewMagazineNotifications()
    .then(function(){ process.exit(); })
    .fail(function(err){ process.exit(1); });
}
