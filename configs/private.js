/* The contents of this config file should only be used by the server, not
 * shared with the client app.
 */

'use strict';

// magnet database connection string
var magnetDbConString = process.env.DB_CON ||
                        'postgres://db_user:db_pwd@db_host/db_name';

// app database connection string
var appDbConString = process.env.DATABASE_URL ||
                     'postgres://db_user:db_pwd@db_host/db_name';

module.exports = {
  // get database connection string
  getDbConString: function(dbName){
    if(dbName === 'magnet'){
      return magnetDbConString;
    }
    if(dbName === 'app'){
      return appDbConString;
    }
    throw 'unknown db ' + dbName;
  },

  // modify database connection string (should only be used by tests)
  setDbConString: function(dbName, conString){
    if(dbName === 'magnet'){
      magnetDbConString = conString;
    }
    else if(dbName === 'app'){
      appDbConString = conString;
    }
    else {
      throw 'unknown db ' + dbName;
    }
  },

  // express session secret
  sessionSecret: process.env.SESSION_SECRET || '6382479310 3792790611',

  // facebook app secret
  fbAppSecret: process.env.FACEBOOK_APP_SECRET,

  // twitter consumer secret
  twitterConsumerSecret: process.env.TWITTER_CONSUMER_SECRET,

  // twilio account sid
  twilioAccountSid: process.env.TWILIO_ACCOUNT_SID,

  // twilio auth token
  twilioAuthToken: process.env.TWILIO_AUTH_TOKEN,

  // a number you bought from Twilio and can use for outbound communication
  twilioPhoneNumber: process.env.TWILIO_PHONE_NUMBER,

  // time interval to run the new magazine notification task.
  // should be a cron pattern as described on
  // https://github.com/ncb000gt/node-cron#available-cron-patterns
  newMagNotificationTime: process.env.NEW_MAGAZINE_NOTIFICATION_CRON_TIME
};
