"use strict";

var loadRouteComponent = require('../actions/loadRouteComponent');
var _ = require('lodash');

var baseRoute = { method: 'get', action: loadRouteComponent };

var paths = {
  home:             '/',
  about:            '/about',
  video:            '/video', 
  browse:           '/browse',
  magazine:         '/magazines/:id',
  dealers:          '/magazines/:id/dealers',
  dealer:           '/dealers/:id',
  directions:       '/directions/:dealerId',
  category:         '/categories/:id',
  search:           '/search',
  feedback:         '/feedback',
  signup:           '/signup',
  signupWithEmail:  '/signup/email',
  account:          '/account',
  verifyPhone:      '/account/verify_phone'
};

module.exports = _.reduce(paths, function(memo, routePath, routeName){
  memo[routeName] = _.extend({ path: routePath }, baseRoute);
  return memo;
}, {});
