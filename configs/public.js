/* The contents of this config file will be shared with the client app, don't
 * include any sensitive data (use the `config/private.js` file for that)
 */

'use strict';

var _ = require('lodash');

var config = {
  // host
  host: process.env.HOST || '',

  // port the server will be listening on
  port: process.env.PORT || 3000,

  apiPath: process.env.API_PATH || '/api',

  // google analytics Web Property ID
  webPropertyID: process.env.ANALYTICS_ID || 'UA-60734985-1',

  // facebook app id
  fbAppID: process.env.FACEBOOK_APP_ID,

  // twitter consumer key
  twitterConsumerKey: process.env.TWITTER_CONSUMER_KEY,

  // Google Maps API Key
  googleApiKey: process.env.GOOGLE_API_KEY,

  // default distance (in miles) to use when filtering out magazines
  defaultSearchRadius: process.env.DEFAULT_SEARCH_RADIUS || 10,

  // time (in milliseconds) before location is removed from cache
  locationExpirationTime: process.env.LOCATION_EXPIRATION_TIME || 5 * 60 * 1000 // 5 minutes
};

if (config.host !== '') {
  config.siteURL = config.host + ':' + config.port;
} else {
  config.siteURL = '';
}

config.apiURL = config.siteURL + config.apiPath;

if(typeof window !== 'undefined'){
  /* global window:false */
  config = _.extend(config, window.config);
}

module.exports = config;
