# MagFinder

Rewrite of [http://magfinder.magnetdata.net](
http://magfinder.magnetdata.net) using a mobile-first approach.

# Getting Started
- Install [Node.js](http://nodejs.org/)
- Install [PostgreSQL](http://www.postgresql.org/)
- Install project dependencies with `npm install`
- Create an empty postgres database called `magfinder` with username `magfinder_user` and password `magfinder_pwd`.
- Make sure to run the file `migrations/required_db_extensions.sql` against the `magfinder` db.
```
cat migrations/required_db_extensions.sql | psql -d magfinder
```
- Create an `.env` file on the root of the project with the values found on the Development section of [Deployment Env Variables](https://brandedcrate.atlassian.net/wiki/display/MAG/Deployment+Env+Variables).
- Run db migrations with
```
db-migrate up
```
- You can run the app either in dev mode or in debug mode:
```
npm run dev
npm run debug
```
The server should now be running on [http://localhost:3000](http://localhost:3000)

# Deployments
The production app is hosted on AWS Elastic Beanstalk and deployed as a
Docker image. Please install the [Elastic Beanstalk command line tools](
http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-getting-set-up.html)
to manage deploys from the command line. After setting up the tools, a
one-touch deploy should be as simple as `eb deploy`

## Debugging
The app uses the [debug](https://github.com/visionmedia/debug) package.
By default, no debugging output is printed to the console. You can
change that by adding a `debug` key to the LocalStorage with whatever
value you want to debug (use `*` to output everything).

## Fluxible
This app is written using [Yahoo's Fluxible framework](http://fluxible.io/). You can find a
good intro to Fluxible on [Isomorphic React + Flux using Yahoo's Fluxible](http://dev.alexishevia.com/2014/11/isomorphic-react-flux-using-yahoos.html).

## Adding routes
To add a new route:

1. Define the route in `configs/routes.js`
2. Define the corresponding component in `actions/loadRouteComponent.js`
3. Declare how the component should be instantiated in `components/general/Application.jsx`

## Configuration
The app is configurable through environment variables. For a complete list of env variables available, check out [Deployment Env Variables](https://brandedcrate.atlassian.net/wiki/display/MAG/Deployment+Env+Variables).

## Running tests
The tests require you to have a local PostgreSQL server running and a db
user with SUPERUSER permissions. You must set the user details as
environment variables: `TEST_DB_USER`, `TEST_DB_PWD`

In case the user doesn't exist, you can create one by running:
```
export TEST_DB_USER=magnet_test_user TEST_DB_PWD=magnet_pwd
psql -c "CREATE ROLE "$TEST_DB_USER" LOGIN PASSWORD '$TEST_DB_PWD' SUPERUSER;" postgres
```

To run tests:
```
export TEST_DB_USER=magnet_test_user TEST_DB_PWD=magnet_pwd
npm run tests
npm run tests:debug
```
