'use strict';
var React = require('react');
var FluxibleApp = require('fluxible');
var routrPlugin = require('fluxible-plugin-routr');

var app = new FluxibleApp({
  appComponent: React.createFactory(require('./components/general/Application.jsx'))
});

app.plug(routrPlugin({
  routes: require('./configs/routes')
}));

app.registerStore(require('./stores/RouteStore'));
app.registerStore(require('./stores/SearchStore'));
app.registerStore(require('./stores/CategoryStore'));
app.registerStore(require('./stores/LocationStore'));
app.registerStore(require('./stores/DirectionsStore'));
app.registerStore(require('./stores/MostPopularStore'));
app.registerStore(require('./stores/ResponsiveStore'));
app.registerStore(require('./stores/DealerStore'));
app.registerStore(require('./stores/UserStore'));
app.registerStore(require('./stores/NotificationStore'));
app.registerStore(require('./stores/FeedbackStore'));

module.exports = app;
