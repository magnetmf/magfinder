"use strict";

var _ = require('lodash');
var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;
var User = require('./models/User');
var config = require('../configs/public');
var privateConfig = require('../configs/private');
var dbWrapper = require('../utils/db_wrapper');

passport.use(new FacebookStrategy({
    clientID: config.fbAppID,
    clientSecret: privateConfig.fbAppSecret,
    callbackURL: config.siteURL + "/auth/facebook/callback"
  },
  function(accessToken, refreshToken, profile, done) {
    User.findOrCreate({ facebookId: profile.id, name: profile._json.name })
      .then(function(user){
        done(null, user);
      })
      .fail(function(err){
        done(err);
      });
  }
));

passport.use(new TwitterStrategy({
    consumerKey: config.twitterConsumerKey,
    consumerSecret: privateConfig.twitterConsumerSecret,
    callbackURL: config.siteURL + "/auth/twitter/callback"
  },
  function(token, tokenSecret, profile, done) {
    User.findOrCreate({ twitterId: profile.id, name: profile._json.name })
      .then(function(user){
        done(null, user);
      })
      .fail(function(err){
        done(err);
      });
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  dbWrapper.query("SELECT id, name FROM users WHERE id = $1", [id],
                  { db: 'app' })
    .then(function(result){
      return done(null, result.rows[0]);
    })
    .fail(function(err){
      done(err);
    });
});

module.exports = passport;
