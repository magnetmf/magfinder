"use strict";

var _ = require('lodash');
var express = require('express');
var router = express.Router();
var Dealer = require('./models/Dealer');
var Magazine = require('./models/Magazine');
var hasLocation = require('./middleware/hasLocation');

router.route('/')
  .get(hasLocation, function(req, res, next){
    Dealer.list({
      latitude: req.query.latitude,
      longitude: req.query.longitude,
      distance: req.query.distance
    })
      .then(function(result){
        res.send(result);
      })
      .fail(next);
  });

router.route('/:dealerId')
  .get(hasLocation, function(req, res, next){
    Dealer.search({
      latitude: req.query.latitude,
      longitude: req.query.longitude,
      dealerId: req.params.dealerId
    })
      .then(function(result){
        res.send(result);
      })
      .fail(next);
  });

router.route('/:dealerId/categories/:name/magazines')
  .get(hasLocation, function(req,res, next){
    Magazine.search({
      dealerId: req.params.dealerId,
      category: req.params.name,
      latitude: req.query.latitude,
      longitude: req.query.longitude,
      distance: req.query.distance
    })
      .then(function(result){
        res.send(result);
      })
      .fail(next);
  });

module.exports = router;
