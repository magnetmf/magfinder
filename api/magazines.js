"use strict";

var _ = require('lodash');
var Magazine = require('./models/Magazine');
var express = require('express');
var router = express.Router();
var hasLocation = require('./middleware/hasLocation');

/*
 * search magazines by name or popularity
 */
router.get('/', hasLocation, function(req, res, next){
  if(!req.query.query && !req.query.popular){
    res.status(400).send("missing required parameter: 'query' or 'popular'");
  }

  var searchParams = {
    latitude: req.query.latitude,
    longitude: req.query.longitude,
    distance: req.query.distance
  };

  if(req.query.query){
    searchParams.query = req.query.query;
  }
  else {
    searchParams.popularity = true;
  }

  Magazine.search(searchParams)
    .then(function(result){
      res.send(result);
    })
    .fail(next);
});

/*
 * search magazines by id
 */
router.get('/:id', hasLocation, function(req, res, next){
  var id = parseInt(req.params.id, 10);
  if(_.isNaN(id)){
    res.status(400).send('id must be a number');
    return;
  }

  Magazine.search({
    id: id,
    latitude: req.query.latitude,
    longitude: req.query.longitude,
    distance: req.query.distance
  })
    .then(function(result){
      res.send(result);
    })
    .fail(next);
});

module.exports = router;
