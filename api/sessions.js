"use strict";

var Q = require('q');
var express = require('express');
var router = express.Router();
var User = require('./models/User');

router.post('/', function(req, res, next){
  User.find({ email: req.body.email, password: req.body.password })
    .then(function(user){
      if(user){
        return Q.nbind(req.login, req)(user)
          .then(function(){
            res.status(200).send(user);
          });
      }
      else {
        res.status(400).send('user_not_found');
      }
    })
    .fail(next);
});

module.exports = router;
