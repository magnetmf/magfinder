"use strict";

var _ = require('lodash');
var Q = require('q');
var express = require('express');
var router = express.Router();
var User = require('./models/User');
var Magazine = require('./models/Magazine');
var passport = require('./passport');
var hasLocation = require('./middleware/hasLocation');
var loggedIn = require('./middleware/loggedIn');

function isRequestedUser(req, res, next){
  if(req.user.id === parseInt(req.params.userID, 10)){ next(); }
  else { return res.status(403).send('Forbidden'); }
}

router.route('/')
  .post(function(req, res, next){
    User.createWithPassword(req.body)
      .then(function(user){
        return Q.nbind(req.login, req)(user)
          .then(function(){
            res.status(200).send(user);
          });
      })
      .fail(function(err){
        if(err.code === "23505"){
          res.status(400).send('unique_violation');
        }
        else {
          next(err);
        }
      });
  });

router.route('/:userID')
  .post(loggedIn, isRequestedUser, function(req, res, next){
    User.update(req.user.id, req.body)
      .then(function(user){
        res.status(200).send(user);
      })
      .fail(next);
  });

router.route('/:userID/email')
  .get(loggedIn, isRequestedUser, function(req, res, next){
    User.getEmail({ userID: req.user.id })
      .then(function(email){
        return res.status(200).send(email);
      })
      .fail(next);
  })
  .post(loggedIn, isRequestedUser, function(req, res, next){
    User.updateEmail(req.user.id, req.body.email)
      .then(function(result){
        res.status(200).send(result);
      })
      .fail(next);
  });

router.route('/:userID/phone')
  .get(loggedIn, isRequestedUser, function(req, res, next){
    User.getPhone({ userID: req.user.id })
      .then(function(phone){
        return res.status(200).send(phone);
      })
      .fail(next);
  })
  .post(loggedIn, isRequestedUser, function(req, res, next){
    User.updatePhone(req.user.id, req.body.phone)
      .then(function(result){
        res.status(200).send(result);
      })
      .fail(next);
  });

router.route('/:userID/favorite_magazines')
  .get(loggedIn, isRequestedUser, hasLocation, function(req, res, next){
    User.getFavoriteMagazines(parseInt(req.user.id, 10))
      .then(function(ids){
        if(_.isEmpty(ids)){
          return [];
        }
        else {
          return Magazine.search({
            ids: ids,
            latitude: req.query.latitude,
            longitude: req.query.longitude
          });
        }
      })
      .then(function(result){
        res.status(200).send(result);
      })
      .fail(next);
  });

router.route('/:userID/favorite_magazines/:magazineID')
  .put(loggedIn, isRequestedUser, hasLocation, function(req, res, next){
    User.addFavoriteMagazine(parseInt(req.user.id, 10),
                             parseInt(req.params.magazineID, 10))
      .then(function(){
        return Magazine.search({
          id: req.params.magazineID,
          latitude: req.query.latitude,
          longitude: req.query.longitude
        });
      })
      .then(function(result){
        res.status(200).send(result[0]);
      })
      .fail(next);
  })
  .delete(loggedIn, isRequestedUser, function(req, res, next){
    User.removeFavoriteMagazine(parseInt(req.user.id, 10),
                                parseInt(req.params.magazineID, 10))
      .then(function(result){
        res.status(204).send('');
      })
      .fail(next);
  });

router.route('/:userID/communication_preferences')
  .get(loggedIn, isRequestedUser, function(req, res, next){
    User.getCommunicationPreferences({ userID: req.user.id })
      .then(function(email){
        return res.status(200).send(email);
      })
      .fail(next);
  })
  .put(loggedIn, isRequestedUser, function(req, res, next){
    User.updateCommunicationPreferences(req.user.id, req.body)
      .then(function(result){
        res.status(200).send(result);
      })
      .fail(next);
  });

router.route('/:userID/refresh_phone_verification_code')
  .post(loggedIn, isRequestedUser, function(req, res, next){
    User.refreshPhoneVerificationCode(req.user.id)
      .then(function(verificationCode){
        res.status(200).send('The verification code was sent as an SMS');
      })
      .fail(next);
  });

router.route('/:userID/verify_phone')
  .post(loggedIn, isRequestedUser, function(req, res, next){
    User.verifyPhone(req.user.id, req.body.verificationCode)
      .then(function(result){
        res.status(200).send(result);
      })
      .fail(function(err){
        switch(err){
          case 'INVALID_CODE':
            res.status(403).send('Invalid Code');
            break;
          case 'EXPIRED':
            res.status(409).send('Code has expired. Request new code.');
            break;
          default:
            next(err);
        }
      });
  });

module.exports = router;
