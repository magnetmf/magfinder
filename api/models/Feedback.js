"use strict";

var dbWrapper = require('../../utils/db_wrapper');
var _ = require('lodash');
var multiline = require('multiline');
var Q = require('q');

var QUERIES = {

  ADD_FEEDBACK: multiline(function(){/*
    INSERT INTO feedback (
      feedback_type, magazine_title, store_name, comments, user_id,
      latitude, longitude
    )
    VALUES ($1, $2, $3, $4, $5, $6, $7)
    RETURNING id, feedback_type, magazine_title, store_name, comments, user_id,
      latitude, longitude
  */})

};

exports.create = function createFeedback(options){
  var fields = ['type', 'magazine', 'store', 'comments', 'userID',
                'latitude', 'longitude'];

  var queryStr = QUERIES.ADD_FEEDBACK;
  var args = _.chain(fields)
              .reduce(function(memo, key){
                memo[key] = null;
                return memo;
              }, {})
              .extend(options)
              .pick(fields)
              .values().value();

  return dbWrapper
    .query(queryStr, args, { db: 'app' })
    .then(function(result){
      return result.rows[0];
    });
};
