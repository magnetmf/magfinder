"use strict";

var dbWrapper = require('../../utils/db_wrapper');
var multiline = require('multiline');
var _ = require('lodash');
var Q = require('q');
var config = require('../../configs/public');
var milesToMeters = require('../../utils/unit_converter').milesToMeters;

var QUERIES = {};

QUERIES.MAGAZINES_WITHIN_DISTANCE = multiline(function(){/*
  -- returns 1 row for each magazine that is sold within the provided
  -- distance

  SELECT dim_title.titleid, dim_title.category
  FROM dim_title
  INNER JOIN fact_titledealerxr
    ON fact_titledealerxr.titleid = dim_title.titleid
  INNER JOIN dim_dealer
    ON fact_titledealerxr.dealerid = dim_dealer.dealerid
  WHERE earth_box(ll_to_earth($1, $2), $3) @>
    ll_to_earth(dim_dealer.latitude, dim_dealer.longitude)
  AND earth_distance(
    ll_to_earth(dim_dealer.latitude, dim_dealer.longitude),
    ll_to_earth($1, $2)
  ) < $3
  GROUP BY dim_title.titleid, dim_title.category
*/});

QUERIES.MAGAZINES_IN_DEALER = multiline(function(){/*
  -- returns 1 row for each magazine that is sold by a given dealer

  SELECT dim_title.titleid, dim_title.category
  FROM dim_title
  INNER JOIN fact_titledealerxr
    ON fact_titledealerxr.titleid = dim_title.titleid
  INNER JOIN dim_dealer
    ON fact_titledealerxr.dealerid = dim_dealer.dealerid
  WHERE fact_titledealerxr.dealerid = $1
*/});

// returns a list of magazines, along with the category it belongs to and the
// magazine count for that category.
// The query makes sure a max of 3 magazines are returned per category.
QUERIES.LIST = multiline(function(){/*
  SELECT
    byCategory.titleid AS id,
    byCategory.bipadname AS name,
    byCategory.coverimageurl AS cover_image_url,
    byCategory.category AS category,
    counter.count AS category_magazine_count,
    byCategory.row_number AS position_in_category
  FROM (
    -- this query gives me all magazines, partitioned by category. Each
    -- magazine is assigned a 'row_number' within its category, so I can later
    -- decide to only include the first N magazines for each category.
    -- Since I use this query to get category covers, I filter out magazines
    -- that are using the default cover.

    SELECT titleid, bipadname, coverimageurl, category, row_number()
    OVER (PARTITION BY category)
    FROM dim_title
    WHERE right(coverimageurl,28) != '/thumbnails/DefaultCover.gif'
  ) byCategory
  INNER JOIN (
    -- this query gives me the magazine count per category.
    -- __FILTER__ should be replaced by either MAGAZINES_WITHIN_DISTANCE or
    -- MAGAZINES_IN_DEALER

    SELECT sold_nearby.category, COUNT(*)
    FROM (
      ___FILTER___
    ) sold_nearby
    GROUP BY sold_nearby.category
  ) counter
  ON byCategory.category = counter.category
  WHERE byCategory.row_number < 4;
*/});

function formatMagazine(row){
  /*jshint camelcase:false */
  return {
    id: row.id,
    name: row.name,
    coverImageUrl: row.cover_image_url
  };
}

function formatCategory(row){
  /*jshint camelcase:false */
  return {
    name: row.category,
    magazineCount: parseInt(row.category_magazine_count, 10),
    magazines: [ formatMagazine(row) ]
  };
}

function groupCategories(rows){
  return _.chain(rows)
    .reduce(function(memo, row){
      if(memo[row.category]){
        memo[row.category].magazines.push(formatMagazine(row));
      }
      else {
        memo[row.category] = formatCategory(row);
      }
      return memo;
    }, {})
    .values()
    .value();
}

function getValidationError(params){
  params = params || {};

  var missing = _.difference(['latitude', 'longitude'], _.keys(params));
  if(!_.isEmpty(missing)){
    return 'missing required parameters: ' + missing.join(', ');
  }
}

exports.list = function(params){
  var error = getValidationError(params);
  if(error){ return Q.reject(error); }

  var query, args;

  if(params.dealerId){
    query = QUERIES.LIST.replace('___FILTER___',
                               QUERIES.MAGAZINES_IN_DEALER);
    args = { dealerId: params.dealerId };
  }
  else {
    query = QUERIES.LIST.replace('___FILTER___',
                               QUERIES.MAGAZINES_WITHIN_DISTANCE);
    args = {
      latitude: params.latitude,
      longitude: params.longitude,
      searchRadius: milesToMeters(params.distance || config.defaultSearchRadius)
    };
  }

  return dbWrapper.query(query, _.values(args))
    .then(function(result){
      if(_.isEmpty(result.rows) &&
         args.searchRadius && args.searchRadius < milesToMeters(50))
      {
        args.searchRadius = milesToMeters(50);
        return dbWrapper.query(query, _.values(args));
      }
      else {
        return result;
      }
    })
    .then(function(result){ return groupCategories(result.rows); });
};
