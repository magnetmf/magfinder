"use strict";

var dbWrapper = require('../../utils/db_wrapper');
var Q = require('q');
var _ = require('lodash');
var multiline = require('multiline');
var config = require('../../configs/public');
var milesToMeters = require('../../utils/unit_converter').milesToMeters;
var metersToMiles = require('../../utils/unit_converter').metersToMiles;
var request = require('superagent');

var BASE_QUERY = multiline(function(){/*
  SELECT
    dim_title.titleid AS id,
    dim_title.bipadname AS name,
    dim_title.coverpriceus AS price_us,
    dim_title.coverpriceca AS price_ca,
    dim_title.coverimageurl AS cover_image_url,
    dim_title.title_description AS description,
    dim_title.category AS category,
    dim_title.titlecategory_popindex AS popularity_in_category,
    fact_titledealerxr.titlestore_popindex AS popularity_in_store,
    (dim_title.titlecategory_popindex +
      fact_titledealerxr.titlestore_popindex) AS popularity_overall,
    dim_dealer.dealerid AS dealer_id,
    dim_dealer.dealername AS dealer_name,
    dim_dealer.address AS dealer_address,
    dim_dealer.city AS dealer_city,
    dim_dealer.state AS dealer_state,
    dim_dealer.latitude AS dealer_latitude,
    dim_dealer.longitude AS dealer_longitude,
    earth_distance(
      ll_to_earth(dim_dealer.latitude, dim_dealer.longitude),
      ll_to_earth($1, $2)
    ) AS dealer_distance_in_meters,
    dim_dealer.phoneareacode AS dealer_phone_area_code,
    dim_dealer.phonenumber AS dealer_phone_number
  FROM dim_title
  INNER JOIN fact_titledealerxr
    ON fact_titledealerxr.titleid = dim_title.titleid
  INNER JOIN dim_dealer
    ON fact_titledealerxr.dealerid = dim_dealer.dealerid

  WHERE earth_box(ll_to_earth($1, $2), $3) @>
    ll_to_earth(dim_dealer.latitude, dim_dealer.longitude)

  -- the @> operator sometimes includes points further than the specified
  -- distance, so a second check using earth_distance is recommended
  AND earth_distance(
    ll_to_earth(dim_dealer.latitude, dim_dealer.longitude),
    ll_to_earth($1, $2)
  ) < $3

  ___REPLACE_CONDITION___
  ORDER BY dealer_distance_in_meters ASC
  LIMIT 200
*/});

var SEARCH_BY_NAME_QUERY = {
  str: BASE_QUERY.replace('___REPLACE_CONDITION___',
                          'AND dim_title.bipadname ILIKE $4'),
  args: ['latitude', 'longitude', 'searchRadius', 'query']
};

var SEARCH_BY_ID_QUERY = {
  str: BASE_QUERY.replace('___REPLACE_CONDITION___',
                          'AND dim_title.titleid = $4'),
  args: ['latitude', 'longitude', 'searchRadius', 'id']
};

var SEARCH_BY_IDS_QUERY = {
  str: BASE_QUERY.replace('___REPLACE_CONDITION___',
                     'AND dim_title.titleid = ANY($4)'),
  args: ['latitude', 'longitude', 'searchRadius', 'ids']
};

var SEARCH_BY_CATEGORY_QUERY = {
  str: BASE_QUERY.replace('___REPLACE_CONDITION___',
                          'AND dim_title.category = $4'),
  args: ['latitude', 'longitude', 'searchRadius', 'category']
};

var SEARCH_BY_POPULARITY_QUERY = {
  str: BASE_QUERY
    .replace('___REPLACE_CONDITION___',
             "AND right(coverimageurl,28) != '/thumbnails/DefaultCover.gif'")
    .replace('ORDER BY dealer_distance_in_meters ASC',
             'ORDER BY popularity_overall DESC'),
  args: ['latitude', 'longitude', 'searchRadius']
};

var SEARCH_BY_DEALER_CATEGORY_QUERY = {
  str: BASE_QUERY.split('WHERE')[0] + multiline(function(){/*
    WHERE dim_dealer.dealerid = $3
    AND dim_title.category = $4
    */}),
  args: ['latitude', 'longitude', 'dealerId', 'category']
};

function getValidationError(params){
  params = params || {};
  if(!params.query && !params.id && !params.ids && !params.category &&
     !params.popularity)
  {
    return 'either query, id, ids, category or popularity must be specified';
  }

  var missing = _.difference(['latitude', 'longitude'], _.keys(params));
  if(!_.isEmpty(missing)){
    return 'missing required parameters: ' + missing.join(', ');
  }
}

function formatMagazine(row){
  /*jshint camelcase:false */
  return {
    id: row.id,
    name: row.name,
    priceUS: parseFloat(row.price_us),
    priceCA: parseFloat(row.price_ca),
    coverImageUrl: row.cover_image_url,
    description: row.description,
    category: row.category,
    popularityInCategory: row.popularity_in_category,
    popularityInStore: row.popularity_in_store,
    popularityOverall: row.popularity_overall,
    dealers: []
  };
}

function formatDealer(row){
  /*jshint camelcase:false */
  return {
    id: row.dealer_id,
    name: row.dealer_name,
    address: row.dealer_address,
    city: row.dealer_city,
    state: row.dealer_state,
    latitude: parseFloat(row.dealer_latitude),
    longitude: parseFloat(row.dealer_longitude),
    distance: metersToMiles(row.dealer_distance_in_meters),
    phone: {
      areaCode: row.dealer_phone_area_code,
      number: row.dealer_phone_number
    }
  };
}

function groupMagazines(options){
    var formattedMagazines = _.reduce(options.magazines, function(memo, row){
      if(!memo[row.id]){
        memo[row.id] = formatMagazine(row);
      }
      var dealer = options.dealers[row.dealer_id];
      if(dealer){
        memo[row.id].dealers.push(dealer);
      }
      return memo;
    }, {});

    return _.chain(options.magazines)
      .pluck('id').uniq()
      .map(function(id){
        return formattedMagazines[id];
      })
      .value();
}

function filterMagazinesWithoutDealers(magazines){
  // since we have a limit on how many dealers we return, there's a chance that
  // a magazine is only sold at one of the dealers that wasn't returned
  return _.filter(magazines, function(magazine){
    return magazine.dealers.length > 0;
  });
}

function constructArgs(params){
  var searchRadius = milesToMeters(params.distance ||
                                   config.defaultSearchRadius);

  return {
    dealerId: params.dealerId,
    latitude: params.latitude,
    longitude: params.longitude,
    searchRadius: searchRadius,
    query: '%' + params.query + '%',
    id: params.id,
    ids: params.ids,
    category: params.category
  };
}

function constructQuery(params){
  if(params.query){
    return SEARCH_BY_NAME_QUERY;
  }
  else if(params.id){
    return SEARCH_BY_ID_QUERY;
  }
  else if(params.ids){
    return SEARCH_BY_IDS_QUERY;
  }
  else if(params.category){
    if(params.dealerId){
      return SEARCH_BY_DEALER_CATEGORY_QUERY;
    }
    else {
      return SEARCH_BY_CATEGORY_QUERY;
    }
  }
  else if(params.popularity){
    return SEARCH_BY_POPULARITY_QUERY;
  }

  throw 'unknown search type';
}

function grabDealersFromResult(options){
  var limit = parseInt(options.limit, 10);
  if(_.isNaN(limit)){ throw 'options.limit must be a number'; }

  var dealers = {};
  var count = 0;

  for(var i=0, len=options.rows.length; i<len; i++){
    var row = options.rows[i];

    if(dealers[row.dealer_id]){ continue; }

    dealers[row.dealer_id] = formatDealer(row);

    count += 1;
    if(count >= limit){
      return dealers;
    }
  }

  return dealers;
}

function getDealerDistancesUsingGoogle(options){
  var deferred = Q.defer();
  var destinations = _.map(_.values(options.dealers), function(dealer){
    return dealer.latitude + ',' + dealer.longitude;
  }).join('|');

  if(_.isEmpty(destinations)){
    deferred.resolve({});
  }
  else {
    request
      .get('https://maps.googleapis.com/maps/api/distancematrix/json')
      .query({
        origins: options.origin.latitude + ',' + options.origin.longitude,
        destinations: destinations,
        key: config.googleApiKey
      })
      .end(function(err, res){
        if(err || res.status !== 200 || res.body.status !== "OK"){
          deferred.reject(err || (res.status !== 200 ? res.status : res.body.status));
        }
        else {
          var keys = _.keys(options.dealers);
          _.each(res.body.rows[0].elements, function(result, i){
            var key = keys[i];
            if(result.status === "OK"){
              options.dealers[key].distance = metersToMiles(
                                                result.distance.value);
            }
            else {
              // we couldn't get a distance calculation from google,
              // remove dealer from results
              delete options.dealers[key];
            }
          });
          deferred.resolve(options.dealers);
        }
      });
  }

  return deferred.promise;
}

function argsToArray(args, queryObj){
  return _.chain(args).pick(queryObj.args).values().value();
}

exports.search = function(params){
  var error = getValidationError(params);
  if(error){ return Q.reject(error); }

  var query = constructQuery(params);
  var args = constructArgs(params);
  return dbWrapper
    .query(query.str, argsToArray(args, query))
    .then(function(result){
      if(_.isEmpty(result.rows) && args.searchRadius < milesToMeters(50)){
        args.searchRadius = milesToMeters(50);
        return dbWrapper.query(query.str, argsToArray(args, query));
      }
      else {
        return result;
      }
    })
    .then(function(result){
      return groupMagazines({
        magazines: result.rows,
        dealers: grabDealersFromResult({rows: result.rows, limit: 999})
      });
    })
    .then(filterMagazinesWithoutDealers);
};

exports.getNew = function(){
  var query = multiline(function(){/*
    SELECT
      dim_title.titleid AS id,
      dim_title.bipadname AS name,
      dim_title.coverimageurl AS cover_image_url,
      dim_title.title_description AS description
    FROM dim_title
    WHERE is_new_title = '1';
  */});
  return dbWrapper.query(query, []).then(function(result){
    return result.rows;
  });
}
