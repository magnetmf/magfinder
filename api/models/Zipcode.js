'use strict';

var Q = require('q');
var _ = require('lodash');
var fs = require('fs');
var path = require('path');
var parse = require('csv-parse');

var codesCache = {};

function parseCodes(country){
  if(!_.contains(['US', 'CA'], country)){
    throw 'country must be US or CA';
  }

  if(codesCache[country]){
    return Q.resolve(codesCache[country]);
  }

  var deferred = Q.defer();
  var parser = parse({delimiter: '\t'});

  var output = {};

  parser.on('error', function(err){
    console.error('Error trying to parse ' + country + ' ZipCodes', err);
    Q.reject(err);
  });

  parser.on('readable', function(){
    var record = parser.read();
    while(record){
      output[record[1]] = {
        zipCode: record[1],
        country: record[0],
        city: record[2],
        state: record[3],
        county: record[5],
        latitude: parseFloat(record[9]),
        longitude: parseFloat(record[10])
      };
      record = parser.read();
    }
  });

  parser.on('finish', function(){
    codesCache[country] = output;
    deferred.resolve(output);
  });

  var filePath = { US: '../../data/US.txt', CA: '../../data/CA.txt' }[country];

  fs.createReadStream(path.join(__dirname, filePath)).pipe(parser);

  return deferred.promise;
}

function matchCode(zipCode, country){
  if(country === 'CA'){
    // for Canada we have only the first letters of the full postal codes
    // (copyright reasons)
    zipCode = zipCode.slice(0,3).toUpperCase();
  }

  return parseCodes(country).then(function(codes){
    return codes[zipCode];
  });
}

exports.match = function(zipCode){
  if(!_.isString(zipCode) || zipCode.length < 3){
    return Q.reject('zipCode must be a string at least 3 chars long.');
  }

  var country;
  if(zipCode.match(/^\d+$/)) { country = 'US'; }
  else                       { country = 'CA'; }

  return matchCode(zipCode, country);
};
