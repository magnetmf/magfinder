"use strict";

var dbWrapper = require('../../utils/db_wrapper');
var _ = require('lodash');
var multiline = require('multiline');
var Q = require('q');
var sendPhoneVerificationCode = require('../../utils/sendPhoneVerificationCode');

function formatPrefs(prefs){
  prefs = prefs || {};
  return {
    receiveEmail: !!prefs.receive_email,
    receiveSMS: !!prefs.receive_sms
  };
}

function parseVerificationCode(code){
  var codeArr = code.split('__');
  return {
    createdAt: parseInt(codeArr[0], 10),
    value: parseInt(codeArr[1], 10)
  };
}

function codeExpired(createdAt, currentDbTime){
  // expires after 24 hours
  return currentDbTime - createdAt > 24 * 60 * 60;
}

function updatePhoneVerificationCode(userID){
  var queryStr = QUERIES.UPDATE_PHONE_VERIFICATION_CODE;
  var args = [userID];
  return dbWrapper.query(queryStr, args, { db: 'app' })
    .then(function(result){
      return parseVerificationCode(result.rows[0].verification_code);
    });
}

function getFreshPhoneVerificationCode(userID, phone){
  var code = parseVerificationCode(phone.verificationCode);

  if(codeExpired(code.createdAt, phone.currentDbTime)){
    return updatePhoneVerificationCode(userID);
  }
  else {
    return Q.when(code);
  }
}

var QUERIES = {

  FIND_USER_BY_ID: multiline(function(){/*
    SELECT id, name
    FROM USERS
    WHERE users.id = $1
  */}),

  FIND_USER_BY_SOCIAL_NETWORK: multiline(function(){/*
    SELECT id, name
    FROM users
    INNER JOIN social_accounts
      ON users.id = social_accounts.user_id
    WHERE social_network = $1
    AND social_account_id = $2
  */}),

  FIND_USER_BY_EMAIL_PASSWORD: multiline(function(){/*
    SELECT id, name
    FROM users
    INNER JOIN user_emails
      ON users.id = user_emails.user_id
    WHERE user_emails.email = $1
    AND users.password = crypt($2, password)
  */}),

  GET_USER_EMAIL: multiline(function(){/*
    SELECT email, verified_at
    FROM user_emails
    WHERE user_emails.user_id = $1
    LIMIT 1
  */}),

  GET_USER_PHONE: multiline(function(){/*
    SELECT phone, verification_code, verified_at,
           ROUND(DATE_PART('epoch', NOW())) as now
    FROM user_phones
    WHERE user_phones.user_id = $1
    LIMIT 1
  */}),

  GET_COMMUNICATION_PREFERENCES: multiline(function(){/*
    SELECT receive_email, receive_sms
    FROM communication_preferences
    WHERE user_id = $1
    LIMIT 1
  */}),

  CREATE_USER: multiline(function(){/*
    INSERT INTO users (name)
    VALUES ($1)
    RETURNING id, name
  */}),

  CREATE_USER_WITH_PASSWORD: multiline(function(){/*
    INSERT INTO users (name, password)
    VALUES ($1, crypt($2, gen_salt('bf', 8)))
    RETURNING id, name
  */}),

  CREATE_SOCIAL_ACCOUNT: multiline(function(){/*
    INSERT INTO social_accounts (social_network, social_account_id, user_id)
    VALUES ($1, $2, $3)
  */}),

  UPDATE_USER: multiline(function(){/*
    UPDATE users
    SET name=$1
    WHERE id=$2
    RETURNING id, name
  */}),

  CREATE_USER_EMAIL: multiline(function(){/*
    INSERT INTO user_emails (email, user_id)
    VALUES ($1, $2)
    RETURNING user_id, email, verified_at
  */}),

  UPDATE_USER_EMAIL: multiline(function(){/*
    UPDATE user_emails
    SET email=$1
    WHERE user_id=$2
    RETURNING email, verified_at
  */}),

  CREATE_USER_PHONE: multiline(function(){/*
    INSERT INTO user_phones (phone, user_id)
    VALUES ($1, $2)
    RETURNING user_id, phone, verified_at
  */}),

  UPDATE_USER_PHONE: multiline(function(){/*
    UPDATE user_phones
    SET phone=$1
    WHERE user_id=$2
    RETURNING phone, verified_at
  */}),


  GET_FAVORITE_MAGAZINES: multiline(function(){/*
    SELECT magazine_id
    FROM favorite_magazines
    WHERE user_id=$1
  */}),

  ADD_FAVORITE_MAGAZINE: multiline(function(){/*
    INSERT INTO favorite_magazines (user_id, magazine_id)
    VALUES ($1, $2)
  */}),

  REMOVE_FAVORITE_MAGAZINE: multiline(function(){/*
    DELETE FROM favorite_magazines
    WHERE user_id=$1 AND magazine_id=$2
  */}),

  ADD_COMMUNICATION_PREFERENCES: multiline(function(){/*
    INSERT INTO communication_preferences
      (user_id, receive_sms, receive_email)
    VALUES ($1, $2, $3)
    RETURNING receive_sms, receive_email
  */}),

  UPDATE_COMMUNICATION_PREFERENCES: multiline(function(){/*
    UPDATE communication_preferences
    SET receive_sms=$2, receive_email=$3
    WHERE user_id=$1
    RETURNING receive_sms, receive_email
  */}),

  VERIFY_USER_PHONE: multiline(function(){/*
    UPDATE user_phones
    SET verified_at=NOW()
    WHERE user_phones.user_id = $1
    RETURNING phone, verified_at
  */}),

  UPDATE_PHONE_VERIFICATION_CODE: multiline(function(){/*
    UPDATE user_phones
    SET verification_code='new value'
    WHERE user_phones.user_id = $1
    RETURNING verification_code
  */}),

  GET_USERS_TO_NOTIFY_BY_SMS: multiline(function(){/*
    SELECT
      user_phones.phone AS phone,
      array_agg(favorite_magazines.magazine_id) AS magazine_ids
    FROM user_phones
    INNER JOIN communication_preferences
      ON user_phones.user_id = communication_preferences.user_id
    INNER JOIN favorite_magazines
      ON user_phones.user_id = favorite_magazines.user_id
    WHERE receive_sms = true
    AND user_phones.verified_at IS NOT NULL
    AND magazine_id = ANY($1)
    GROUP BY user_phones.phone;
  */})
};

exports.find = function findUser(options){
  var queryStr, args;

  if(options.id){
    queryStr = QUERIES.FIND_USER_BY_ID;
    args = [options.id];
  }
  else if(options.facebookId){
    queryStr = QUERIES.FIND_USER_BY_SOCIAL_NETWORK;
    args = [ 'facebook', options.facebookId ];
  }
  else if(options.twitterId){
    queryStr = QUERIES.FIND_USER_BY_SOCIAL_NETWORK;
    args = [ 'twitter', options.twitterId ];
  }
  else if(options.email && options.password){
    queryStr = QUERIES.FIND_USER_BY_EMAIL_PASSWORD;
    args = [ options.email, options.password ];
  }
  else {
    return Q.reject('One of the following options must be passed: ' +
                    'id, facebookId, twitterId, email/password');
  }

  return dbWrapper
    .query(queryStr, args, { db: 'app' })
    .then(function(result){
      return result.rows[0];
    });
};

exports.createWithPassword = function createUserWithPassword(options){
  var user;

  return dbWrapper.transaction(function(query){
    return query(
      QUERIES.CREATE_USER_WITH_PASSWORD,
      [ options.email.split('@')[0], options.password ]
    )
    .then(function(result){
      user = result.rows[0];
      return query(QUERIES.CREATE_USER_EMAIL, [ options.email, user.id ]);
    });
  }, { db: 'app' })
    .then(function(result){
      // return the user object, not the email
      return user;
    });
};

exports.create = function createUser(options){
  var socialAccountArgs;
  if(options.facebookId){
    socialAccountArgs = ['facebook', options.facebookId];
  }
  else if(options.twitterId){
    socialAccountArgs = ['twitter', options.twitterId];
  }
  else {
    return Q.reject('One of the following options must be passed: ' +
                    'facebookId, twitterId');
  }

  if(_.isEmpty(options.name)){
    return Q.reject('name is missing');
  }

  var user;

  return dbWrapper.transaction(function(query){
    return query(QUERIES.CREATE_USER, [options.name])
      .then(function createSocialAccount(result){
        user = result.rows[0];
        socialAccountArgs.push(user.id);

        return query(QUERIES.CREATE_SOCIAL_ACCOUNT, socialAccountArgs);
      });
  }, { db: 'app' })
    .then(function(){
      // return the user object, not the social account object
      return user;
    });
};

exports.findOrCreate = function findOrCreateUser(options){
  return exports.find(options)
    .then(function(user){
      if(user){ return user; }
      else { return exports.create(options); }
    });
};

exports.getEmail = function getUserEmail(options){
  var queryStr = QUERIES.GET_USER_EMAIL;
  var args = [options.userID];

  return dbWrapper
    .query(queryStr, args, { db: 'app' })
    .then(function(result){
      var res = result.rows[0];
      if(res){
        return { email: res.email, verifiedAt: res.verified_at };
      }
    });
};

exports.getPhone = function getUserPhone(options){
  var queryStr = QUERIES.GET_USER_PHONE;
  var args = [options.userID];

  return dbWrapper
    .query(queryStr, args, { db: 'app' })
    .then(function(result){
      var res = result.rows[0];
      if(res){
        var phone = { phone: res.phone, verifiedAt: res.verified_at };
        if(options.includeVerificationCode){
          phone.verificationCode = res.verification_code;
          phone.currentDbTime = res.now;
        }
        return phone;
      }
    });
};

exports.getCommunicationPreferences = function getCommunicationPrefs(options){
  var queryStr = QUERIES.GET_COMMUNICATION_PREFERENCES;
  var args = [options.userID];

  return dbWrapper
    .query(queryStr, args, { db: 'app' })
    .then(function(result){
      return formatPrefs(result.rows[0]);
    });
};

exports.updateCommunicationPreferences = function(userID, preferences){
  var queryStr = QUERIES.GET_COMMUNICATION_PREFERENCES;
  var args = [userID];
  return dbWrapper
    .query(queryStr, args, { db: 'app' })
    .then(function(result){
      if(_.isEmpty(result.rows)){
        queryStr = QUERIES.ADD_COMMUNICATION_PREFERENCES;
        args = args.concat([!!preferences.receiveSMS,
                            !!preferences.receiveEmail]);
      }
      else {
        var oldPrefs = formatPrefs(result.rows[0]);
        queryStr = QUERIES.UPDATE_COMMUNICATION_PREFERENCES;
        args = args.concat(
          _.chain(oldPrefs).extend(preferences)
           .pick('receiveSMS', 'receiveEmail')
           .values().value()
        );
      }
      return dbWrapper
        .query(queryStr, args, { db: 'app' });
    })
    .then(function(result){
      return formatPrefs(result.rows[0]);
    });
};

exports.updateEmail = function updateUserEmail(id, email){
  return exports.getEmail({ userID: id })
    .then(function(oldEmail){
      var args = [email, id];

      var queryStr;
      if(oldEmail){
        queryStr = QUERIES.UPDATE_USER_EMAIL;
      }
      else {
        queryStr = QUERIES.CREATE_USER_EMAIL;
      }

      return dbWrapper.query(queryStr, args, { db: 'app' });
    })
    .then(function(result){
      return result.rows[0];
    });
};

exports.updatePhone = function updateUserPhone(id, phone){
  return exports.getPhone({ userID: id })
    .then(function(oldPhone){
      var args = [phone, id];

      var queryStr;
      if(oldPhone){
        queryStr = QUERIES.UPDATE_USER_PHONE;
      }
      else {
        queryStr = QUERIES.CREATE_USER_PHONE;
      }

      return dbWrapper.query(queryStr, args, { db: 'app' });
    })
    .then(function(result){
      return result.rows[0];
    });
};

exports.update = function updateUser(id, attrs){
  return exports.find({ id: id })
    .then(function(user){
      var queryStr = QUERIES.UPDATE_USER;

      // expected args are [name, userid]
      var args = _.chain(user).extend(attrs).pick(['name']).values().push(id)
                  .value();

      return dbWrapper.query(queryStr, args, { db: 'app' });
    })
    .then(function(result){
      return result.rows[0];
    });
};

exports.getFavoriteMagazines = function getFavoriteMagazines(userID){
  var queryStr = QUERIES.GET_FAVORITE_MAGAZINES;
  var args = [userID];
  return dbWrapper
    .query(queryStr, args, { db: 'app' })
    .then(function(result){
      return _.pluck(result.rows, 'magazine_id');
    });
};

exports.addFavoriteMagazine = function addFavoriteMagazine(userID, magazineID){
  return exports.getFavoriteMagazines(userID)
    .then(function(magazines){
      if(_.include(magazines, magazineID)){
        return true; // already in favorites
      }
      else {
        var queryStr = QUERIES.ADD_FAVORITE_MAGAZINE;
        var args = [userID, magazineID];
        return dbWrapper.query(queryStr, args, { db: 'app' });
      }
    })
    .then(function(){
      return magazineID;
    });
};

exports.removeFavoriteMagazine = function removeFavoriteMagazine(
  userID, magazineID
){
  return exports.getFavoriteMagazines(userID)
    .then(function(magazines){
      if(_.include(magazines, magazineID)){
        var queryStr = QUERIES.REMOVE_FAVORITE_MAGAZINE;
        var args = [userID, magazineID];
        return dbWrapper.query(queryStr, args, { db: 'app' });
      }
      else {
        return true; // not in favorites
      }
    })
    .then(function(){
      return magazineID;
    });
};

exports.refreshPhoneVerificationCode = function(userID){
  return exports.getPhone({ userID: userID, includeVerificationCode: true })
    .then(function(phone){
      if(!phone){ throw 'NO_PHONE'; }
      if(phone.verifiedAt){ throw 'PHONE_IS_VERIFIED'; }

      return getFreshPhoneVerificationCode(userID, phone)
        .then(function(code){
          sendPhoneVerificationCode({ phoneNumber: phone.phone,
                                      codeValue: code.value });
          return true;
        });
    });
};

exports.verifyPhone = function verifyPhone(userID, verificationCode){
  return exports.getPhone({ userID: userID, includeVerificationCode: true })
    .then(function(phone){
      if(!phone){ throw 'NO_PHONE'; }

      var code = parseVerificationCode(phone.verificationCode);

      if(code.value !== parseInt(verificationCode, 10)){
        throw 'INVALID_CODE';
      }

      if(codeExpired(code.createdAt, phone.currentDbTime)){
        throw 'EXPIRED';
      }

      var queryStr = QUERIES.VERIFY_USER_PHONE;
      var args = [userID];
      return dbWrapper.query(queryStr, args, { db: 'app' });
    })
    .then(function(result){
      var res = result.rows[0];
      if(res){
        return { phone: res.phone, verifiedAt: res.verified_at };
      }
    });
};

function getPhonesToNotify(magazines){
  var query = QUERIES.GET_USERS_TO_NOTIFY_BY_SMS;
  var args = [_.pluck(magazines, 'id')];
  return dbWrapper.query(query, args, { db: 'app' })
    .then(function(result){
      return result.rows;
    })
}

function mergeMagazinesIntoPhonesToNotify(magazines, phonesToNotify){
  return _.map(phonesToNotify, function(phone){
    var magazineData = _.chain(magazines)
      .filter(function(magazine){
        return _.contains(phone.magazine_ids, magazine.id);
      })
      .map(function(magazine){
        return { id: magazine.id, name: magazine.name };
      })
      .value();

    return {
      phoneNumber: phone.phone,
      magazines: magazineData
    };
  });
}

exports.getNewMagazineNotifications = function(options){
  var Magazine = require('./Magazine');
  return Magazine.getNew().then(function(magazines){
    return getPhonesToNotify(magazines)
      .then(_.partial(mergeMagazinesIntoPhonesToNotify, magazines))
  })
  .then(function(result){
    return result;
  });
}
