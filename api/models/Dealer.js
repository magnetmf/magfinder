"use strict";

var dbWrapper = require('../../utils/db_wrapper');
var multiline = require('multiline');
var _ = require('lodash');
var Q = require('q');
var config = require('../../configs/public');
var milesToMeters = require('../../utils/unit_converter').milesToMeters;
var metersToMiles = require('../../utils/unit_converter').metersToMiles;

var SEARCH_QUERY = multiline(function(){/*
  -- returns one dealer

  SELECT
    dim_dealer.dealerid AS id,
    dim_dealer.dealername AS name,
    dim_dealer.address AS address,
    dim_dealer.city AS city,
    dim_dealer.state AS state,
    dim_dealer.latitude AS latitude,
    dim_dealer.longitude AS longitude,
    earth_distance(
      ll_to_earth(dim_dealer.latitude, dim_dealer.longitude),
      ll_to_earth($1, $2)
    ) AS distance_in_meters,
    dim_dealer.phoneareacode AS phone_area_code,
    dim_dealer.phonenumber AS phone_number
  FROM dim_dealer
  WHERE dim_dealer.dealerid = $3
*/});

var LIST_QUERY = multiline(function(){/*
  -- returns a list of dealers near your current location

  SELECT
    dim_dealer.dealerid AS id,
    dim_dealer.dealername AS name,
    dim_dealer.address AS address,
    dim_dealer.city AS city,
    dim_dealer.state AS state,
    dim_dealer.latitude AS latitude,
    dim_dealer.longitude AS longitude,
    earth_distance(
      ll_to_earth(dim_dealer.latitude, dim_dealer.longitude),
      ll_to_earth($1, $2)
    ) AS distance_in_meters,
    dim_dealer.phoneareacode AS phone_area_code,
    dim_dealer.phonenumber AS phone_number
  FROM dim_dealer

  WHERE earth_box(ll_to_earth($1, $2), $3) @>
    ll_to_earth(dim_dealer.latitude, dim_dealer.longitude)

  -- the @> operator sometimes includes points further than the specified
  -- distance, so a second check using earth_distance is recommended
  AND earth_distance(
    ll_to_earth(dim_dealer.latitude, dim_dealer.longitude),
    ll_to_earth($1, $2)
  ) < $3

  ORDER BY distance_in_meters
*/});

function getValidationError(params){
  params = params || {};

  var missing = _.difference(['latitude', 'longitude'], _.keys(params));
  if(!_.isEmpty(missing)){
    return 'missing required parameters: ' + missing.join(', ');
  }
}

function formatDealers(dealers){
  return _.map(dealers, function(dealer){
    /*jshint camelcase:false */
    return _.extend(
      _.pick(dealer, ['id', 'name', 'address', 'city', 'state']),
      {
        latitude: parseFloat(dealer.latitude),
        longitude: parseFloat(dealer.longitude),
        distance: metersToMiles(dealer.distance_in_meters),
        phone: {
          areaCode: dealer.phone_area_code,
          number: dealer.phone_number
        }
      }
    );
  });
}

exports.list = function(params){
  var error = getValidationError(params);
  if(error){ return Q.reject(error); }

  var query = LIST_QUERY;
  var searchRadius = milesToMeters(params.distance ||
                                   config.defaultSearchRadius);
  var args = [
    params.latitude,                                          // $1
    params.longitude,                                         // $2
    searchRadius                                              // $3
  ];

  return dbWrapper.query(query, args)
    .then(function(result){
      if(_.isEmpty(result.rows) &&
         searchRadius < milesToMeters(50))
      {
        args[2] = milesToMeters(50);
        return dbWrapper.query(query, _.values(args));
      }
      else {
        return result;
      }
    })
    .then(function(result){ return formatDealers(result.rows); });
};

exports.search = function(params){
  var error = getValidationError(params);
  if(error){ return Q.reject(error); }

  var query = SEARCH_QUERY;
  var args = [
    params.latitude,                                          // $1
    params.longitude,                                         // $2
    params.dealerId                                           // $3
  ];

  return dbWrapper.query(query, args)
    .then(function(result){ return formatDealers(result.rows); });
};
