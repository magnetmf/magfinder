"use strict";

var _ = require('lodash');
var express = require('express');
var router = express.Router();
var Zipcode = require('./models/Zipcode');

router.get('/:zipCode', function(req,res,next){
  Zipcode.match(req.params.zipCode)
    .then(function(result){
      if(_.isEmpty(result)){ res.status(404).send('Not Found'); }
      else{ res.send(result); }
    })
    .fail(next);
});

module.exports = router;
