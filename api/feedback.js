"use strict";

var _ = require('lodash');
var express = require('express');
var router = express.Router();
var Feedback = require('./models/Feedback');

router.post('/feedback', function(req, res, next){
  Feedback.create(req.body)
    .then(function(feedback){
      res.status(200).send(feedback);
    })
    .fail(next);
});

module.exports = router;
