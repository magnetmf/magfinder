"use strict";

var debug = require('debug')('api');

module.exports = function apiErrorHandler(err, req, res, next){
  debug('Error on ' + req.method + ' ' + req.path + '. ID: ' + req.id + '\n',
        err.stack);
  res.status(500).send('Internal Server Error');
};
