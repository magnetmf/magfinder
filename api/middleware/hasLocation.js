"use strict";

var _ = require('lodash');

module.exports = function hasLocation(req, res, next){
  var missing = _.difference(['latitude', 'longitude'], _.keys(req.query));

  if(_.isEmpty(missing)){ next(); }
  else {
    res.status(400).send('missing required parameters: ' + missing.join(', '));
  }
};
