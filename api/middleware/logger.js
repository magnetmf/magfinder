"use strict";

var randomstring = require('randomstring');
var debug = require('debug')('api');

module.exports = function requestLogger(req, res, next){
  req.id = randomstring.generate(5);
  debug(req.method + ' ' + req.path + '. ID: ' + req.id);
  next();
};

