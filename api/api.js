"use strict";

var _ = require('lodash');
var express = require('express');
var cors = require('express-cors');
var debug = require('debug')('api');

var app = express();

app.use(cors({ allowedOrigins: ['*'] }));
app.use(require('./middleware/logger'));

app.use('/magazines', require('./magazines'));
app.use('/users', require('./users'));
app.use('/sessions', require('./sessions'));
app.use('/zipcodes', require('./zipcodes'));
app.use('/dealers', require('./dealers'));
app.use('/categories', require('./categories'));
app.use('/feedback', require('./feedback'));

app.use(require('./middleware/errorHandler'));

module.exports = app;
