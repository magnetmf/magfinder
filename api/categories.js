"use strict";

var _ = require('lodash');
var express = require('express');
var router = express.Router();
var Category = require('./models/Category');
var hasLocation = require('./middleware/hasLocation');
var Magazine = require('./models/Magazine');

router.get('/', function(req, res, next){
  var missing = _.difference(['latitude', 'longitude'], _.keys(req.query));

  if(!_.isEmpty(missing)){
    res.status(400).send('missing required parameters: ' + missing.join(', '));
    return;
  }

  Category.list({
    latitude: req.query.latitude,
    longitude: req.query.longitude,
    distance: req.query.distance,
    dealerId: req.query.dealerId
  })
    .then(function(result){
      res.send(result);
    })
    .fail(next);
});

router.get('/:name/magazines', hasLocation, function(req, res, next){
  Magazine.search({
    category: req.params.name,
    latitude: req.query.latitude,
    longitude: req.query.longitude,
    distance: req.query.distance
  })
    .then(function(result){
      res.send(result);
    })
    .fail(next);
});

module.exports = router;
