'use strict';

require('./magazine.less');

var React = require('react');
var _ = require('lodash');
var SearchStore = require('../../stores/SearchStore');
var ResponsiveStore = require('../../stores/ResponsiveStore');
var StoreMixin = require('fluxible').StoreMixin;
var TopPanel = require('../top_panel/TopPanel.jsx');
var MostPopular = require('../most_popular/MostPopular.jsx');
var PriceFormatter = require('../../utils/PriceFormatter');
var NavLink = require('flux-router-component').NavLink;
var searchMagazines = require('../../actions/searchMagazines');
var Message = require('../general/Message.jsx');
var LoadingMessageMixin = require('../../mixins/LoadingMessageMixin');
var navigateAction = require('flux-router-component').navigateAction;
var DealerStore = require('../../stores/DealerStore');
var DealerPreview = require('../dealers/DealerPreview');
var FacebookButton = require('../general/ShareMagazineButton').Facebook;
var TwitterButton = require('../general/ShareMagazineButton').Twitter;
var FavoriteMagazineButton = require('../general/FavoriteMagazineButton');

var Magazine = React.createClass({
  mixins: [StoreMixin, LoadingMessageMixin],

  statics: {
    storeListeners: {
      'onSizeChange': ResponsiveStore,
      'onSearchChange': SearchStore
    }
  },

  getStateFromStores: function(){
    return {
      size: this.getStore(ResponsiveStore).getState().size,
      magazine: this.getStore(SearchStore).getMagazine(this.props.id)
    };
  },

  getInitialState: function(){
    return this.getStateFromStores();
  },

  onSizeChange: function(){
    this.setState(this.getStateFromStores());
  },

  onSearchChange: function(){
    this.setState(this.getStateFromStores());
  },

  componentDidMount: function(){
    this.redirectIfNotMobile();

    if(!this.state.magazine){
      this.props.context.executeAction(searchMagazines,
                                       { magazineId: this.props.id });
    }
  },

  componentDidUpdate: function(){
    this.redirectIfNotMobile();
  },

  componentWillReceiveProps: function(nextProps){
    if(nextProps.id !== this.state.magazine.id){
      this.props.context.executeAction(searchMagazines,
                                       { magazineId: nextProps.id });
    }
  },

  redirectIfNotMobile: function(){
    if(this.state.size !== ResponsiveStore.MOBILE){
      this.props.context.executeAction(navigateAction, {
        url: this.dealersURL('map')
      });
    }
  },

  renderBasicInfo: function(){
    var style = {
      backgroundImage: 'url(' + this.state.magazine.coverImageUrl + ')',
      backgroundSize: 'cover'
    };
    return (
      <div className="magazine__basic-info">
        <div className="magazine__cover"><div style={style}></div></div>
        <div className="magazine__description">
          <div>
            <h2>{ this.state.magazine.name }</h2>
            <div>
              <FavoriteMagazineButton context={this.props.context}
                magazine={this.state.magazine} />
            </div>
            <p>{ this.state.magazine.description }</p>
          </div>
          <div className="magazine__share">
            <TwitterButton context={this.props.context}
              magazine={this.state.magazine} />
            <FacebookButton context={this.props.context}
              magazine={this.state.magazine} />
          </div>
        </div>
      </div>
    );
  },

  renderPrice: function(){
    var price = PriceFormatter.split(this.state.magazine.price);
    return (
      <div className="magazine__results__from">
        <div>From:</div>&nbsp;
        <div><span>{ price.dollars }</span><span>{ price.cents }</span>
        </div>
      </div>
    );
  },

  renderDetails: function(){
    if(this.props.dealerId){ return; }
    return (
      <div>
        <p>
          We have found <em>{ this.state.magazine.dealers.length }
          </em> local results.
        </p>
        <p>
          Nearest location: <em>
            { this.state.magazine.nearestDealer.distance.toFixed(2) } Miles
          </em>
        </p>
      </div>
    );
  },

  renderResults: function(){
    return (
      <div className="magazine__results">
        <div className="magazine__results__details">
          { this.renderDetails() }
        </div>
        { this.renderPrice() }
      </div>
    );
  },

  dealersURL: function(display){
    display = display || 'map';
    var url = this.props.context.makePath('dealers', {id: this.props.id}) +
              '?display=' + display;
    if(this.props.dealerId){
      url += '&dealerId=' + this.props.dealerId;
    }
    return url;
  },

  renderButtons: function(){
    if(this.props.dealerId){ return; }
    return (
      <div className="magazine__buttons">
        <NavLink href={this.dealersURL('map')} context={this.props.context}>
          Nearest Location
        </NavLink>
        <NavLink href={this.dealersURL('list')} context={this.props.context}>
          All Locations
        </NavLink>
      </div>
    );
  },

  renderContent: function(){
    var loadingMessage = this.renderLoadingMessage(this.props.context,
                                                   'searchResults');
    if(loadingMessage){ return loadingMessage; }

    if(this.state.magazine){
      return (
        <div>
          { this.renderBasicInfo() }
          { this.renderResults() }
          { this.renderButtons() }
        </div>
      );
    }

    return <Message type="empty">No results found.</Message>;
  },

  renderFooter: function(){
    var dealer;
    if(this.props.dealerId){
      dealer = this.getStore(DealerStore).getDealer(this.props.dealerId);
    }

    if(dealer){
      return (
        <div className="magazine__footer">
          <DealerPreview key={dealer.id} context={this.props.context}
            dealer={dealer} />
        </div>
      );
    }

    return <MostPopular context={this.props.context}/>;
  },

  render: function(){
    return (
      <div className="magazine">
        <TopPanel context={this.props.context} />
        <div className="magazine__content">
          { this.renderContent() }
        </div>
        { this.renderFooter() }
      </div>
    );
  }

});

module.exports = Magazine;
