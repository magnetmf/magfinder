"use strict";

require('./magazine-preview.less');

var React = require('react/addons');
var cx = React.addons.classSet;
var PriceFormatter = require('../../utils/PriceFormatter');
var NavLink = require('flux-router-component').NavLink;
var Icon = require('../general/Icon.jsx');
var FacebookButton = require('../general/ShareMagazineButton').Facebook;
var TwitterButton = require('../general/ShareMagazineButton').Twitter;
var FavoriteMagazineButton = require('../general/FavoriteMagazineButton');
var removeFavoriteMagazine = require('../../actions/removeFavoriteMagazine');

var MagazinePreview = React.createClass({
  getDefaultProps: function(){
    return { clickable: true, arrangement: 'default' };
  },

  hasDefaultCover: function(){
    return !!this.props.magazine.coverImageUrl
            .match(/thumbnails\/DefaultCover.gif/);
  },

  onRemoveFavoriteClick: function(evt){
    evt.preventDefault();
    evt.stopPropagation();

    this.props.context.executeAction(removeFavoriteMagazine,
                                     { id: this.props.magazine.id });
  },

  renderResultCount: function(){
    return (
      <div key="resultCount" className="magazine-preview__count">
        We have found <em>{ this.props.magazine.dealers.length }</em> local
        results.
      </div>
    );
  },

  renderCover: function(){
    var content;

    if(!this.hasDefaultCover()){
      var style = {
        backgroundImage: 'url(' + this.props.magazine.coverImageUrl + ')',
        backgroundSize: 'cover'
      };
      content = <div style={ style }></div>;
    }

    return (
      <div key="cover" className="magazine-preview__cover">{content}</div>
    );
  },

  renderPrice: function(){
    var price = {};
    if(this.props.magazine.price){
      price = PriceFormatter.split(this.props.magazine.price);
    }

    var className="magazine-preview__info__field magazine-preview__info__from";
    return (
      <div className={className} key="price">
        <div>From:</div>
        <div><span>{ price.dollars }</span><span>{ price.cents }</span></div>
      </div>
    );
  },

  renderNearestDealer: function(){
    return (
      <div className="magazine-preview__info__field" key="nearestDealer">
        <div>Nearest Location:</div>
        <div>
          { this.props.magazine.nearestDealer.distance.toFixed(2) + ' miles' }
        </div>
      </div>
    );
  },

  renderDescription: function(){
    return <p key="description">{ this.props.magazine.description }</p>;
  },

  renderIcon: function(){
    return <Icon className="magazine-preview__click-icon" key="icon"
            name="rightAngle" />;
  },

  renderShareButtons: function(){
    return (
      <div className="magazine-preview__share" key="shareButtons">
        <FavoriteMagazineButton context={this.props.context}
          magazine={this.props.magazine} />
        <TwitterButton context={this.props.context}
          magazine={this.props.magazine} />
        <FacebookButton context={this.props.context}
          magazine={this.props.magazine} />
      </div>
    );
  },

  renderRemoveFavoriteButton: function(){
    return (
      <div className="magazine-preview__remove-favorite" key="removeFavorite">
        <button onClick={this.onRemoveFavoriteClick}>Remove</button>
      </div>
    );
  },

  renderInfo: function(){
    var content;
    if(this.props.arrangement === 'default'){
      content = [ this.renderPrice(), this.renderNearestDealer(),
                  this.renderDescription(), this.renderShareButtons() ];
    }
    else if(this.props.arrangement === 'minimal'){
      content = [ this.renderPrice(), this.renderNearestDealer(),
                  this.renderDescription() ];
    }
    else if(this.props.arrangement === 'descriptionFirst'){
      content = [ this.renderDescription(), this.renderPrice(),
                  this.renderResultCount(), this.renderNearestDealer(),
                  this.renderShareButtons() ];
    }
    else if(this.props.arrangement === 'removeFavorite'){
      content = [ this.renderPrice(), this.renderNearestDealer(),
                  this.renderRemoveFavoriteButton() ];
    }
    return (
      <div key="info" className="magazine-preview__info">
        <h2>{ this.props.magazine.name }</h2>
        { content }
      </div>
    );
  },

  renderContent: function(){
    return [ this.renderCover(), this.renderInfo(), this.renderIcon() ];
  },

  render: function(){
    var content = this.renderContent();
    var classes = "magazine-preview";

    if(!this.props.clickable){
      return <div className={classes}>{ content }</div>;
    }

    var url = this.props.context.makePath('magazine',
                                          {id: this.props.magazine.id});
    if(this.props.dealerId){
      url += '?dealerId=' + this.props.dealerId;
    }

    return (
      <NavLink className={classes} context={this.props.context} href={url}>
        { content }
      </NavLink>
    );
  }
});

module.exports = MagazinePreview;
