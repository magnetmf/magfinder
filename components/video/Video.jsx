"use strict";

require('./video.less');

var React = require('react');
var DefaultLayout = require('../general/DefaultLayout.jsx');

var Video = React.createClass({
  render: function(){
    return (
      <DefaultLayout className="video" context={this.props.context}>
        <iframe width="560" height="315"
          src="https://www.youtube.com/embed/Cy9VUNALJ5k" frameborder="0"
          allowfullscreen></iframe>
      </DefaultLayout>
    );
  }
});

module.exports = Video;
