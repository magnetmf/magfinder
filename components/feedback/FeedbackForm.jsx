"use strict";

require('./feedback.less');

var _ = require('lodash');
var React = require('react/addons');
var NavLink = require('flux-router-component').NavLink;
var DefaultLayout = require('../general/DefaultLayout.jsx');
var submitFeedback = require('../../actions/submitFeedback');
var StoreMixin = require('fluxible').StoreMixin;
var FeedbackStore = require('../../stores/FeedbackStore');

var FeedbackForm = React.createClass({
  mixins: [StoreMixin, React.addons.LinkedStateMixin],

  statics: {
    storeListeners: [ FeedbackStore ]
  },

  getInitialState: function(){
    return {
      magazine: this.props.magazine || '',
      store: '',
      comments: '',
      errorMessage: '',
      feedbackResult: null
    };
  },

  getStateFromStores: function(){
    var feedbackResult = this.getStore(FeedbackStore)
                             .getFeedback(this.getFormValues());

    if(feedbackResult && feedbackResult.isRejected()){
      return {
        errorMessage: 'There was an error submitting the form. Please try again.',
        feedbackResult: null
      };
    }
    else {
      return { feedbackResult: feedbackResult };
    }
  },

  getFormValues: function(){
    return _.extend(
      { type: this.props.type },
      _.pick(this.state, ['magazine', 'store', 'comments'])
    );
  },

  onChange: function(){
    this.setState(this.getStateFromStores());
  },

  onSubmit: function(evt){
    evt.preventDefault();

    if(_.isEmpty(this.state.magazine)){
      return this.setState({
        errorMessage: 'Magazine title can\'t be empty.'
      });
    }

    if(_.isEmpty(this.state.store)){
      return this.setState({
        errorMessage: 'Store name can\'t be empty.'
      });
    }

    this.props.context.executeAction(submitFeedback, this.getFormValues());
    this.setState({ errorMessage: null });
  },

  renderErrorMessage: function(){
    if(this.state.errorMessage){
      return (
        <div className="feedback-form__error">
          { this.state.errorMessage }
        </div>
      );
    }
  },

  renderCommentsField: function(){
    if(this.props.type === 'invalid_info'){
      return <textarea name="comments" placeholder="Comments"
        valueLink={this.linkState('comments')} />;
    }
  },

  renderForm: function(){
    return (
      <div className="feedback-form">
        <p>Sorry for the mixup...</p>
        <p>Thank you for using our service. Could you tell us a little more so
          we can improve our service?
        </p>
        <form onSubmit={this.onSubmit}>
          <input type="text" name="magazine" placeholder="Magazine Title"
            valueLink={this.linkState('magazine')} />
          <input type="text" name="store" placeholder="Store Name"
            valueLink={this.linkState('store')} />
          { this.renderCommentsField() }
          { this.renderErrorMessage() }
          <div className="feedback-form__submit"><button>Send</button></div>
        </form>
      </div>
    );
  },

  render: function(){
    if(!this.state.feedbackResult){
      return this.renderForm();
    }
    if(this.state.feedbackResult.isPending()){
      return (
        <div className="feedback-form__loading">
          Loading...
        </div>
      );
    }
    return (
      <div className="feedback-form__success">
        Thank you for your feedback!
      </div>
    );
  }

});

module.exports = FeedbackForm;
