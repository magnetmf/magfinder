"use strict";

require('./feedback.less');

var _ = require('lodash');
var React = require('react');
var NavLink = require('flux-router-component').NavLink;
var DefaultLayout = require('../general/DefaultLayout.jsx');
var FeedbackForm = require('./FeedbackForm.jsx');

var Feedback = React.createClass({
  renderIndex: function(){
    var url = this.props.context.makePath('feedback');
    return (
      <div className="feedback-index">
        <NavLink href={url + '?type=invalid_info'}
          context={this.props.context}>
          My magazine wasn’t at the store.
        </NavLink>
        <NavLink href={url + '?type=magazine_request'}
          context={this.props.context}>
          My magazine isn’t carried around me.
        </NavLink>
      </div>
    );
  },

  renderContent: function(type){
    return {
      'invalid_info': _.bind(function(){
        return <FeedbackForm context={this.props.context} type={type} />;
      }, this),
      'magazine_request': _.bind(function(){
        return <FeedbackForm context={this.props.context} type={type} />;
      }, this),
      'index': _.bind(function(){
        return this.renderIndex();
      }, this)
    }[type]();
  },

  render: function(){
    return (
      <DefaultLayout context={this.props.context}>
        { this.renderContent(this.props.type || 'index') }
      </DefaultLayout>
    );
  }
});

module.exports = Feedback;
