"use strict";

require('./distance_selector.less');

var React = require('react');
var _ = require('lodash');
var OPTIONS = [1,2,4,10,25,50];
var range = OPTIONS.length;

var DistanceSelector = React.createClass({
  getInitialState: function(){
    if(_.contains(OPTIONS, this.props.initialDistance)){
      return { selected: this.props.initialDistance };
    }
    else {
      return { selected: _.last(OPTIONS) };
    }
  },

  onMarkerClick: function(i, evt){
    evt.preventDefault();
    this.setState({ selected: i });
  },

  onApplyClick: function(evt){
    evt.preventDefault();
    if(this.props.initialDistance !== this.state.selected &&
       this.props.onApplyClick)
    {
      this.props.onApplyClick(this.state.selected);
    }
  },

  renderMarkers: function(){
    var selectedIndex = _.indexOf(OPTIONS, this.state.selected);

    return _.map(_.range(range), function(i){
      var className, left, onClick;

      className = "distance-selector__mark";
      if(i > selectedIndex){
        className += " distance-selector__mark--unselected";
      }

      left = i * ( 100 / (range - 1) ) + '%';
      onClick = _.partial(this.onMarkerClick, OPTIONS[i]);

      return <a href="#" key={ "marker" + i } className={className}
                onClick={onClick} style={{ left: left }}></a>;
    }, this);
  },

  renderLabels: function(){
    return _.map(OPTIONS, function(option,i){
      var text = option + ' mile';
      var left = i * ( 100 / (range - 1) ) + '%';
      var width;

      if(option > 1){ text += 's'; }

      if(option === 50){
        width = "52.25px";
        left = "98%";
      }

      return <label key={text} className="distance-selector__label" style={{ left: left, width: width }}>
              { text }</label>;
    });
  },

  render: function(){
    var knobPos = ( 100 / (range - 1) ) * _.indexOf(OPTIONS, this.state.selected) + '%';

    return (
      <div className="distance-selector">
        <div className="distance-selector__bar">
          <div className="distance-selector__fill" style={{ width: knobPos }}></div>
          { this.renderMarkers() }
          <div className="distance-selector__knob" style={{ left: knobPos }}></div>
          { this.renderLabels() }
        </div>
        <div className="distance-selector__button">
          <button onClick={this.onApplyClick}>Apply</button>
        </div>
      </div>
    );
  }
});

module.exports = DistanceSelector;
