"use strict";

var React = require('react');
var GMaps = require("react-google-maps");
var _ = require('lodash');

function dealerPosition(dealer){
  return {
    lat: dealer.latitude,
    lng: dealer.longitude
  };
}

var NearestMap = React.createClass({
  displayName: "NearestMap",

  mixins: [GMaps.GoogleMapsMixin],

  onMarkerClick: function(dealer){
    if(this.props.onMarkerClick){
      this.props.onMarkerClick(dealer);
    }
  },

  renderCurrentLocation: function(){
    if(this.props.currentLocation){
      var icon = "/public/geolocationmarker.png";

      return <GMaps.Marker key="currentLocation" icon={icon}
      position={this.props.currentLocation} />;
    }
  },

  renderMarkers: function(){
    return _.map(this.props.dealers, function(dealer){
      var icon = "/public/darkmarker.png";
      var zIndex;
      if(dealer === this.props.centerOn){
        icon = null; // use google maps default icon
        zIndex = 9999; // make sure active marker is displayed on top
      }
      return <GMaps.Marker key={dealer.id} position={dealerPosition(dealer)}
        icon={icon} zIndex={zIndex} onClick={_.partial(this.onMarkerClick, dealer)} />;
    }, this);
  },

  render: function() {
    return (
      <div style={{height: "100%"}}>
        <GMaps.Map style={{height: "100%"}} zoom={14}
          center={dealerPosition(this.props.centerOn)} />
        { this.renderCurrentLocation() }
        { this.renderMarkers() }
      </div>
    );
  }

});

module.exports = NearestMap;
