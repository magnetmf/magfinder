'use strict';

require('./dealer_preview.less');

var React = require('react/addons');
var cx = React.addons.classSet;
var PriceFormatter = require('../../utils/PriceFormatter');
var Icon = require('../general/Icon.jsx');
var NavLink = require('flux-router-component').NavLink;

var LocationPreview = React.createClass({
  getDefaultProps: function(){
    return { linkToDirectionsAs: 'map' };
  },

  getDirectionsURL: function(){
    var route = this.props.context.makePath('directions',
      { dealerId: this.props.dealer.id }) + '?';

    if(this.props.magazine && this.props.magazine.id){
      route += '&magazineId=' + this.props.magazine.id;
    }

    if(this.props.sidebarDealerId){
      // link to the directions page, but make sure the sidebar links remain
      // filtered by the provided dealer
      route += '&sidebarDealerId=' + this.props.sidebarDealerId;
    }

    return route + '&displayMode=' + (this.props.linkToDirectionsAs || 'map');
  },

  renderPrice: function(){
    if(!this.props.magazine || !this.props.magazine.price){ return; }

    var price = PriceFormatter.split(this.props.magazine.price);
    return (
      <div className="dealer-preview__price">
        <span>{ price.dollars }</span><span>{ price.cents }</span>
      </div>
    );
  },

  renderLink: function(){
    return (
      <NavLink href={this.props.linkURL || this.getDirectionsURL()}
      context={this.props.context}>
        { this.props.linkText || "DIRECTIONS" }
      </NavLink>
    );
  },

  renderCallButton: function(){
    if(!this.props.dealer || !this.props.dealer.phone){ return; }

    var phone = this.props.dealer.phone;

    var formatted = [
      phone.areaCode,
      phone.number.substring(0, 3),
      phone.number.substring(3),
    ].join('-');

    var classes = "dealer-preview__button";
    var href = 'tel://' + formatted;
    var onClick;

    if(phone.number.match(/^0+$/)){
      // phone number is '000'
      classes += " dealer-preview__button--disabled";
      href = "#";
      onClick = function(evt){ evt.preventDefault(); };
    }

    return <a href={href} onClick={onClick} className={classes}>Call</a>;
  },

  render: function(){
    var dealer = this.props.dealer;
    var classes = cx({
      "dealer-preview": true,
      "dealer-preview--active": this.props.active
    });
    var allPublicationsURL = this.props.context.makePath('browse') +
      '?browseBy=dealerCategories&dealerId=' + dealer.id;

    return (
      <div className={classes} id={this.props.id} onClick={this.props.onClick}>
        <div className="dealer-preview__icon">
          <Icon name="marker" />
          { this.renderCallButton() }
        </div>
        <div className="dealer-preview__info">
          <h2>{ dealer.name }</h2>
          <div>
            <span className="dealer-preview__address">{dealer.address}</span>
            <span className="dealer-preview__city-state">
              {dealer.city}, {dealer.state}
            </span>
          </div>
          <div>
            { this.renderPrice() }
            <NavLink href={allPublicationsURL} context={this.props.context}
              className="dealer-preview__button">All Publications</NavLink>
          </div>
        </div>
        <div className="dealer-preview__directions">
          { this.renderLink() }
        </div>
        <Icon name="rightAngle" />
      </div>
    );
  }

});

module.exports = LocationPreview;
