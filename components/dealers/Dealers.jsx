'use strict';

require('./dealers.less');

var React = require('react');
var _ = require('lodash');
var SearchStore = require('../../stores/SearchStore');
var LocationStore = require('../../stores/LocationStore');
var StoreMixin = require('fluxible').StoreMixin;
var ResponsiveStore = require('../../stores/ResponsiveStore');
var TopPanel = require('../top_panel/TopPanel.jsx');
var MagazinePreview = require('../magazine/MagazinePreview');
var DealerPreview = require('./DealerPreview');
var NavLink = require('flux-router-component').NavLink;
var DealersMap = require('./DealersMap');
var Message = require('../general/Message.jsx');
var LoadingMessageMixin = require('../../mixins/LoadingMessageMixin');
var searchMagazines = require('../../actions/searchMagazines');
var DistanceSelector = require('./DistanceSelector.jsx');
var navigateAction = require('flux-router-component').navigateAction;
var Sidebar = require('../sidebar/Sidebar.jsx');

var Dealers = React.createClass({
  mixins: [StoreMixin, LoadingMessageMixin],

  statics: {
    storeListeners: {
      'onSizeChange': ResponsiveStore,
      'onSearchChange': SearchStore
    }
  },

  getDefaultProps: function(){
    return {display: 'list'};
  },

  getStateFromStores: function(){
    return {
      size: this.getStore(ResponsiveStore).getState().size,
      magazine: this.getStore(SearchStore).getMagazine(this.props.id),
      currentLocation: this.getStore(LocationStore).currentLocation
    };
  },

  getInitialState: function() {
    return _.extend({
      googleMapsApi: null,
      displayFilter: false,
      maxDistance: 10,
      selected: null
    }, this.getStateFromStores());
  },

  getActiveDealer: function(){
    return this.state.selected || this.state.magazine.nearestDealer;
  },

  getCurrentLocationCoords: function(){
    var coords = this.state.currentLocation.valueOf().coords;
    return { lat: coords.latitude, lng: coords.longitude };
  },

  onSizeChange: function(){
    this.setState(this.getStateFromStores());
  },

  onSearchChange: function(){
    this.setState(this.getStateFromStores());
    if(this.state.magazine && _.isEmpty(this.getFilteredDealers()) &&
       this.state.maxDistance < 50)
    {
      this.setState({maxDistance: 50});
    }
  },

  componentDidMount: function() {
    /* jshint browser: true */
    this.setState({googleMapsApi: window.google.maps});

    if(!this.state.magazine){
      this.props.context.executeAction(searchMagazines,
                                       { magazineId: this.props.id });
    }
  },

  baseURL: function(){
    return this.props.context.makePath('dealers', {id: this.props.id});
  },

  onDistanceChange: function(newVal){
    this.setState({ maxDistance: newVal, displayFilter: false });
    _.defer(_.bind(function(){
      if(this.state.maxDistance > 10 && _.isEmpty(this.dealersFurtherThan(10))){
        this.props.context.executeAction(searchMagazines, {
          magazineId: this.props.id,
          distance: 50
        });
      }
    }, this));
  },

  onDealerListItemClick: function(dealer){
    this.setState({ selected: dealer });

    if(this.props.display === 'list'){
      this.props.context.executeAction(navigateAction, {
        url: this.baseURL() + '?display=map'
      });
    }
  },

  onMarkerClick: function(dealer){
    this.setState({ selected: dealer });

    // scroll dealers list so selected dealer is visible
    window.location.hash = this.dealerHtmlId(dealer);
  },

  toggleFilter: function(evt){
    if(evt){ evt.preventDefault(); }
    this.setState({ displayFilter: !this.state.displayFilter });
  },

  getFilteredDealers: function(){
    if(!this.state.maxDistance){
      return this.state.magazine.dealers;
    }

    return _.filter(this.state.magazine.dealers, function(dealer){
      return dealer.distance <= this.state.maxDistance;
    }, this);
  },

  dealersFurtherThan: function(miles){
    return _.filter(this.state.magazine.dealers, function(dealer){
      return dealer.distance > miles;
    }, this);
  },

  dealerHtmlId: function(dealer){
    return "dealers__list__dealer--" + dealer.id;
  },

  renderMagazinePreview: function(){
    var clickable = true;
    var arrangement = 'default';
    if(this.state.size !== ResponsiveStore.MOBILE){
      clickable = false;
      arrangement = 'descriptionFirst';
    }

    return <MagazinePreview magazine={this.state.magazine} clickable={clickable}
      context={this.props.context} arrangement={arrangement} />;
  },

  renderResultCount: function(){
    if(this.state.size !== ResponsiveStore.MOBILE){ return; }
    return (
      <div className="dealers__count">
        <div>{ this.state.magazine.dealers.length + " Local Results" }</div>
        <div>Current Location</div>
      </div>
    );
  },

  renderButtons: function(){
    var baseURL = this.baseURL();
    var classes = {};
    classes[this.props.display] = " active";

    return (
      <div className="dealers__buttons">
        <div>
          <a href="#" key="distance" className="dealers__buttons__distance"
            onClick={this.toggleFilter}>Distance</a>
        </div>
        <div>
          <NavLink key="list" context={this.props.context}
            href={baseURL + '?display=list'}
            className={ "dealers__buttons__list" + classes.list }
          >List</NavLink>
          <NavLink key="map" context={this.props.context}
            className={ "dealers__buttons__list" + classes.map }
            href={baseURL + '?display=map'}
          >Map</NavLink>
        </div>
      </div>
    );
  },

  renderDealersAsList: function(){
    if(this.state.size === ResponsiveStore.MOBILE &&
    this.props.display !== "list"){ return null; }

    var dealers = _.map(_.sortBy(this.getFilteredDealers(), 'distance'),
    function(dealer){
      return <DealerPreview key={dealer.id} context={this.props.context}
        onClick={_.partial(this.onDealerListItemClick, dealer)}
        dealer={dealer} magazine={this.state.magazine}
        active={dealer === this.getActiveDealer()}
        id={this.dealerHtmlId(dealer)} sidebarDealerId={this.props.dealerId} />;
    }, this);

    return <div className="dealers__list">{ dealers }</div>;
  },

  renderDealersAsMap: function(){
    if(this.state.size === ResponsiveStore.MOBILE &&
    this.props.display !== "map" || !this.state.googleMapsApi){ return null; }

    return (
      <div className="dealers__map">
        <DealersMap googleMapsApi={this.state.googleMapsApi}
          dealers={this.getFilteredDealers()}
          centerOn={this.getActiveDealer()}
          onMarkerClick={this.onMarkerClick}
          currentLocation={this.getCurrentLocationCoords()}
        />
      </div>
    );
  },

  renderDealerPreview: function(){
    if(this.state.size !== ResponsiveStore.MOBILE ||
    this.props.display !== "map"){ return null; }

    return (
      <div className="dealers__footer">
        <DealerPreview magazine={this.state.magazine}
          context={this.props.context}
          dealer={this.getActiveDealer()} />
      </div>
    );
  },

  renderFilter: function(){
    if(!this.state.displayFilter){ return; }

    return <DistanceSelector initialDistance={this.state.maxDistance}
           onApplyClick={this.onDistanceChange} />;
  },

  renderSidebar: function(){
    if(this.state.size !== ResponsiveStore.DESKTOP){ return; }
    var activeCategory;
    if(this.state.magazine){
      activeCategory = this.state.magazine.category;
    }

    return (
      <div className="dealers__sidebar">
        <Sidebar context={this.props.context} dealerId={this.props.dealerId}
          activeCategory={activeCategory} />
      </div>
    );
  },

  renderContent: function(){
    return (
      <div className="dealers">
        <TopPanel context={this.props.context} />
        { this.renderSidebar() }
        <div className="dealers__main"><div>
          <div className="dealers__header">
            { this.renderResultCount() }
            { this.renderMagazinePreview() }
            { this.renderButtons() }
            { this.renderFilter() }
          </div>
          <div className="dealers__content"><div>
            { this.renderDealersAsList() }
            { this.renderDealersAsMap() }
          </div></div>
          { this.renderDealerPreview() }
        </div></div>
      </div>
    );
  },

  render: function(){
    if(this.state.magazine){
      return this.renderContent();
    }

    var message = this.renderLoadingMessage(this.props.context,
                                            'searchResults');
    if(!message){
      message = <Message type="empty">No results found.</Message>;
    }

    return (
      <div className="dealers">
        <TopPanel context={this.props.context} />
        { message }
      </div>
    );
  }

});

module.exports = Dealers;
