"use strict";

require('./category.less');

var React = require('react');
var _ = require('lodash');
var TopPanel = require('../top_panel/TopPanel.jsx');
var MostPopular = require('../most_popular/MostPopular.jsx');
var SearchStore = require('../../stores/SearchStore');
var StoreMixin = require('fluxible').StoreMixin;
var MagazinePreview = require('../magazine/MagazinePreview');
var NavLink = require('flux-router-component').NavLink;
var LoadingMessageMixin = require('../../mixins/LoadingMessageMixin');
var searchMagazines = require('../../actions/searchMagazines');
var Sidebar = require('../sidebar/Sidebar.jsx');
var ResponsiveStore = require('../../stores/ResponsiveStore');
var DealerPreview = require('../dealers/DealerPreview');
var DealerStore = require('../../stores/DealerStore');

var Category = React.createClass({
  mixins: [StoreMixin, LoadingMessageMixin],

  statics: {
    storeListeners: {
      'onSearchChange': SearchStore,
      'onSizeChange': ResponsiveStore
    }
  },

  getStateFromStore: function(){
    return _.extend(
      _.pick(this.getStore(SearchStore).getState(), ['searchStr', 'results']),
      _.pick(this.getStore(ResponsiveStore).getState(), ['size'])
    );
  },

  getInitialState: function(){
    return this.getStateFromStore();
  },

  onSearchChange: function(){
    this.setState(this.getStateFromStore());
  },

  onSizeChange: function(){
    this.setState(this.getStateFromStore());
  },

  baseURL: function(){
    var url = this.props.context.makePath('category', {id: this.props.id}) +'?';
    if(this.props.dealerId){
      url += '&dealerId=' + this.props.dealerId;
    }
    return url;
  },

  componentDidMount: function(){
    if(!this.state.results || this.props.id !== this.state.searchStr){
      this.props.context.executeAction(searchMagazines,
        { categoryName: this.props.id, dealerId: this.props.dealerId });
    }
  },

  componentWillReceiveProps: function(nextProps){
    if(nextProps.id !== this.state.searchStr){
      this.props.context.executeAction(searchMagazines,
        { categoryName: nextProps.id, dealerId: this.props.dealerId });
    }
  },

  renderSortButtons: function(){
    if(!this.props.sort){ return; }

    var baseURL = this.baseURL();
    var classes = {};
    classes[this.props.sort] = "active";
    return (
      <div className="category__sort-buttons">
        <NavLink key="name" href={baseURL + '&sort=name'}
          className={classes.name} context={this.props.context}
        >Name</NavLink>
        <NavLink key="distance" href={baseURL + '&sort=distance'}
          className={classes.distance} context={this.props.context}
        >Distance</NavLink>
        <NavLink key="price" href={baseURL + '&sort=price'}
          className={classes.price} context={this.props.context}
        >Price</NavLink>
        <NavLink key="cancel" href={baseURL} context={this.props.context}
        >x</NavLink>
      </div>
    );
  },

  renderResultCount: function(){
    var resultCount = "";

    if(this.state.results && this.state.results.isFulfilled()){
      resultCount = this.state.results.valueOf().length + " Results";
    }

    return (
      <div className="category__count">
        <div>{ this.props.id }</div>
        <div>{ resultCount }</div>
      </div>
    );
  },

  renderMagazines: function(){
    var loadingMessage = this.renderLoadingMessage(this.props.context,
                                                   'searchResults');
    if(loadingMessage){ return loadingMessage; }

    var magazines = this.state.results.valueOf();

    var sortParam = {
      'name': 'name',
      'distance': 'nearestDistance',
      'price': 'price',
      'popularity': function(magazine){
        var value = -1 * magazine.popularityInCategory;
        if(magazine.coverImageUrl.match(/DefaultCover/)){
          // we want magazines with default cover images to be pushed to the bottom
          value += 99;
        }
        return value;
      }
    }[this.props.sort || 'popularity'];

    if(sortParam){
      magazines = _.sortBy(magazines, sortParam);
    }

    return _.map(magazines, function(magazine){
      return <MagazinePreview key={magazine.id} magazine={magazine}
        arrangement="minimal" dealerId={this.props.dealerId}
        context={this.props.context} />;
    }, this);
  },

  renderSidebar: function(){
    if(this.state.size !== ResponsiveStore.DESKTOP){ return; }
    return <Sidebar context={this.props.context} dealerId={this.props.dealerId}
            activeCategory={this.props.id} />;
  },

  renderFooter: function(){
    var dealer;
    if(this.props.dealerId){
      dealer = this.getStore(DealerStore).getDealer(this.props.dealerId);
    }

    if(dealer){
      return (
        <div className="category__footer">
          <DealerPreview key={dealer.id} context={this.props.context}
            dealer={dealer} />
        </div>
      );
    }

    return <MostPopular context={this.props.context}/>;
  },

  render: function(){
    var sortURL = this.baseURL() + '&sort=name';

    return (
      <div className="category">
        <TopPanel browseURL={sortURL} browseText="Sort"
          context={this.props.context} />
        <div className="category__content">
          { this.renderSidebar() }
          <div className="category__magazines">
            { this.renderSortButtons() }
            { this.renderResultCount() }
            { this.renderMagazines() }
          </div>
        </div>
        { this.renderFooter() }
      </div>
    );
  },

});

module.exports = Category;
