"use strict";

require('./signup.less');

var _ = require('lodash');
var React = require('react/addons');
var cx = React.addons.classSet;
var ModalLayout = require('../general/ModalLayout.jsx');
var RequireLoginMixin = require('../../mixins/RequireLoginMixin');
var StoreMixin = require('fluxible').StoreMixin;
var UserStore = require('../../stores/UserStore');
var createUser = require('../../actions/createUser');
var loginUser = require('../../actions/loginUser');

var SignupWithEmail = React.createClass({
  mixins: [React.addons.LinkedStateMixin, StoreMixin, RequireLoginMixin],

  statics: {
    storeListeners: [UserStore]
  },

  getInitialState: function(){
    return _.extend({
      tab: 'login',
      email: '',
      password: '',
      passwordConfirmation: '',
      errorMessage: '',
      newUser: null
    }, this.getStateFromStores());
  },

  getStateFromStores: function(){
    var state = {
      user: this.getStore(UserStore).getUser(),
      newUser: this.getStore(UserStore).getNewUser()
    };

    var errorMessage = this.getErrorMessageFromStores();
    if(errorMessage){
      state.errorMessage = errorMessage;
    }

    return state;
  },

  getErrorMessageFromStores: function(){
    var newUser = this.getStore(UserStore).getNewUser();

    if(!newUser || !newUser.isRejected()){
      return;
    }

    if(newUser.valueOf().exception.message === 'user_not_found'){
      return 'Invalid email/password combination.';
    }

    if(newUser.valueOf().exception.message === 'unique_violation'){
      return 'Email is already in use.';
    }

    var tab = (this.state || {}).tab;
    return {
      login: 'There was an error trying to login.',
      signup: 'There was an error trying to create the account.',
    }[tab] || 'Unkown error';
  },

  componentDidMount: function(){
    this.redirectIfUser();
  },

  onChange: function(){
    this.setState(this.getStateFromStores());
    this.redirectIfUser();
  },

  onTabClick: function(name, evt){
    evt.preventDefault();
    this.setState({ tab: name, errorMessage: '' });
  },

  onSubmit: function(evt){
    evt.preventDefault();

    if(this.state.newUser && !this.state.newUser.isRejected()){
      return;
    }

    if(_.isEmpty(this.state.email)){
      return this.setState({
        errorMessage: 'Email can\'t be empty.'
      });
    }

    if(_.isEmpty(this.state.password)){
      return this.setState({
        errorMessage: 'Password can\'t be empty.'
      });
    }

    if(this.state.tab === 'signup' &&
    this.state.password !== this.state.passwordConfirmation){
      return this.setState({
        errorMessage: 'Password confirmation does not match password.'
      });
    }

    this.setState({ errorMessage:  '' });

    if(this.state.tab === 'signup'){
      this.props.context.executeAction(createUser, {
        email: this.state.email,
        password: this.state.password
      });
    }
    else if(this.state.tab === 'login'){
      this.props.context.executeAction(loginUser, {
        email: this.state.email,
        password: this.state.password
      });
    }
    else {
      throw 'invalid tab ' + this.state.tab;
    }
  },

  renderTab: function(name){
    var attrs = {
      login: { label: 'Login' },
      signup: { label: 'Sign up' },
    }[name];

    if(!attrs){ throw 'invalid tab ' + name; }

    var classes = cx({
      "signup-with-email__tab": true,
      "signup-with-email__tab--active": this.state.tab === name
    });

    return (
      <a href="#" className={classes}
      onClick={_.partial(this.onTabClick, name)}>
        { attrs.label }
      </a>
    );
  },

  renderTabs: function(){
    return (
      <div>
        {this.renderTab('login') }
        {this.renderTab('signup') }
      </div>
    );
  },

  renderForm: function(){
    var fields = [
      <input type="email" key="email" placeholder="Email"
        valueLink={this.linkState('email')} />,
      <input type="password" key="password" placeholder="Password"
        valueLink={this.linkState('password')} />
    ];

    if(this.state.tab === 'login'){
      fields.push(
        <input type="submit" key="signInSubmit" value="Sign In" />
      );
    }
    else if(this.state.tab === 'signup'){
      fields.push(
        <input type="password" key="passwordConfirmation"
          placeholder="Confirm Password"
          valueLink={this.linkState('passwordConfirmation')} />,
        <input type="submit" key="signInSubmit" value="Create Account" />
      );
    }
    else {
      throw 'Invalid tab ' + this.state.tab;
    }

    return (
      <form className="signup-with-email__form" onSubmit={this.onSubmit}>
        { fields }
      </form>
    );
  },

  renderLoadingMessage: function(){
    if(this.state.newUser && this.state.newUser.isPending()){
      return (
        <div className="signup-with-email__loading">Loading...</div>
      );
    }
  },


  renderErrorMessage: function(){
    if(!this.state.errorMessage){ return; }

    return (
      <div className="signup-with-email__error">{ this.state.errorMessage }</div>
    );
  },

  render: function(){
    return (
      <ModalLayout className="signup-with-email" context={this.props.context}>
        { this.renderTabs() }
        { this.renderForm() }
        { this.renderLoadingMessage() }
        { this.renderErrorMessage() }
      </ModalLayout>
    );
  }

});

module.exports = SignupWithEmail;
