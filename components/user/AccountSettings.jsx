"use strict";

var React = require('react');
var UserNameForm = require('./UserNameForm.jsx');
var UserEmailForm = require('./UserEmailForm.jsx');
var UserPhoneForm = require('./UserPhoneForm.jsx');
var CommunicationPreferencesForm = require('./CommunicationPreferencesForm.jsx');

var AccountSettings = React.createClass({
  render: function(){
    return (
      <div className="account-settings">
        <UserNameForm context={this.props.context} />
        <UserEmailForm context={this.props.context} />
        <UserPhoneForm context={this.props.context} />
        <CommunicationPreferencesForm context={this.props.context} />
      </div>
    );
  }
});

module.exports = AccountSettings;
