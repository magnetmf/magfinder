"use strict";

require('./favorite_magazines.less');

var _ = require('lodash');
var React = require('react');
var searchFavoriteMagazines = require('../../actions/searchFavoriteMagazines');
var UserStore = require('../../stores/UserStore');
var StoreMixin = require('fluxible').StoreMixin;
var MagazinePreview = require('../magazine/MagazinePreview');

var FavoriteMagazines = React.createClass({
  mixins: [StoreMixin],

  statics: {
    storeListeners: [UserStore]
  },

  getInitialState: function(){
    return this.getStateFromStores();
  },

  componentDidMount: function(){
    if(!this.state.magazines){
      this.props.context.executeAction(searchFavoriteMagazines);
    }
  },

  getStateFromStores: function(){
    return {
      magazines: this.getStore(UserStore).getFavoriteMagazines()
    };
  },

  onChange: function(){
    this.setState(this.getStateFromStores());
  },

  renderLoading: function(){
    return (
      <div className="favorite-magazines">
        <div className="favorite-magazines__loading">Loading...</div>
      </div>
    );
  },

  renderLoadError: function(){
    return (
      <div className="favorite-magazines">
        <div className="favorite-magazines__error">
          There was an error trying to load favorite magazines.
        </div>
      </div>
    );
  },

  renderEmpty: function(){
    return (
      <div className="favorite-magazines">
        <div className="favorite-magazines__msg">
          No magazines have been marked as favorite.
        </div>
      </div>
    );
  },

  renderMagazines: function(){
    var magazines = _.map(this.state.magazines.valueOf(), function(magazine){
      return <MagazinePreview key={magazine.id} magazine={magazine}
              arrangement="removeFavorite" context={this.props.context} />;
    }, this);
    return (
      <div className="favorite-magazines">{ magazines }</div>
    );
  },

  render: function(){
    if(!this.state.magazines || this.state.magazines.isPending()){
      return this.renderLoading();
    }

    if(this.state.magazines.isRejected()){
      return this.renderLoadError();
    }

    var magazines = this.state.magazines.valueOf();
    if(_.isEmpty(magazines)){
      return this.renderEmpty();
    }

    return this.renderMagazines();
  }
});

module.exports = FavoriteMagazines;
