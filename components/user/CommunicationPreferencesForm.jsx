"use strict";

require('react-toggle/style.css');

var _ = require('lodash');
var Q = require('q');
var React = require('react/addons');
var UserStore = require('../../stores/UserStore');
var StoreMixin = require('fluxible').StoreMixin;
var Toggle = require('react-toggle');
var searchCommunicationPreferences = require('../../actions/' +
                                             'searchCommunicationPreferences');
var updateCommunicationPreferences = require('../../actions/' +
                                             'updateCommunicationPreferences');

var CommunicationPreferencesForm = React.createClass({
  mixins: [StoreMixin],

  statics: {
    storeListeners: [UserStore]
  },

  getInitialState: function(){
    var preferences = this.getStore(UserStore).getCommunicationPreferences();
    if(!preferences){
      this.props.context.executeAction(searchCommunicationPreferences);
    }
    return {
      preferences: preferences,
      errorMsg: null
    };
  },

  onChange: function(){
    this.setState({
      preferences: this.getStore(UserStore).getCommunicationPreferences()
    });
  },

  onInputChange: function(name, evt){
    var payload = {};
    payload[name] = evt.target.checked;
    this.props.context.executeAction(updateCommunicationPreferences, payload);
  },

  renderErrorMessage: function(){
    if(!this.state.errorMsg){ return; }

    return <div className="user-form__error">{ this.state.errorMsg }</div>;
  },

  render: function(){
    var preferences;
    if(this.state.preferences && this.state.preferences.isFulfilled()){
      preferences = this.state.preferences.valueOf();
    }
    if(!preferences){
      preferences = {};
    }

    return (
      <div className="communication-preferences-form">
        <h2>Communication Preferences</h2>
        <label>
          <Toggle checked={preferences.receiveSMS}
            id="communication-preferences-sms-input"
            onChange={_.partial(this.onInputChange, 'receiveSMS')} />
          <span>SMS</span>
        </label>
        <label>
          <Toggle checked={preferences.receiveEmail}
            id="communication-preferences-email-input"
            onChange={_.partial(this.onInputChange, 'receiveEmail')} />
          <span>Email</span>
        </label>
      </div>
    );
  }

});

module.exports = CommunicationPreferencesForm;
