"use strict";

var _ = require('lodash');
var Q = require('q');
var React = require('react/addons');
var cx = React.addons.classSet;
var UserStore = require('../../stores/UserStore');
var StoreMixin = require('fluxible').StoreMixin;
var searchUserPhone = require('../../actions/searchUserPhone');
var updateUserPhone = require('../../actions/updateUserPhone');
var NavLink = require('flux-router-component').NavLink;

var UserPhoneForm = React.createClass({
  mixins: [StoreMixin],

  statics: {
    storeListeners: [UserStore]
  },

  getInitialState: function(){
    var phone = this.getStore(UserStore).getPhone();
    if(!phone){
      this.props.context.executeAction(searchUserPhone);
    }
    return {
      mode: 'display',
      phone: phone,
      errorMsg: null
    };
  },

  onChange: function(){
    this.setState({
      phone: this.getStore(UserStore).getPhone(),
      mode: 'display'
    });
  },

  onEditClick: function(){
    this.setState({ mode: 'edit', errorMsg: null });
  },

  onCancelClick: function(evt){
    evt.preventDefault();
    this.setState({ mode: 'display', errorMsg: null });
  },

  onSubmit: function(evt){
    evt.preventDefault();
    var phone = this.refs.input.getDOMNode().value;
    if(_.isEmpty(phone)){
      this.setState({ errorMsg: "Phone can't be empty" });
    }
    else {
      this.props.context.executeAction(updateUserPhone, {phone: phone});
      this.setState({ mode: 'display', errorMsg: null, phone: Q.when(phone)});
    }
  },

  renderErrorMessage: function(){
    if(!this.state.errorMsg){ return; }

    return <div className="user-form__error">{ this.state.errorMsg }</div>;
  },

  renderVerifyButton: function(phone){
    if(phone && phone.phone && !phone.verifiedAt){
      var url = this.props.context.makePath('verifyPhone');
      return <NavLink className="user-form__verify-btn" href={url}
        context={this.props.context}>Verify</NavLink>;
    }
  },

  renderDisplayMode: function(phone){
    var classes = cx({
      "user-form": true,
      "user-form--empty": !phone.phone
    });

    return (
      <div className={classes}>
        <div className="user-form__field">
          <span>{ phone.phone || 'Phone' }</span>
        </div>
        <div className="user-form__buttons">
          <button onClick={this.onEditClick}>Edit</button>
          { this.renderVerifyButton(phone) }
        </div>
      </div>
    );
  },

  renderEditMode: function(phone){
    return (
      <div className="user-form">
        <form onSubmit={this.onSubmit}>
          <div className="user-form__field">
            <input ref="input" type="phone" defaultValue={phone.phone}
              autoFocus={true} placeholder="Phone" />
            { this.renderErrorMessage() }
          </div>
          <div className="user-form__buttons">
            <button>Save</button>
            <button className="user-form__cancel-btn"
              onClick={this.onCancelClick}>Cancel</button>
          </div>
        </form>
      </div>
    );
  },

  render: function(){
    var phone;
    if(this.state.phone && this.state.phone.isFulfilled()){
      phone = this.state.phone.valueOf();
    }
    phone = phone || {};

    if(this.state.mode === 'display'){
      return this.renderDisplayMode(phone);
    }
    else if(this.state.mode === 'edit') {
      return this.renderEditMode(phone);
    }
  }

});

module.exports = UserPhoneForm;
