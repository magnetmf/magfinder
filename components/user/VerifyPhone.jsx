"use strict";

require('./verify-phone.less');

var _ = require('lodash');
var React = require('react/addons');
var cx = React.addons.classSet;
var ModalLayout = require('../general/ModalLayout.jsx');
var StoreMixin = require('fluxible').StoreMixin;
var RequireLoginMixin = require('../../mixins/RequireLoginMixin');
var UserStore = require('../../stores/UserStore');
var searchUserPhone = require('../../actions/searchUserPhone');
var verifyPhone = require('../../actions/verifyPhone');
var refreshPhoneVerificationCode = require('../../actions/refreshPhoneVerificationCode');
var Loader = require('react-loader');

var VerifyPhone = React.createClass({
  mixins: [StoreMixin, RequireLoginMixin],

  statics: {
    storeListeners: [UserStore]
  },

  getInitialState: function(){
    return _.extend({ code: [] }, this.getStateFromStores());
  },

  getStateFromStores: function(){
    var phone = this.getStore(UserStore).getPhone();

    if(!phone){
      this.props.context.executeAction(searchUserPhone);
    }

    return { phone: phone };
  },

  componentDidMount: function(){
    this.redirectIfNoUser();
    this.props.context.executeAction(refreshPhoneVerificationCode);
  },

  focusNextField: function(i){
    if(i < 5){
      this.refs['char' + (i+1)].getDOMNode().focus();
    }
  },

  focusPreviousField: function(i){
    if(i > 0){
      this.refs['char' + (i-1)].getDOMNode().focus();
    }
  },

  onChange: function(){
    this.setState(this.getStateFromStores());
    this.redirectIfNoUser();
  },

  onInputKeyDown: function(i, evt){
    if(evt.key === 'Backspace' && evt.target.value === ''){
      this.focusPreviousField(i);
    }
  },

  onInputChange: function(i, evt){
    var value = evt.target.value[0];

    if(_.isFinite(value)){
      this.focusNextField(i);
    }
    else{
      value = '';
    }

    var newCode = _.clone(this.state.code);
    newCode[i] = value;

    this.setState({ code: newCode });
  },

  onSubmit: function(evt){
    evt.preventDefault();

    if(this.isCodeComplete()){
      var code = this.state.code.join('');
      this.props.context.executeAction(verifyPhone, { verificationCode: code });
    }
  },

  isCodeComplete: function(){
    return _.filter(this.state.code, _.isFinite).length === 6;
  },

  renderInputFields: function(){
    var fields = _.times(6, function(i){
      return <input type="text" key={'char'+i} value={this.state.code[i]}
        ref={'char'+i} onChange={_.partial(this.onInputChange, i)}
        onKeyDown={_.partial(this.onInputKeyDown, i)} />;
    }, this);
    fields.splice(3, 0, <span className="verify-phone__separator" />);
    return <div className="verify-phone__code">{ fields }</div>;
  },

  renderSubmitButton: function(){
    var classes = cx({
      "verify-phone__submit-btn": true,
      "verify-phone__submit-btn--active": this.isCodeComplete()
    });
    return (
      <button className={classes}>Verify and Complete</button>
    );
  },

  renderStatusMessage: function(){
    var verification = this.getStore(UserStore)
                           .getPhoneVerification(this.state.code.join(''));
    if(!verification){
      return;
    }

    var classes = "verify-phone__status-msg";
    var content;

    if(verification.isPending()){
      var options = { lines: 9, length: 3, width: 1, radius: 3 };
      content = <Loader options={options} />;
    }
    else if(verification.isFulfilled()){
      classes += " verify-phone__status-msg--success";
      content = "Success!";
    }
    else if(verification.isRejected()){
      classes += " verify-phone__status-msg--error";
      var errorCode = verification.valueOf().exception;

      if(errorCode === 403){
        content = "Oops. Looks like that code is incorrect.";
      }
      else if(errorCode === 409){
        content = "Verification code has expired. Please reload the page to " +
                  "request a new one.";
      }
      else {
        content = "There was an error trying to verify code.";
      }
    }

    return <div className={classes}>{content}</div>;
  },

  render: function(){
    var content = <div />;

    if(this.state.phone && this.state.phone.isFulfilled()){
      var phone = this.state.phone.valueOf().phone;
      content = (
        <form onSubmit={this.onSubmit}>
          <h2>Enter the verification code</h2>
          { this.renderStatusMessage() }
          { this.renderInputFields() }
          <div className="verify-phone__msg">
            <div>We just sent a text message to:</div>
            <div>{phone}</div>
          </div>
          { this.renderSubmitButton() }
        </form>
      );
    }

    return (
      <ModalLayout className="verify-phone" context={this.props.context}>
        { content }
      </ModalLayout>
    );
  }

});

module.exports = VerifyPhone;
