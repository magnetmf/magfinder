"use strict";

var _ = require('lodash');
var Q = require('q');
var React = require('react/addons');
var cx = React.addons.classSet;
var UserStore = require('../../stores/UserStore');
var StoreMixin = require('fluxible').StoreMixin;
var searchUserEmail = require('../../actions/searchUserEmail');
var updateUserEmail = require('../../actions/updateUserEmail');

var UserEmailForm = React.createClass({
  mixins: [StoreMixin],

  statics: {
    storeListeners: [UserStore]
  },

  getInitialState: function(){
    var email = this.getStore(UserStore).getEmail();
    if(!email){
      this.props.context.executeAction(searchUserEmail);
    }
    return {
      mode: 'display',
      email: email,
      errorMsg: null
    };
  },

  onChange: function(){
    this.setState({
      email: this.getStore(UserStore).getEmail(),
      mode: 'display'
    });
  },

  onEditClick: function(){
    this.setState({ mode: 'edit', errorMsg: null });
  },

  onCancelClick: function(evt){
    evt.preventDefault();
    this.setState({ mode: 'display', errorMsg: null });
  },

  onSubmit: function(evt){
    evt.preventDefault();
    var email = this.refs.input.getDOMNode().value;
    if(_.isEmpty(email)){
      this.setState({ errorMsg: "Email can't be empty" });
    }
    else {
      this.props.context.executeAction(updateUserEmail, {email: email});
      this.setState({ mode: 'display', errorMsg: null, email: Q.when(email)});
    }
  },

  renderErrorMessage: function(){
    if(!this.state.errorMsg){ return; }

    return <div className="user-form__error">{ this.state.errorMsg }</div>;
  },

  renderDisplayMode: function(email){
    var classes = cx({
      "user-form": true,
      "user-form--empty": !email.email
    });

    return (
      <div className={classes}>
        <div className="user-form__field">
          <span>{ email.email || 'Email' }</span>
        </div>
        <div className="user-form__buttons">
          <button onClick={this.onEditClick}>Edit</button>
        </div>
      </div>
    );
  },

  renderEditMode: function(email){
    return (
      <div className="user-form">
        <form onSubmit={this.onSubmit}>
          <div className="user-form__field">
            <input ref="input" type="email" defaultValue={email.email}
              autoFocus={true} placeholder="Email" />
            { this.renderErrorMessage() }
          </div>
          <div className="user-form__buttons">
            <button>Save</button>
            <button className="user-form__cancel-btn"
              onClick={this.onCancelClick}>Cancel</button>
          </div>
        </form>
      </div>
    );
  },

  render: function(){
    var email;
    if(this.state.email && this.state.email.isFulfilled()){
      email = this.state.email.valueOf();
    }
    email = email || {};

    if(this.state.mode === 'display'){
      return this.renderDisplayMode(email);
    }
    else if(this.state.mode === 'edit') {
      return this.renderEditMode(email);
    }
  }

});

module.exports = UserEmailForm;
