"use strict";

require('./account.less');

var _ = require('lodash');
var React = require('react');
var TopPanel = require('../top_panel/TopPanel.jsx');
var UserStore = require('../../stores/UserStore');
var StoreMixin = require('fluxible').StoreMixin;
var NavLink = require('flux-router-component').NavLink;
var AccountSettings = require('./AccountSettings.jsx');
var FavoriteMagazines = require('./FavoriteMagazines.jsx');
var RequireLoginMixin = require('../../mixins/RequireLoginMixin');

var Account = React.createClass({
  mixins: [StoreMixin, RequireLoginMixin],

  statics: {
    storeListeners: [UserStore]
  },

  getStateFromStores: function(){
    return { user: this.getStore(UserStore).getState().user };
  },

  getInitialState: function(){
    return this.getStateFromStores();
  },

  onChange: function(){
    this.setState(this.getStateFromStores());
    this.redirectIfNoUser();
  },

  componentDidMount: function(){
    this.redirectIfNoUser();
  },

  activePanel: function(){
    return this.props.active || 'Settings';
  },

  renderNavButton: function(label){
    var url = this.props.context.makePath('account') + '?active=' + label;

    var classes = '';
    if(label === this.activePanel()) {
      classes = 'active';
    }

    return <NavLink key={label} className={classes} href={url}
            context={this.props.context}>{ label }</NavLink>;
  },

  renderNavButtons: function(){
    var buttons = _.map(['Favorites', 'Settings'],
                        this.renderNavButton);
    return <div className="account__nav-buttons">{ buttons }</div>;
  },

  renderActivePanel: function(){
    return {
      Favorites: function(self){
        return <FavoriteMagazines context={self.props.context} />;
      },

      Interests: function(){
        return <div>Interests</div>;
      },

      Settings: function(self ){
        return  <AccountSettings context={self.props.context} />;
      }
    }[this.activePanel()](this);
  },

  render: function(){
    if(!this.state.user){
      return <div></div>;
    }

    return (
      <div className="account">
        <TopPanel context={this.props.context} />
        { this.renderNavButtons() }
        <div className="account__content">
          { this.renderActivePanel() }
        </div>
        <div className="account__footer">
          <a href="/logout">Sign Out</a>
        </div>
      </div>
    );
  }
});

module.exports = Account;
