"use strict";

require('./signup.less');

var React = require('react');
var Icon = require('../general/Icon.jsx');
var ModalLayout = require('../general/ModalLayout.jsx');
var navigateAction = require('flux-router-component').navigateAction;

function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

var SignupIndex = React.createClass({
  onClick: function(evt){
    evt.preventDefault();
    this.props.context.executeAction(navigateAction, {
      url: evt.currentTarget.getAttribute('href')
    });
  },

  renderSignupButton: function(name){
    var url = {
      facebook: '/auth/facebook',
      twitter: '/auth/twitter',
      email: '/signup/email'
    }[name];

    var className = "signup-index__signup-btn signup-index__signup-btn--" +
                    name;

    var onClick = name === 'email' ? this.onClick : null;

    return (
      <a href={url} className={className} onClick={onClick}>
        <Icon name={name} /> { "Login with " + capitalize(name) }
      </a>
    );
  },

  render: function(){
    return (
      <ModalLayout className="signup-index" context={this.props.context}>
        <h2>Login to Magfinder</h2>
        { this.renderSignupButton('facebook') }
        { this.renderSignupButton('twitter') }
        <div className="signup-index__divider">
          <span>or</span>
        </div>
        { this.renderSignupButton('email') }
        <p className="signup-index__disclaimer">
          By signing up, I agree to Magfinder's <a href="#">Terms of Service.</a>
          and <a href="#">Privacy Policy</a>.
        </p>
      </ModalLayout>
    );
  }
});

module.exports = SignupIndex;
