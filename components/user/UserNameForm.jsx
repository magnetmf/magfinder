"use strict";

var _ = require('lodash');
var React = require('react');
var UserStore = require('../../stores/UserStore');
var StoreMixin = require('fluxible').StoreMixin;
var updateUser = require('../../actions/updateUser');

var UserNameForm = React.createClass({
  mixins: [StoreMixin],

  statics: {
    storeListeners: [UserStore]
  },

  getInitialState: function(){
    var user = this.getStore(UserStore).getState().user || {};
    return {
      mode: 'display',
      name: user.name,
      errorMsg: null
    };
  },

  onChange: function(){
    var user = this.getStore(UserStore).getState().user || {};
    this.setState({
      name: user.name,
      mode: 'display'
    });
  },

  onEditClick: function(){
    this.setState({ mode: 'edit', errorMsg: null });
  },

  onCancelClick: function(evt){
    evt.preventDefault();
    this.setState({ mode: 'display', errorMsg: null });
  },

  onSubmit: function(evt){
    evt.preventDefault();
    var name = this.refs.input.getDOMNode().value;
    if(_.isEmpty(name)){
      this.setState({ errorMsg: "Name can't be empty" });
    }
    else {
      this.props.context.executeAction(updateUser, {name: name});
      this.setState({ mode: 'display', errorMsg: null, name: name});
    }
  },

  renderErrorMessage: function(){
    if(!this.state.errorMsg){ return; }

    return <div className="user-form__error">{ this.state.errorMsg }</div>;
  },

  renderDisplayMode: function(){
    return (
      <div className="user-form">
        <div className="user-form__field">
          <span>{ this.state.name }</span>
        </div>
        <div className="user-form__buttons">
          <button onClick={this.onEditClick}>Edit</button>
        </div>
      </div>
    );
  },

  renderEditMode: function(){
    return (
      <div className="user-form">
        <form onSubmit={this.onSubmit}>
          <div className="user-form__field">
            <input ref="input" type="text" defaultValue={this.state.name}
              autoFocus={true} />
            { this.renderErrorMessage() }
          </div>
          <div className="user-form__buttons">
            <button>Save</button>
            <button className="user-form__cancel-btn"
              onClick={this.onCancelClick}>Cancel</button>
          </div>
        </form>
      </div>
    );
  },

  render: function(){
    if(this.state.mode === 'display'){
      return this.renderDisplayMode();
    }
    else if(this.state.mode === 'edit') {
      return this.renderEditMode();
    }
  }

});

module.exports = UserNameForm;
