"use strict";

require('./top_panel.less');
var React = require('react/addons');
var _ = require('lodash');
var cx = React.addons.classSet;
var NavLink = require('flux-router-component').NavLink;
var Icon = require('../general/Icon.jsx');
var SearchInput = require('../general/SearchInput.jsx');
var ResponsiveStore = require('../../stores/ResponsiveStore');
var UserStore = require('../../stores/UserStore');
var RouteStore = require('../../stores/RouteStore');
var StoreMixin = require('fluxible').StoreMixin;

var TopPanel = React.createClass({
  mixins: [StoreMixin],

  statics: {
    storeListeners: {
      'onRouteChange': RouteStore,
      'onSizeChange': ResponsiveStore,
      'onUserChange': UserStore
    }
  },

  getInitialState: function(){
    return _.extend({
      searchHasFocus: false, // focus status of search input
      browseExpanded: false
    }, this.getStateFromStores());
  },

  getStateFromStores: function(){
    return {
      size: this.getStore(ResponsiveStore).getState().size,
      user: this.getStore(UserStore).getState().user
    };
  },

  onSizeChange: function(){
    this.setState(this.getStateFromStores());
  },

  onUserChange: function(){
    this.setState(this.getStateFromStores());
  },

  onRouteChange: function(){
    if(this.isMounted()){
      this.setState({ browseExpanded: false });
    }
  },

  onSearchInputFocus: function(){
    this.setState({ searchHasFocus: true });
  },

  onSearchInputBlur: function(){
    this.setState({ searchHasFocus: false });
  },

  onBrowseClick: function(evt){
    evt.preventDefault();
    this.setState({browseExpanded: true});
  },

  renderHomeButton: function(){
    return (
      <NavLink href="/" context={this.props.context} key="home"
        className="top-panel__button top-panel__home-btn"
      ><Icon name="home" /></NavLink>
    );
  },

  renderBrowseButton: function(){
    if(this.props.browseURL){
      return (
        <NavLink href={this.props.browseURL} context={this.props.context}
          key="browse" className="top-panel__button top-panel__browse-btn">
          { this.props.browseText || "Browse" }
        </NavLink>
      );
    }

    if(this.state.browseExpanded){
      return (
        <div className="top-panel__browse-btn top-panel__browse-btn--expanded">
          { this.renderBrowseByCategoryButton() }
          { this.renderBrowseByStoreButton() }
        </div>
      );
    }

    return <a href="#" onClick={this.onBrowseClick} key="browse"
      className="top-panel__button top-panel__browse-btn">Browse</a>;
  },

  renderBrowseByCategoryButton: function(){
    var url = this.props.context.makePath('browse') + '?browseBy=categories';
    return (
      <NavLink href={url} context={this.props.context} key="browseByCategory"
      className="top-panel__button top-panel__browse-btn">
        By Category
      </NavLink>
    );
  },

  renderBrowseByStoreButton: function(){
    var url = this.props.context.makePath('browse') + '?browseBy=dealers';
    return (
      <NavLink href={url} context={this.props.context} key="browseByStore"
      className="top-panel__button top-panel__browse-btn">
        By Store
      </NavLink>
    );
  },

  renderUserButton: function(){
    if(this.state.size === ResponsiveStore.MOBILE &&
    this.state.browseExpanded){ return; }

    if(this.state.user){
      return (
        <NavLink routeName="account" context={this.props.context}
        className="top-panel__button top-panel__account-btn">
          <Icon name="user" />
        </NavLink>
      );
    }
    else {
      return (
        <NavLink routeName="signup" context={this.props.context}
        className="top-panel__button top-panel__login-btn">
          Login
        </NavLink>
      );
    }
  },

  renderSearch: function(){
    if(this.state.size === ResponsiveStore.MOBILE &&
    this.state.browseExpanded){ return; }

    return <SearchInput context={this.props.context}
      onFocus={this.onSearchInputFocus} onBlur={this.onSearchInputBlur}/>;
  },

  render: function(){
    var className = cx({
      "top-panel": true,
      "top-panel--search-active": this.state.searchHasFocus
    });

    return (
      <div className={className}>
        { this.renderHomeButton() }
        { this.renderBrowseButton() }
        { this.renderUserButton() }
        { this.renderSearch() }
      </div>
    );
  }
});

module.exports = TopPanel;
