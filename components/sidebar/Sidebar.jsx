"use strict";

require('./sidebar.less');

var React = require('react');
var FindMagazineForm = require('../home/FindMagazineForm.jsx');
var CategoriesList = require('./CategoriesList.jsx');
var ResponsiveStore = require('../../stores/ResponsiveStore');

var NavigationSidebar = React.createClass({

  render: function(){
    return (
      <div className="sidebar">
        <CategoriesList context={this.props.context}
          dealerId={this.props.dealerId}
          activeCategory={this.props.activeCategory} />
        <div className="sidebar__search">
          <h2>Search</h2>
          <p>Search local retailers for directions to your favorite title.</p>
          <FindMagazineForm context={this.props.context}
            size={ResponsiveStore.DESKTOP}/>
        </div>
      </div>
    );
  }

});

module.exports = NavigationSidebar;
