"use strict";

require('./categories_list.less');

var React = require('react/addons');
var cx = React.addons.classSet;
var _ = require('lodash');
var NavLink = require('flux-router-component').NavLink;
var CategoryStore = require('../../stores/CategoryStore');
var StoreMixin = require('fluxible').StoreMixin;
var searchCategories = require('../../actions/searchCategories');

var CategoriesList = React.createClass({
  mixins: [StoreMixin],

  statics: {
    storeListeners: [CategoryStore]
  },

  getStateFromStores: function(){
    return {
      categories: this.props.context.getStore(CategoryStore).getState()
                  .categories
    };
  },

  getInitialState: function(){
    return this.getStateFromStores();
  },

  onChange: function(){
    this.setState(this.getStateFromStores());
  },

  renderCategories: function(){
    return _.map(this.state.categories.valueOf(), function(category){
      var classes = cx({
        "categories-list__category": true,
        "categories-list__category--active": (category.name ===
                                              this.props.activeCategory)
      });

      var url = this.props.context.makePath('category', {id: category.name});
      if(this.props.dealerId){ url += '?dealerId=' + this.props.dealerId; }

      return (
        <NavLink key={category.name} context={this.props.context} href={url}
          className={classes}
        >
          { category.name.split('/').join(' / ') }
        </NavLink>
      );
    }, this);
  },

  render: function(){
    var classes = "categories-list";

    if(!this.state.categories){
      this.props.context.executeAction(searchCategories, {}, function(){});
    }

    if(!this.state.categories || !this.state.categories.isFulfilled() ||
       _.isEmpty(this.state.categories.valueOf()))
    {
      return <div className={classes} />;
    }

    return (
      <div className={classes}>
        <h2>Categories</h2>
        <ul>{ this.renderCategories() }</ul>
      </div>
    );
  }
});

module.exports = CategoriesList;
