"use strict";

require('./most_popular.less');
var React = require('react/addons');
var _ = require('lodash');
var cx = React.addons.classSet;
var Icon = require('../general/Icon.jsx');
var MostPopularStore = require('../../stores/MostPopularStore');
var ResponsiveStore = require('../../stores/ResponsiveStore');
var StoreMixin = require('fluxible').StoreMixin;
var LoadingMessageMixin = require('../../mixins/LoadingMessageMixin');
var toggleFooter = require('../../actions/toggleFooter');
var searchPopularMagazines = require('../../actions/searchPopularMagazines');
var Message = require('../general/Message.jsx');
var NavLink = require('flux-router-component').NavLink;

var MostPopular = React.createClass({
  mixins: [StoreMixin, LoadingMessageMixin],

  statics: {
    storeListeners: [MostPopularStore, ResponsiveStore]
  },

  getInitialState: function(){
    return this.getStateFromStores();
  },

  getStateFromStores: function(){
    return _.extend({},
      _.pick(this.getStore(MostPopularStore).getState(),
             ['hidden', 'magazines']),
      _.pick(this.getStore(ResponsiveStore).getState(), ['size'])
    );
  },

  onChange: function(){
    this.setState(this.getStateFromStores());
  },

  fetchMagazines: function(){
    if(!this.state.hidden && !this.state.magazines){
      this.props.context.executeAction(searchPopularMagazines, {popular: true});
    }
  },

  componentDidMount: function(){
    this.fetchMagazines();
  },

  componentDidUpdate: function(){
    this.fetchMagazines();
  },

  onToggleClick: function(evt){
    evt.preventDefault();
    this.props.context.executeAction(toggleFooter);
  },

  renderMagazines: function(){
    if(this.state.hidden){ return; }

    var loadingMessage = this.renderLoadingMessage(this.props.context,
                                                   'currentLocation');
    if(loadingMessage){ return loadingMessage; }

    if(!this.state.magazines || this.state.magazines.isPending()){
      return <Message type="loading">Loading...</Message>;
    }

    if(this.state.magazines.isRejected()){
      return <Message type="error">There was an error trying to fetch most
        popular magazines.</Message>;
    }

    var magazines = this.state.magazines.valueOf();

    if(_.isEmpty(magazines)){
      return <Message type="empty">No magazines were found near your
        current location.</Message>;
    }

    return _.chain(magazines)
      .sortBy(function(magazine){ return -1*magazine.popularityOverall; })
      .first(10)
      .map(function(magazine){
        var style = { backgroundImage: 'url(' + magazine.coverImageUrl + ')' };

        return (
          <NavLink className="most-popular__magazine" context={this.props.context}
          key={magazine.id} routeName="magazine" navParams={ {id: magazine.id} }
          style={style}></NavLink>
        );
      }, this)
      .value();
  },

  renderBottomBar: function(){
    if(this.state.size === ResponsiveStore.MOBILE){ return; }
    return (
      <div className="most-popular__bottom-bar">
        <div>
          <NavLink routeName="about" context={this.props.context}>
            About
          </NavLink>
          { " | " }
          <NavLink routeName="feedback" context={this.props.context}>
            Feedback
          </NavLink>
        </div>
      </div>
    );
  },

  render: function(){
    var classes = cx({
      "most-popular": true,
      "most-popular--hidden": this.state.hidden,
      "most-popular--expanded": !this.state.hidden
    });
    return (
      <div className={ classes }>
        <a href="#" className="most-popular__title"
        onClick={this.onToggleClick}>
          <h2>{ this.props.title || "Most Popular Magazines:"}</h2>
          <Icon name={this.state.hidden ? 'upAngle' : 'downAngle'} />
        </a>
        <div className="most-popular__magazines">
          { this.renderMagazines() }
        </div>
        { this.renderBottomBar() }
      </div>
    );
  }

});

module.exports = MostPopular;
