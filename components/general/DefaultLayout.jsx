"use strict";

require('./default-layout.less');

var React = require('react');
var TopPanel = require('../top_panel/TopPanel.jsx');
var MostPopular = require('../most_popular/MostPopular.jsx');

var DefaultLayout = React.createClass({
  render: function(){
    var className = "default-layout " + this.props.className;
    return (
      <div className={className}>
        <TopPanel context={this.props.context} />
        <div className="default-layout__content">
          { this.props.children }
        </div>
        <MostPopular context={this.props.context} />
      </div>
    );
  }
});

module.exports = DefaultLayout;
