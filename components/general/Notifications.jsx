'use strict';

var _ = require('lodash');
var React = require('react');
var StoreMixin = require('fluxible').StoreMixin;
var NotificationStore = require('../../stores/NotificationStore');
var resetNotifications = require('../../actions/resetNotifications');

var Notifications = React.createClass({
  mixins: [StoreMixin],

  statics: {
    storeListeners: [NotificationStore]
  },

  onChange: function(){
    _.each(this.getStore(NotificationStore).getState(), function(notification){
      this.noty(notification);
    }, this);
    this.props.context.executeAction(resetNotifications);
  },

  render: function(){
    return <div></div>;
  },

  componentDidMount: function(){
    this.noty = require('noty');
  }

});

module.exports = Notifications;
