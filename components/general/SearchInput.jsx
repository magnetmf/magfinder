"use strict";

var React = require('react/addons');
var cx = React.addons.classSet;
var _ = require('lodash');
var NavLink = require('flux-router-component').NavLink;
var Icon = require('../general/Icon.jsx');
var searchMagazines = require('../../actions/searchMagazines');
var navigateAction = require('flux-router-component').navigateAction;

var SearchInput = React.createClass({
  getInitialState: function(){
    return {
      selected: null, // index of currently selected search result
      isFocused: false, // focus status of search input
      searchStr: null,
      searchResults: null
    };
  },

  onSearchChange: function(){
    var searchStr = this.refs.search.getDOMNode().value;
    this.setState({ searchStr: searchStr });

    searchMagazines(this.props.context, {searchStr: searchStr, trigger: false},
    _.bind(function(err, results){
      if(!err && results && results.results){
        this.setState({ searchResults: results.results.valueOf() });
      }
      else {
        this.setState({ searchResults: [] });
      }
    }, this));
  },

  onFocus: function(){
    this.setState({ isFocused: true });
    if(this.props.onFocus){
      this.props.onFocus();
    }
  },

  onBlur: function(){
    // If we don't add a delay before removing search results, then click events
    // on NavLinks are not processed
    _.delay(function(component){
      if(component.isMounted()){
        component.setState({ isFocused: false, selected: null });
        if(component.props.onBlur){
          component.props.onBlur();
        }
      }
    }, 100, this);
  },

  onKeyDown: function(evt){
    if(evt.key === "ArrowDown"){
      evt.preventDefault();
      return this.selectNext();
    }
    if(evt.key === "ArrowUp"){
      evt.preventDefault();
      return this.selectPrevious();
    }
    if(evt.key === "Enter"){
      var navlink = this.refs['navlink' + this.state.selected];
      if(navlink){
        navlink.getDOMNode().click();
      }
      else {
        var query = this.refs.search.getDOMNode().value;
        if(_.isEmpty(query)){ return; }
        this.props.context.executeAction(navigateAction, {
          url: this.props.context.makePath('search') + '?query=' + query,
          params: { optimistic: true }
        });
      }
    }
  },

  selectNext: function(){
    var next = _.isNumber(this.state.selected) ? this.state.selected : -1;
    next += 1;
    if(next >= this.state.searchResults.length){
      return;
    }
    this.setState({ selected: next });
  },

  selectPrevious: function(){
    var next = _.isNumber(this.state.selected) ? this.state.selected : 0;
    next -= 1;
    if(next < 0){ this.setState({ selected: null }); }
    else { this.setState({ selected: next }); }
  },

  renderSearchInput: function(){
    return (
      <label key="searchInput">
        <Icon name="search" />
        <input type="text" ref="search" onFocus={this.onFocus}
          onBlur={this.onBlur} onKeyDown={this.onKeyDown}
          onChange={this.onSearchChange} />
      </label>
    );
  },

  renderSearchResults: function(){
    if(!this.state.isFocused){
      return null;
    }
    var items = this.renderSearchResultItems();
    if(!_.isEmpty(items)){
      return (
        <div key="searchResults" className="search-input__search-results">
          <div>{ items }</div>
        </div>
      );
    }
  },

  renderSearchResultItems: function(){
    return _.map(this.state.searchResults, function(magazine, i){
      var classes = (this.state.selected === i) ? 'active' : '';
      return (
        <NavLink ref={ 'navlink' + i } key={magazine.id} routeName="magazine"
          navParams={{id: magazine.id}} className={classes}
          context={this.props.context}
        >
          { magazine.name } <Icon name="rightAngle" />
        </NavLink>
      );
    }, this);
  },

  render: function(){
    var className = cx({
      "search-input": true,
      "search-input--active": this.state.isFocused
    });

    return (
      <div className={className}>
        { this.renderSearchInput() }
        { this.renderSearchResults() }
      </div>
    );
  }
});

module.exports = SearchInput;
