"use strict";

require('./share-magazine-button.less');

var React = require('react');
var config = require('../../configs/public');
var Icon = require('../general/Icon.jsx');

function getMagazineURL(context, magazine){
  var url = context.makePath('magazine', {id: magazine.id});

  if(typeof window !== 'undefined' && window.location){
    /* jshint browser:true */
    return window.location.origin + url;
  }

  return url;
}

function getCaption(magazine){
  return 'I just found ' + magazine.name + ' using #MagFinder.';
}

function handleClick(evt){
  /* jshint browser:true */
  evt.preventDefault();
  evt.stopPropagation();

  window.open(evt.currentTarget.getAttribute('href'), 'Share',
              "height=300,width=550");
}

exports.Facebook = React.createClass({
  render: function(){
    var magazineURL = encodeURIComponent(getMagazineURL(this.props.context,
                                                        this.props.magazine));
    var href = [
      'https://www.facebook.com/dialog/feed?',
      '&app_id=', config.fbAppID,
      '&display=popup',
      '&redirect_uri=', magazineURL,
      '&link=', magazineURL,
      '&name=', encodeURIComponent(this.props.magazine.name) + ' on MagFinder',
      '&picture=', encodeURIComponent(this.props.magazine.coverImageUrl),
      '&description=', encodeURIComponent(this.props.magazine.description)
    ].join('');

    return (
      <div className="share-magazine-button share-magazine-button--facebook"
        href={href} target="_blank" onClick={handleClick}
      >
        <Icon name="facebookSquare" /> Share
      </div>
    );
  },
});

exports.Twitter= React.createClass({
  render: function(){
    var magazineURL = encodeURIComponent(getMagazineURL(this.props.context,
                                                        this.props.magazine));
    var href= [
      'https://twitter.com/intent/tweet?',
      '&url=', magazineURL,
      '&text=', encodeURIComponent(getCaption(this.props.magazine)),
      '&tw_p=tweetbutton'
    ].join('');

    return (
      <div className="share-magazine-button share-magazine-button--twitter"
        href={href} target="_blank" onClick={handleClick}
      >
        <Icon name="twitter" /> Tweet
      </div>
    );
  }
});
