"use strict";

require('./zipcode_form.less');

var React = require('react');
var _ = require('lodash');
var LocationStore = require('../../stores/LocationStore');
var setZipCode = require('../../actions/setZipCode');

var ZipCodeForm = React.createClass({
  render: function(){
    var zipCode = this.props.context.getStore(LocationStore).getState().zipCode;
    return (
      <form className="zipcode-form" onSubmit={this.onSubmit}>
        <input type="text" placeholder="Insert your zip code"
          defaultValue={zipCode} ref="zipCodeInput" />
        <button>Submit</button>
      </form>
    );
  },

  onSubmit: function(evt){
    evt.preventDefault();
    var val = this.refs.zipCodeInput.getDOMNode().value;
    if(!_.isEmpty(val)){
      this.props.context.executeAction(setZipCode, {zipCode: val});
    }
  }
});

module.exports = ZipCodeForm;
