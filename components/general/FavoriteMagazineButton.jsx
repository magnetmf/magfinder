"use strict";

require('./favorite-magazine-button.less');

var _ = require('lodash');
var React = require('react');
var addFavoriteMagazine = require('../../actions/addFavoriteMagazine');
var removeFavoriteMagazine = require('../../actions/removeFavoriteMagazine');
var UserStore = require('../../stores/UserStore');
var StoreMixin = require('fluxible').StoreMixin;
var searchFavoriteMagazines = require('../../actions/searchFavoriteMagazines');
var Icon = require('./Icon.jsx');

var FavoriteMagazineButton = React.createClass({
  mixins: [StoreMixin],

  statics: {
    storeListeners: [UserStore]
  },

  getInitialState: function(){
    return this.getStateFromStores();
  },

  getStateFromStores: function(){
    var favorites = this.getStore(UserStore).getFavoriteMagazines();

    if(!favorites){
      this.props.context.executeAction(searchFavoriteMagazines);
    }

    var favs = (favorites && favorites.isFulfilled()) ? favorites.valueOf() : [];
    return {
      isFavorite: _.include(_.pluck(favs, 'id'),
                            parseInt(this.props.magazine.id, 10))
    };
  },

  onChange: function(){
    this.setState(this.getStateFromStores());
  },

  addFavorite: function(evt){
    evt.preventDefault();
    evt.stopPropagation();
    this.props.context.executeAction(addFavoriteMagazine,
      { id: parseInt(this.props.magazine.id, 10) });
  },

  removeFavorite: function(evt){
    evt.preventDefault();
    evt.stopPropagation();
    this.props.context.executeAction(removeFavoriteMagazine,
      { id: parseInt(this.props.magazine.id, 10) });
  },

  render: function(){
    var text, onClick, icon;
    if(this.state.isFavorite){
      text = 'Unfavorite';
      icon = 'removeFavorite';
      onClick = this.removeFavorite;
    }
    else {
      text = 'Favorite';
      icon = 'favorite';
      onClick = this.addFavorite;
    }
    return (
      <button className="favorite-magazine-button" onClick={onClick}>
        <Icon name={icon} /><span>{ text }</span>
      </button>
    );
  }
});

module.exports = FavoriteMagazineButton;
