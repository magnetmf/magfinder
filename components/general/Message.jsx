"use strict";

var React = require('react');

var Message = React.createClass({
  render: function(){
    var className = "message message--" + this.props.type;
    return <div className={className}>{ this.props.children }</div>;
  }
});

module.exports = Message;
