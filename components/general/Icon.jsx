"use strict";

var React = require('react');

var ICONS = {
  search: "icon-search-find",
  marker: "icon-pin",
  info: "icon-info-circled",
  upAngle: "icon-angle-up",
  rightAngle: "icon-angle-right",
  downAngle: "icon-angle-down",
  leftAngle: "icon-angle-left",
  home: "icon-home",
  facebook: "icon-facebook",
  twitter: "icon-twitter",
  facebookSquare: "icon-facebook-square",
  twitterSquare: "icon-twitter-square",
  user: "icon-user",
  email: "icon-email",
  favorite: "icon-star",
  removeFavorite: "icon-star-half"
};

module.exports = React.createClass({
  render: function(){
    var className = '' + this.props.className + ' ' + ICONS[this.props.name];
    return <i className={ className } />;
  }
});
