/* jshint browser: true */
'use strict';

// polyfill require.ensure
if(typeof require.ensure !== "function"){
  require.ensure = function(d, c) { return c(require); };
}

var React = require('react');
var RouteStore = require('../../stores/RouteStore');
var RouterMixin = require('flux-router-component').RouterMixin;
var StoreMixin = require('fluxible').StoreMixin;
var $ = require('jquery');
var _ = require('lodash');
var Notifications = require('./Notifications.jsx');
var resolveIntent = require('../../actions/resolveIntent');

var contentGenerators = {
  home: function(route, Home, context){
    return <Home context={context} />;
  },

  about: function(route, About, context){
    return <About context={context} />;
  },

  video: function(route, Video, context){
    return <Video context={context} />;
  },

  browse: function(route, Browse, context){
    return <Browse context={context} browseBy={route.query.browseBy}
            dealerId={parseInt(route.query.dealerId, 10)} />;
  },

  magazine: function(route, Magazine, context){
    return <Magazine context={context} id={parseInt(route.params.id, 10)}
            dealerId={parseInt(route.query.dealerId, 10)} />;
  },

  category: function(route, Category, context){
    return <Category context={context} id={decodeURIComponent(route.params.id)}
      dealerId={parseInt(route.query.dealerId, 10)} sort={route.query.sort} />;
  },

  dealers: function(route, Dealers, context){
    return <Dealers context={context} id={parseInt(route.params.id, 10)}
      dealerId={parseInt(route.query.dealerId, 10)}
      display={route.query.display} />;
  },

  directions: function(route, Directions, context){
    return (
      <Directions context={context}
                  magazineId={parseInt(route.query.magazineId, 10)}
                  dealerId={parseInt(route.params.dealerId, 10)}
                  displayMode={route.query.displayMode || 'map'}
                  sidebarDealerId={parseInt(route.query.sidebarDealerId, 10)} />
    );
  },

  search: function(route, SearchResults, context){
    return (
      <SearchResults context={context} query={route.query.query} />
    );
  },

  feedback: function(route, Feedback, context){
    return <Feedback context={context} type={route.query.type} />;
  },

  signup: function(route, SignupIndex, context){
    return <SignupIndex context={context} />;
  },

  signupWithEmail: function(route, SignupWithEmail, context){
    return <SignupWithEmail context={context} />;
  },

  account: function(route, Account, context){
    return <Account context={context} active={route.query.active} />;
  },

  verifyPhone: function(route, VerifyPhone, context){
    return <VerifyPhone context={context} />;
  }
};

var Application = React.createClass({
  mixins: [RouterMixin, StoreMixin],
  statics: {
    storeListeners: [RouteStore]
  },
  getInitialState: function () {
    return this.getStore(RouteStore).getState();
  },
  onChange: function () {
    this.setState(this.getInitialState());
  },
  render: function(){
    var contentFunc = contentGenerators[this.state.route.name];
    var content = contentFunc(this.state.route, this.state.component,
                              this.props.context);
    return (
      <div>
        <Notifications context={this.props.context} />
        { content }
      </div>
    );
  },
  componentDidMount: function(){
    /*
     * hide footer when an input is focused
     * (on mobile devices the keyboard appears when an input is focused,
     * reducing screen estate)
     */
    $('input').on('focusin', function() {
      $('.most-popular').hide();
    });
    $(document).on('focusout', function(){
      $('.most-popular').show();
    });

    /*
     * catch 401 (Unauthorized) AJAX errors and log out user
     */
    var AjaxInterceptor = require('ajax-interceptor');
    AjaxInterceptor.addResponseCallback(_.bind(function(xhr){
      if(xhr.status === 401){
        window.location = '/signup';
      }
    }, this));
    AjaxInterceptor.wire();

    /*
     * check if there is any pending intent to resolve
     */
    this.props.context.executeAction(resolveIntent, {}, function(){});
  }
});

module.exports = Application;
