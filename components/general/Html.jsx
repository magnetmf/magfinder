'use strict';

var React = require('react');
var config = require('../../configs/public');

var gmapsURL = "https://maps.googleapis.com/maps/api/js?key=" + config.googleApiKey;

module.exports = React.createClass({
  render: function() {
    return (
      <html>
        <head>
          <meta charSet="utf-8" />
          <meta name="viewport" content="initial-scale=1.0, user-scalable=0, width=device-width, height=device-height"/>
          <title>MagFinder</title>
          <link href='//fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css' />
          <link rel="stylesheet" type="text/css" href="/public/base_styles.css" />
          <link rel="icon" type="image/png" href="/public/favicon.png" />
        </head>
        <body>
          <div id="app" dangerouslySetInnerHTML={{__html: this.props.markup}}></div>
        </body>
        <script dangerouslySetInnerHTML={{__html: this.props.state}}></script>
        <script type="text/javascript" src={gmapsURL}></script>
        <script src="/public/client.js" defer></script>
      </html>
    );
  }
});

