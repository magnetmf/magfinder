"use strict";

require('./modal-layout.less');

var React = require('react');
var DefaultLayout = require('./DefaultLayout.jsx');

var ModalLayout = React.createClass({

  render: function(){
    var className = "modal-layout " + this.props.className;

    return (
      <DefaultLayout className={className} context={this.props.context}>
        <div className="modal-layout__modal">
          <div className="modal-layout__modal__content">
            { this.props.children }
          </div>
        </div>
      </DefaultLayout>
    );
  }

});

module.exports = ModalLayout;
