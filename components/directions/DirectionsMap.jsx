"use strict";

var React = require('react');
var GMaps = require("react-google-maps");

var NearestMap = React.createClass({
  displayName: "NearestMap",

  mixins: [GMaps.GoogleMapsMixin],

  render: function() {
    return (
      <div style={{height: "100%"}}>
        <GMaps.Map style={{height: "100%"}} zoom={8} center={this.props.center} />
        { this.renderDirections() }
      </div>
    );
  },

  renderDirections: function(){
    return <GMaps.DirectionsRenderer directions={this.props.directions} />;
  }

});

module.exports = NearestMap;
