"use strict";

require('./directions.less');

var React = require('react');
var _ = require('lodash');
var StoreMixin = require('fluxible').StoreMixin;
var LocationStore = require('../../stores/LocationStore');
var SearchStore = require('../../stores/SearchStore');
var DirectionsStore = require('../../stores/DirectionsStore');
var ResponsiveStore = require('../../stores/ResponsiveStore');
var DealerStore = require('../../stores/DealerStore');
var getDirections = require('../../actions/getDirections');
var TopPanel = require('../top_panel/TopPanel.jsx');
var DealerPreview = require('../dealers/DealerPreview');
var DirectionsList = require('./DirectionsList');
var DirectionsMap = require('./DirectionsMap');
var Message = require('../general/Message.jsx');
var LoadingMessageMixin = require('../../mixins/LoadingMessageMixin');
var MagazinePreview = require('../magazine/MagazinePreview');
var Sidebar = require('../sidebar/Sidebar.jsx');

var Directions = React.createClass({
  mixins: [StoreMixin, LoadingMessageMixin],

  statics: {
    storeListeners: [LocationStore, SearchStore, DirectionsStore,
                     DealerStore, ResponsiveStore]
  },

  getStateFromStores: function(){
    return {
      size: this.getStore(ResponsiveStore).getState().size,
      directions: this.getStore(DirectionsStore).getState().directions,
      currentLocation: this.getStore(LocationStore).currentLocation
    };
  },

  getInitialState: function(){
    return _.extend({ googleMapsApi: null }, this.getStateFromStores());
  },

  onChange: function(){
    this.setState(this.getStateFromStores());
  },

  componentDidMount: function(){
    /* jshint browser: true */
    this.setState({googleMapsApi: window.google.maps});

    if(!this.state.directions || this.directionsAreStale()){
      this.props.context.executeAction(getDirections,
                                       { dealerId: this.props.dealerId });
    }
  },

  getCurrentLocationCoords: function(){
    var coords = this.state.currentLocation.valueOf().coords;
    return { lat: coords.latitude, lng: coords.longitude };
  },

  getMagazineFromProps: function(){
    if(this.props.magazineId){
      return this.getStore(SearchStore).getMagazine(this.props.magazineId);
    }
  },

  getDealerFromProps: function(){
    if(this.props.dealerId){
      return this.getStore(DealerStore).getDealer(this.props.dealerId);
    }
  },

  getDealerCoordsFromProps: function(){
    var dealer = this.getDealerFromProps();
    if(!dealer){ return; }
    return {
      latitude: dealer.latitude,
      longitude: dealer.longitude
    };
  },

  getDealerCoordsFromDirections: function(){
    if(!this.state.directions || !this.state.directions.isFulfilled()){
      return;
    }
    var dealer = this.state.directions.valueOf().destination;
    return {
       latitude: dealer.lat(),
       longitude: dealer.lng()
    };
  },

  directionsAreStale: function(){
    // if the coords provided by state.directions are not the same as the coords
    // in props.dealerId, it means we have a stale direction
    return !_.isEqual(this.getDealerCoordsFromDirections(),
                      this.getDealerCoordsFromProps());
  },

  renderFooter: function(){
    if(this.state.size !== ResponsiveStore.MOBILE){ return; }
    return this.renderDealerPreview();
  },

  renderDealerPreview: function(){
    var dealer = this.getDealerFromProps();
    if(!dealer){ return; }

    return (
      <div className="directions__dealer-preview">
        <DealerPreview dealer={dealer} magazine={this.getMagazineFromProps()}
          linkToDirectionsAs={this.props.displayMode === 'list' ? 'map':'list'}
          context={this.props.context} />
      </div>
    );
  },

  renderMagazinePreview: function(){
    var magazine = this.getMagazineFromProps();
    if(!magazine || this.state.size === ResponsiveStore.MOBILE){ return; }

    return <MagazinePreview magazine={magazine} clickable={false}
      context={this.props.context} arrangement="descriptionFirst" />;
  },

  renderDirectionsAsList: function(){
    if(this.state.size === ResponsiveStore.MOBILE &&
    this.props.displayMode === 'map'){ return; }

    return (
      <div className="directions__list">
        <DirectionsList directions={this.state.directions.valueOf()}
          context={this.props.context} />
      </div>
    );
  },

  renderDirectionsAsMap: function(){
    if(this.state.size === ResponsiveStore.MOBILE &&
    this.props.displayMode === 'list'){ return; }

    return (
      <div className="directions__map">
        <DirectionsMap googleMapsApi={this.state.googleMapsApi}
            center={this.getCurrentLocationCoords()}
            directions={this.state.directions.valueOf()} />
      </div>
    );
  },

  renderSidebar: function(){
    if(this.state.size !== ResponsiveStore.DESKTOP){ return; }
    var activeCategory;
    if(this.state.magazine){
      activeCategory = this.state.magazine.category;
    }

    return (
      <div className="directions__sidebar">
        <Sidebar context={this.props.context} activeCategory={activeCategory}
          dealerId={this.props.sidebarDealerId} />;
      </div>
    );
  },

  renderContent: function(){
    var message = this.renderLoadingMessage(this.props.context, 'directions');
    if(message){ return message; }

    return (
      <div className="directions__content"><div>
        { this.renderDirectionsAsList() }
        { this.renderDirectionsAsMap() }
      </div></div>
    );
  },

  render: function(){
    return (
      <div className="directions">
        <TopPanel context={this.props.context} />
        { this.renderSidebar() }
        <div className="directions__main"><div>
          <div className="directions__header">
            { this.renderMagazinePreview() }
          </div>
          { this.renderContent() }
          { this.renderFooter() }
        </div></div>
      </div>
    );
  }

});

module.exports = Directions;
