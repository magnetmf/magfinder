"use strict";

var React = require('react');

var DirectionsAsList = React.createClass({
  getInitialState: function(){
    return {};
  },

  render: function(){
    return <div></div>;
  },

  componentDidMount: function(){
    /* globals google:false */
    var directionsDisplay = new google.maps.DirectionsRenderer();
    directionsDisplay.setDirections(this.props.directions);
    directionsDisplay.setPanel(this.getDOMNode());
  }

});

module.exports = DirectionsAsList;
