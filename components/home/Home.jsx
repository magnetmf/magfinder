"use strict";

var React = require('react');
var _ = require('lodash');
var NavLink = require('flux-router-component').NavLink;
var TopPanel = require('../top_panel/TopPanel.jsx');
var MostPopular = require('../most_popular/MostPopular.jsx');
var Icon = require('../general/Icon.jsx');
var FindMagazineForm = require('./FindMagazineForm.jsx');
var StoreMixin = require('fluxible').StoreMixin;
var ResponsiveStore = require('../../stores/ResponsiveStore');
var MostPopularStore = require('../../stores/MostPopularStore');
var searchPopularMagazines = require('../../actions/searchPopularMagazines');
var expireCurrentLocation = require('../../actions/expireCurrentLocation');
var Categories = require('../browse/BrowseCategories.jsx');

module.exports = React.createClass({
  mixins: [StoreMixin],

  statics: {
    storeListeners: {
      'onSizeChange': ResponsiveStore,
      'onPopularMagazinesChange': MostPopularStore
    }
  },

  getStateFromStores: function(){
    return {
      size: this.getStore(ResponsiveStore).getState().size,
      popularMagazines: this.getStore(MostPopularStore).getState().magazines
    };
  },

  getInitialState: function(){
    return this.getStateFromStores();
  },

  fetchMagazines: function(){
    if(this.state.size !== ResponsiveStore.MOBILE &&
      !this.state.popularMagazines)
    {
      expireCurrentLocation(
        this.props.context,
        {},
        _.bind(function(){
          this.props.context.executeAction(searchPopularMagazines, {});
        }, this)
      );
    }
  },

  onSizeChange: function(){
    this.setState(this.getStateFromStores());
  },

  onPopularMagazinesChange: function(){
    this.setState(this.getStateFromStores());
  },

  renderInfoBar: function(){
    return (
      <div className="home__info-bar">
        <NavLink routeName="home" context={this.props.context}>
          <Icon name="marker" /> MagFinder
        </NavLink>
        <div>
          <NavLink routeName="about" context={this.props.context}>
            <Icon name="info" /> About
          </NavLink>
          { " | " }
          <NavLink routeName="feedback" context={this.props.context}>
            Feedback
          </NavLink>
        </div>
      </div>
    );
  },

  renderVideoLink: function(){
    return (
      <NavLink className="home__video-link" routeName="video" context={this.props.context}>
        Watch our about MagFinder video
      </NavLink>
    );
  },

  renderHero: function(){
    return (
      <div className="home__hero">
        <div className="home__hero__info">
          <div>
            <h2>Search <em>Local</em> Retailers</h2>
            <p>Get directions to <br />find your favorite <em>magazine</em>.</p>
          </div>
        </div>
        { this.renderHeroMagazines() }
      </div>
    );
  },

  renderHeroMagazines: function(){
    if(this.state.size === ResponsiveStore.MOBILE ||
       !this.state.popularMagazines ||
       !this.state.popularMagazines.isFulfilled())
    {
      this.fetchMagazines();
      return;
    }

    var magazines = _.chain(_.shuffle(this.state.popularMagazines.valueOf()))
      .first(4)
      .map(function(magazine){
        var style = { backgroundImage: 'url(' + magazine.coverImageUrl + ')' };
        return <div key={magazine.id} style={style}></div>;
      })
      .value();

    return(
      <div className="home__hero__magazines">{ magazines }</div>
    );
  },

  renderCategories: function(){
    if(this.state.size === ResponsiveStore.MOBILE){ return; }
    return <Categories context={this.props.context} />;
  },

  render: function(){
    return (
      <div className="home">
        <TopPanel context={this.props.context} />
        <div className="home__content">
          { this.renderInfoBar() }
          { this.renderHero() }
          <FindMagazineForm context={this.props.context}
            size={this.state.size}/>
          { this.renderVideoLink() }
        </div>
        { this.renderCategories() }
        <MostPopular context={this.props.context} />
      </div>
    );
  }

});
