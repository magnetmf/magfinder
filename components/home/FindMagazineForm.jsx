"use strict";

var React = require('react/addons');
var _ = require('lodash');
var searchMagazines = require('../../actions/searchMagazines');
var navigateAction = require('flux-router-component').navigateAction;
var setZipCode = require('../../actions/setZipCode');
var getCurrentLocation = require('../../actions/getCurrentLocation');
var StoreMixin = require('fluxible').StoreMixin;
var LocationStore = require('../../stores/LocationStore');
var ResponsiveStore = require('../../stores/ResponsiveStore');
var cx = React.addons.classSet;
var Q = require('q');

module.exports = React.createClass({
  mixins: [StoreMixin],

  statics: {
    storeListeners: {
      'onLocationChange': LocationStore
    }
  },

  getInitialState: function(){
    var geolocation = this.getStore(LocationStore).getState();
    return {
      title: '',
      geolocationSupport: geolocation.geolocationSupport,
      findNearMe: !geolocation.zipCode,
      zipCode: geolocation.zipCode || ''
    };
  },

  onLocationChange: function(){
    var geolocation = this.getStore(LocationStore).getState();
    this.setState({
      errorMessage: null,
      geolocationSupport: geolocation.geolocationSupport,
      findNearMe: !geolocation.zipCode,
      zipCode: geolocation.zipCode || ''
    });
  },

  getCurrentLocation: function(){
    if(!this.state.currentLocation){
      this.props.context.executeAction(getCurrentLocation);
    }
  },

  geolocationIsNotAvailable: function(){
    return this.state.geolocationSupport === false || (
      this.state.currentLocation && this.state.currentLocation.isRejected() &&
      !this.state.zipCode
    );
  },

  onTitleChange: function(evt){
    var newVal = evt.target.value;
    this.setState({ title: newVal });
  },

  onZipCodeChange: function(evt){
    var newVal = evt.target.value;
    this.setState({ findNearMe: false, zipCode: newVal });
  },

  onCheckboxChange: function(evt){
    var newVal = !this.state.findNearMe;
    var newState = { findNearMe: newVal };
    if(newVal === true){
      newState.zipCode = null;
    }

    this.setState(newState);
  },

  onSubmit: function(evt){
    evt.preventDefault();

    if(_.isEmpty(this.state.title)){
      return this.setState({
        errorMessage: 'Magazine title can\'t be empty.'
      });
    }

    if(!_.isEmpty(this.state.zipCode)){
      this.props.context.executeAction(setZipCode,
                                       {zipCode: this.state.zipCode });
      return this.navigateToSearchPage();
    }

    if(this.state.findNearMe){
      this.props.context.executeAction(setZipCode, {zipCode: null });
      return this.navigateToSearchPage();
    }
    else {
      return this.setState({
        errorMessage: 'Please select "Find near me" or enter a zip code.'
      });
    }
  },

  navigateToSearchPage: function(){
    var url = this.props.context.makePath('search') + '?query=' +
      this.state.title;
    this.props.context.executeAction(navigateAction, {
      url: url,
      params: { optimistic: true }
    });
  },

  componentDidMount: function(){
    this.getCurrentLocation();
  },

  renderFindNearMe: function(){
    return (
      <div className="home__magform__find-near-me">
        <input type="checkbox" name="findNearMe"
          checked={this.state.findNearMe}
          onChange={this.onCheckboxChange}
          id="home__magform__find-near-me__input" />
        <label htmlFor="home__magform__find-near-me__input">Find Near Me</label>
      </div>
    );
  },

  renderErrorMessage: function(){
    if(this.state.errorMessage){
      return (
        <div className="home__magform__error">
          { this.state.errorMessage }
        </div>
      );
    }
  },

  render: function(){
    var titlePlaceholder = "Magazine Title";
    var zipCodePlaceholder = "Or Enter Zip Code";
    if(this.props.size !== ResponsiveStore.MOBILE){
      titlePlaceholder = "Enter Your Magazine Title";
      zipCodePlaceholder = "Use Zip Code";
    }

    if(this.state.size === ResponsiveStore.MOBILE){ return; }
    return (
      <form className="home__magform" onSubmit={this.onSubmit}>
        <input type="text" name="title" ref="title"
          placeholder={titlePlaceholder} value={this.state.title}
          onChange={this.onTitleChange} />
        {this.renderFindNearMe() }
        <input type="text" name="zipCode" ref="zipCode"
          value={this.state.zipCode} placeholder={zipCodePlaceholder}
          onChange={this.onZipCodeChange} />
        { this.renderErrorMessage() }
        <button>Find Your Magazine</button>
      </form>
    );
  }

});
