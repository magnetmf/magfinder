"use strict";

require('./browse.less');

var React = require('react');
var TopPanel = require('../top_panel/TopPanel.jsx');
var MostPopular = require('../most_popular/MostPopular.jsx');
var BrowseCategories = require('./BrowseCategories.jsx');
var BrowseDealers = require('./BrowseDealers.jsx');
var ResponsiveStore = require('../../stores/ResponsiveStore');
var DealerStore = require('../../stores/DealerStore');
var StoreMixin = require('fluxible').StoreMixin;
var Sidebar = require('../sidebar/Sidebar.jsx');
var DealerPreview = require('../dealers/DealerPreview');
var getDealer = require('../../actions/getDealer');

var Browse = React.createClass({
  mixins: [StoreMixin],

  getDefaultProps: function(){
    return { browseBy: 'categories' };
  },

  statics: { storeListeners: [ResponsiveStore, DealerStore] },

  getDealer: function(dealerId){
    if(!dealerId){ return; }

    var dealer = this.getStore(DealerStore).getDealer(dealerId);
    if(!dealer){
      this.props.context.executeAction(getDealer, this.props.dealerId);
    }

    return dealer;
  },

  getStateFromStores: function(){
    return {
      size: this.getStore(ResponsiveStore).getState().size,
      dealer: this.getDealer(this.props.dealerId)
    };
  },

  getInitialState: function(){
    return this.getStateFromStores();
  },

  componentWillReceiveProps: function(nextProps){
    if(nextProps.dealerId !== this.props.dealerId){
      this.setState({ dealer: this.getDealer(nextProps.dealerId) });
    }
  },

  onChange: function(){
    this.setState(this.getStateFromStores());
  },

  renderSidebar: function(){
    if(this.state.size !== ResponsiveStore.DESKTOP){ return; }
    return <Sidebar context={this.props.context}
            dealerId={this.props.dealerId} />;
  },

  renderContent: function(){
    switch(this.props.browseBy){
      case 'categories':
        return <BrowseCategories context={this.props.context} />;
      case 'dealers':
        return <BrowseDealers context={this.props.context} />;
      case 'dealerCategories':
        var dealerId = this.props.dealerId;
        if(!dealerId){ throw 'dealerId is required'; }
        return <BrowseCategories context={this.props.context}
                dealerId={dealerId} />;
    }

    throw 'Unkown browseBy: ' + this.props.browseBy;
  },

  renderFooter: function(){
    if(this.state.dealer){
      return (
        <div className="browse__footer">
          <DealerPreview key={this.state.dealer.id} context={this.props.context}
            dealer={this.state.dealer} />
        </div>
      );
    }

    return <MostPopular context={this.props.context}/>;
  },

  render: function(){
    var homeURL = this.props.context.makePath('home');

    return (
      <div className="browse">
        <TopPanel context={this.props.context} />
        <div className="browse__content">
          { this.renderSidebar() }
          { this.renderContent() }
        </div>
        { this.renderFooter() }
      </div>
    );
  }
});

module.exports = Browse;
