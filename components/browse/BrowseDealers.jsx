"use strict";

require('./browse_dealers.less');

var React = require('react');
var _ = require('lodash');
var LoadingMessageMixin = require('../../mixins/LoadingMessageMixin');
var Icon = require('../general/Icon.jsx');
var LocationStore = require('../../stores/LocationStore');
var DealerStore = require('../../stores/DealerStore');
var NavLink = require('flux-router-component').NavLink;
var Message = require('../general/Message.jsx');
var searchDealers = require('../../actions/searchDealers');
var StoreMixin = require('fluxible').StoreMixin;
var DealerPreview = require('../dealers/DealerPreview');
var navigateAction = require('flux-router-component').navigateAction;

var BrowseDealers = React.createClass({
  mixins: [StoreMixin, LoadingMessageMixin],

  statics: {
    storeListeners: [DealerStore, LocationStore]
  },

  getStateFromStores: function(){
    return _.pick(this.props.context.getStore(DealerStore).getState(),
                  ['dealers']);
  },

  getInitialState: function(){
    return this.getStateFromStores();
  },

  getDealerURL: function(dealer){
    return this.props.context.makePath('browse') +
      '?browseBy=dealerCategories&dealerId=' + dealer.id;
  },

  onChange: function(){
    this.setState(this.getStateFromStores());
  },

  onDealerListItemClick: function(dealer){
    this.props.context.executeAction(navigateAction, {
      url: this.getDealerURL(dealer)
    });
  },

  componentDidMount: function(){
    this.searchDealers();
  },

  componentDidUpdate: function(){
    this.searchDealers();
  },

  searchDealers: function(){
    if(!this.state.dealers){
      this.props.context.executeAction(searchDealers, {});
    }
  },

  renderDealers: function(){
    var loadingMessage = this.renderLoadingMessage(this.props.context,
                                                   'currentLocation');

    if(loadingMessage){ return loadingMessage; }

    if(!this.state.dealers || this.state.dealers.isPending()){
      return <Message type="loading">Loading Stores...</Message>;
    }

    if(this.state.dealers.isRejected()){
      return (
        <Message type="error">
          There was an error trying to load stores.
        </Message>
      );
    }

    if(_.isEmpty(this.state.dealers.valueOf())){
      return (
        <Message type="empty">
          We're sorry, no stores were found near your current location.
        </Message>
      );
    }

    return _.map(this.state.dealers.valueOf(), function(dealer){
      return <DealerPreview key={dealer.id} context={this.props.context}
        onClick={_.partial(this.onDealerListItemClick, dealer)}
        dealer={dealer} magazine={this.state.magazine}
        linkText="ALL PUBLICATIONS" linkURL={this.getDealerURL(dealer)} />;
    }, this);
  },

  render: function(){
    return (
      <div className="browse-dealers">
        { this.renderDealers() }
      </div>
    );
  }
});

module.exports = BrowseDealers;
