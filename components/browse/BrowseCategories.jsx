"use strict";

require('./browse_categories.less');

var React = require('react');
var _ = require('lodash');
var LoadingMessageMixin = require('../../mixins/LoadingMessageMixin');
var Icon = require('../general/Icon.jsx');
var LocationStore = require('../../stores/LocationStore');
var CategoryStore = require('../../stores/CategoryStore');
var NavLink = require('flux-router-component').NavLink;
var Message = require('../general/Message.jsx');
var searchCategories = require('../../actions/searchCategories');
var StoreMixin = require('fluxible').StoreMixin;

var BrowseCategories = React.createClass({
  mixins: [StoreMixin, LoadingMessageMixin],

  statics: {
    storeListeners: [CategoryStore, LocationStore]
  },

  getStateFromStores: function(){
    return _.pick(this.props.context.getStore(CategoryStore).getState(),
                  ['categories']);
  },

  getInitialState: function(){
    return this.getStateFromStores();
  },

  onChange: function(){
    this.setState(this.getStateFromStores());
  },

  componentDidMount: function(){
    this.searchCategories();
  },

  componentDidUpdate: function(){
    this.searchCategories();
  },

  searchCategories: function(){
    if(!this.state.categories ||
       this.state.categories.dealerId !== this.props.dealerId)
    {
      this.props.context.executeAction(searchCategories,
                                       {dealerId: this.props.dealerId});
    }
  },

  renderCategories: function(){
    var loadingMessage = this.renderLoadingMessage(this.props.context,
                                                   'currentLocation');

    if(loadingMessage){ return loadingMessage; }

    if(!this.state.categories || this.state.categories.isPending()){
      return <Message type="loading">Loading Categories...</Message>;
    }

    if(this.state.categories.isRejected()){
      return (
        <Message type="error">
          There was an error trying to load categories.
        </Message>
      );
    }

    if(_.isEmpty(this.state.categories.valueOf())){
      return (
        <Message type="empty">
          We're sorry, no publications were found near your current location.
        </Message>
      );
    }

    return _.map(this.state.categories.valueOf(), function(category){
      var text = category.magazineCount + ' publication';
      if(category.magazineCount > 1){
        text += 's';
      }

      var covers = _.map(_.first(category.magazines, 3), function(magazine){
        var style = { backgroundImage: 'url(' + magazine.coverImageUrl + ')' };
        return <div key={magazine.id} style={style}></div>;
      });

      var url = this.props.context.makePath('category', {id: category.name});
      if(this.props.dealerId){
        url += '?dealerId=' + this.props.dealerId;
      }

      return (
        <NavLink key={category.name} className="browse-categories__category"
          href={url} context={this.props.context}
        >
          <div className="browse-categories__category__covers">{covers}</div>
          <div className="browse-categories__category__info">
            <h2>{ category.name.split('/').join(' / ') }</h2>
            <p>{ text }</p>
          </div>
           <Icon name="rightAngle" />
        </NavLink>
      );
    }, this);
  },

  render: function(){
    return (
      <div className="browse-categories">
        { this.renderCategories() }
      </div>
    );
  }
});

module.exports = BrowseCategories;
