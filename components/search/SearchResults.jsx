"use strict";

require('./search_results.less');

var React = require('react/addons');
var cx = React.addons.classSet;
var _ = require('lodash');
var StoreMixin = require('fluxible').StoreMixin;
var SearchStore = require('../../stores/SearchStore');
var LocationStore = require('../../stores/LocationStore');
var ResponsiveStore = require('../../stores/ResponsiveStore');
var TopPanel = require('../top_panel/TopPanel.jsx');
var MostPopular = require('../most_popular/MostPopular.jsx');
var MagazinePreview = require('../magazine/MagazinePreview');
var searchMagazines = require('../../actions/searchMagazines');
var Message = require('../general/Message.jsx');
var LoadingMessageMixin = require('../../mixins/LoadingMessageMixin');
var EmptySearchResults = require('./EmptySearchResults.jsx');
var Sidebar = require('../sidebar/Sidebar.jsx');

var SearchResults = React.createClass({
  mixins: [StoreMixin, LoadingMessageMixin],

  statics: {
    storeListeners: {
      'onSearchChange': SearchStore,
      'onSizeChange': ResponsiveStore
    }
  },

  getStateFromStores: function(){
    return _.extend(
      _.pick(this.getStore(SearchStore).getState(), ['searchStr', 'results']),
      _.pick(this.getStore(ResponsiveStore).getState(), ['size'])
    );
  },

  getInitialState: function(){
    return this.getStateFromStores();
  },

  onSearchChange: function(){
    this.setState(this.getStateFromStores());
  },

  onSizeChange: function(){
    this.setState(this.getStateFromStores());
  },

  baseURL: function(){
    return this.props.context.makePath('search') + '?query=' + this.props.query;
  },

  fetchSearchResults: function(){
    if(!this.state.results || this.props.query !== this.state.searchStr){
      this.props.context.executeAction(searchMagazines,
                                       { searchStr: this.props.query });
    }
  },

  componentDidMount: function(){
    this.fetchSearchResults();
  },

  componentDidUpdate: function(){
    this.fetchSearchResults();
  },

  componentWillReceiveProps: function(nextProps){
    if(nextProps.query !== this.state.searchStr){
      this.props.context.executeAction(searchMagazines,
                                       { searchStr: nextProps.query });
    }
  },

  hasSearchResults: function(){
    return this.state.results && this.state.results.isFulfilled() &&
      this.state.results.valueOf().length > 0;
  },

  isEmpty: function(){
    return this.state.results && this.state.results.isFulfilled() &&
      this.state.results.valueOf().length === 0;
  },

  renderResultCount: function(){
    return (
      <div className="search-results__count">
        <div>{ this.props.query }</div>
        <div>{ this.state.results.valueOf().length + " Results" }</div>
      </div>
    );
  },

  renderMagazines: function(){
    return _.chain(this.state.results.valueOf())
      .sortBy(function(magazine){
        var value = -1 * magazine.popularityOverall;
        if(magazine.coverImageUrl.match(/DefaultCover/)){
          // we want magazines with default cover images to be pushed to the bottom
          value += 99;
        }
        return value;
      })
      .map(function(magazine){
        return <MagazinePreview key={magazine.id} magazine={magazine}
                arrangement="minimal" context={this.props.context} />;
      }, this)
      .value();
  },

  renderSearchResults: function(){
    if(this.hasSearchResults()){
      return (
        <div className="search-results__magazines">
          { this.renderResultCount() }
          { this.renderMagazines() }
        </div>
      );
    }
    else if(this.isEmpty()){
      return <EmptySearchResults {...this.props} />;
    }
    else {
      return this.renderLoadingMessage(this.props.context, 'searchResults');
    }
  },

  renderSidebar: function(){
    if(this.state.size !== ResponsiveStore.DESKTOP){ return; }
    return <Sidebar context={this.props.context} />;
  },

  render: function(){
    var className = cx({
      "search-results": true,
      "search-results--empty": this.isEmpty()
    });

    return (
      <div className={className}>
        <TopPanel context={this.props.context} />
        <div className="search-results__content">
          { this.renderSidebar() }
          { this.renderSearchResults() }
        </div>
        <MostPopular context={this.props.context}/>
      </div>
    );
  }

});

module.exports = SearchResults;
