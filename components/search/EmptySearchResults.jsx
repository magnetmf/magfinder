"use strict";

var React = require('react');
var NavLink = require('flux-router-component').NavLink;
var LocationStore = require('../../stores/LocationStore');
var Icon = require('../general/Icon.jsx');
var SearchInput = require('../general/SearchInput.jsx');
var FeedbackForm = require('../feedback/FeedbackForm.jsx');

var EmptySearchResults = React.createClass({

  getLocation: function(){
    var state = this.props.context.getStore(LocationStore).getState();
    if(state.zipCode){
      return "zip code " + state.zipCode;
    }
    else {
      var coords = state.currentLocation.valueOf().coords;
      return "latitude " + coords.latitude.toFixed(1) + ", longitude " +
        coords.longitude.toFixed(1);
    }
  },

  render: function(){
    return (
      <div className="empty-search-results">
        <div>
          <h2>Search Results</h2>
          <p>
            We're sorry, no publications were found for your
            search: "{this.props.query}" near { this.getLocation() }.
          </p>
        </div>
        <div>
          <h2>Try a New Search</h2>
          <SearchInput context={this.props.context} />
        </div>
        <div>
          <h2>Search Tips</h2>
          <ul>
            <li>Try using fewer and more general words e.g. try "health"</li>
            <li>
              Try different keywords or categories
              or <NavLink context={this.props.context} routeName="browse">
                    browse publications</NavLink>.
            </li>
            <li>Don't start your search with a * or a ?</li>
          </ul>
        </div>
        <div>
          <h2>Request this magazine to be carried near you</h2>
          <FeedbackForm context={this.props.context} magazine={this.props.query}
            type={'magazine_request'} />
        </div>
      </div>
    );
  }
});

module.exports = EmptySearchResults;
