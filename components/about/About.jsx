"use strict";

require('./about.less');

var React = require('react');
var TopPanel = require('../top_panel/TopPanel.jsx');
var MostPopular = require('../most_popular/MostPopular.jsx');

var About = React.createClass({
  render: function(){
    return (
      <div className="about">
        <TopPanel context={this.props.context} />
        <div className="about__content">
          <h2>About Us</h2>
          <p>
            MagFinder is the premier application for finding your favorite magazine at retail outlets around North America. Search by name or category, browse recent covers, get directions, and more.
          </p>
          <p>
            MagFinder is the only GPS-Enabled magazine locator available, and it covers nearly every publication sold in the United States and Canada.
          </p>
          <p>
            Updated daily with new inventory data, MagFinder not only knows what's on the shelf at your favorite retailer, but it will take you there as well. And with the popularity index you'll always know what's hot on newsstands today!
          </p>
          <p>
            MagFinder, powered by MagNet.
          </p>
          <div className="about__disclaimer">
            <h3>Disclaimer</h3>
            <p>MagNet disclaims any representation or warranty that the magazine title entered into the app by the user will in fact be present in inventory and/or will be available for sale at the store/location identified by the application at the time the user visits the identified store/location.</p>
            <p>
              Content on MagFinder is not affiliated with any magazine publisher. For questions please contact magfinder@magnetdata.net
            </p>
          </div>
        </div>
        <MostPopular context={this.props.context} />
      </div>
    );
  }
});

module.exports = About;
