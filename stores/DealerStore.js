'use strict';
var createStore = require('fluxible/utils/createStore');
var _ = require('lodash');

var DealerStore = createStore({
  storeName: 'DealerStore',

  handlers: {
    'SEARCH_DEALERS_START': 'updateSearch',
    'GET_DEALER_SUCCESS': 'addDealer',
    'GET_CURRENT_LOCATION_START': 'resetSearch',
    'NEW_ZIP_CODE': 'resetSearch',
    'EXPIRE_CURRENT_LOCATION': 'resetSearch'
  },

  initialize: function (dispatcher){
    this.dealers = null;                // dealers used for browsing by dealers
    this.individualDealers = {};        // dealers fetched individually
  },

  addDealer: function(dealer){
    this.individualDealers[dealer.id] = dealer;
    this.emitChange();
  },

  updateSearch: function(search){
    this.dealers = search;
    this.dealers.fin(_.bind(function(){ this.emitChange(); }, this));
    this.emitChange();
  },

  resetSearch: function(){
    if(this.dealers){
      this.dealers = null;
      this.emitChange();
    }
  },

  getState: function(){
    return { dealers: this.dealers };
  },

  getDealer: function(id){
    var dealer = this.individualDealers[id];
    if(dealer){ return dealer; }

    if(!this.dealers || !this.dealers.isFulfilled()){ return; }
    return _.find(this.dealers.valueOf(), { id: id });
  },

  dehydrate: function () {
    return this.getState();
  },

  rehydrate: function (state) {
    this.dealers = state.dealers;
  }

});

module.exports = DealerStore;
