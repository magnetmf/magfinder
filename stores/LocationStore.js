'use strict';

var createStore = require('fluxible/utils/createStore');
var _ = require('lodash');

var LocationStore = createStore({
  storeName: 'LocationStore',

  handlers: {
    'GEOLOCATION_SUPPORTED': 'handleGeolocationSupport',
    'GET_CURRENT_LOCATION_START': 'handleCurrentLocationStart',
    'NEW_ZIP_CODE': 'handleNewZipCode',
    'EXPIRE_CURRENT_LOCATION': 'expireCurrentLocation'
  },

  initialize: function (dispatcher) {
    this.geolocationSupport = null;
    this.zipCode = null;
    this.currentLocation = null;
  },

  handleCurrentLocationStart: function(promise){
    this.currentLocation = promise;
    promise.fin(_.bind(function(){
      this.currentLocation.updatedAt = Date.now();
      this.emitChange();
    }, this));
    this.emitChange();
  },

  handleGeolocationSupport: function(isSupported){
    this.geolocationSupport = isSupported;
    this.emitChange();
  },

  handleNewZipCode: function(zipCode){
    this.zipCode = zipCode;
    this.currentLocation = null;
    this.emitChange();
  },

  expireCurrentLocation: function(){
    this.currentLocation = null;
    this.emitChange();
  },

  getState: function(){
    return {
      geolocationSupport: this.geolocationSupport,
      currentLocation: this.currentLocation,
      zipCode: this.zipCode
    };
  },

  dehydrate: function () {
    return this.getState();
  },

  rehydrate: function (state) {
    this.geolocationSupport = state.geolocationSupport;
    this.currentLocation = state.currentLocation;
    this.zipCode = state.zipCode;
  }
});

module.exports = LocationStore;
