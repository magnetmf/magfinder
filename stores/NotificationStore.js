'use strict';

var createStore = require('fluxible/utils/createStore');

var NotificationStore = createStore({
  storeName: 'NotificationStore',

  handlers: {
    'UPDATE_USER_SUCCESS': 'onUserUpdateSuccess',
    'UPDATE_USER_FAILURE': 'onUserUpdateFailure',
    'UPDATE_USER_EMAIL_SUCCESS': 'onUserUpdateSuccess',
    'UPDATE_USER_EMAIL_FAILURE': 'onUserUpdateFailure',
    'UPDATE_USER_PHONE_SUCCESS': 'onUserUpdateSuccess',
    'UPDATE_USER_PHONE_FAILURE': 'onUserUpdateFailure',
    'FAVORITE_MAGAZINE_SUCCESS': 'onFavoriteMagazineSuccess',
    'FAVORITE_MAGAZINE_FAILURE': 'onFavoriteMagazineFailure',
    'REMOVE_FAVORITE_MAGAZINE_SUCCESS': 'onRemoveFavoriteSuccess',
    'REMOVE_FAVORITE_MAGAZINE_FAILURE': 'onRemoveFavoriteFailure',
    'UPDATE_COMMUNICATION_PREFERENCES_SUCCESS': 'onUserUpdateSuccess',
    'UPDATE_COMMUNICATION_PREFERENCES_FAILURE': 'onUserUpdateFailure',
    'RESET_NOTIFICATIONS': 'onReset'
  },

  initialize: function (dispatcher) {
    this.notifications = [];
  },

  onUserUpdateSuccess: function(){
    this.notifications.push({
      type: 'success',
      text: 'Account updated successfully.',
      timeout: 2000
    });
    this.emitChange();
  },

  onUserUpdateFailure: function(err){
    if(err === 401){ return; }

    this.notifications.push({
      type: 'error',
      text: 'There was an error updating account details.',
      timeout: 3000
    });

    this.emitChange();
  },

  onFavoriteMagazineSuccess: function(){
    this.notifications.push({
      type: 'success',
      text: 'Magazine was added to favorites.',
      timeout: 2000
    });
    this.emitChange();
  },

  onFavoriteMagazineFailure: function(err){
    if(err === 401){ return; }

    this.notifications.push({
      type: 'error',
      text: 'There was an error adding magazine to favorites.',
      timeout: 3000
    });

    this.emitChange();
  },

  onRemoveFavoriteSuccess: function(){
    this.notifications.push({
      type: 'success',
      text: 'Magazine was removed from favorites.',
      timeout: 2000
    });
    this.emitChange();
  },

  onRemoveFavoriteFailure: function(err){
    if(err === 401){ return; }

    this.notifications.push({
      type: 'error',
      text: 'There was an error removing magazine from favorites.',
      timeout: 3000
    });

    this.emitChange();
  },

  onReset: function(){
    this.notifications = [];
  },

  getState: function(){
    return this.notifications;
  },

  dehydrate: function () {
    return this.getState();
  },

  rehydrate: function(state) {
    this.notifications = state;
  }
});

module.exports = NotificationStore;
