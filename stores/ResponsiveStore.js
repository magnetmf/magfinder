'use strict';

var createStore = require('fluxible/utils/createStore');
var _ = require('lodash');

var ResponsiveStore = createStore({
  storeName: 'ResponsiveStore',

  handlers: {
    'WINDOW_RESIZE': 'handleWindowResize'
  },

  initialize: function(){
    this.size = this.getWindowSize();
  },

  getWindowSize: function(){
    var size;

    if(typeof window !== 'undefined'){
      size = window.getComputedStyle(document.body,':after')
                   .getPropertyValue('content');
    }

    return this.getSizeAsConstant(size);
  },

  getSizeAsConstant: function(sizeStr){
    if(_.contains(sizeStr, 'tablet')){
      return ResponsiveStore.TABLET;
    }
    if(_.contains(sizeStr, 'desktop')){
      return ResponsiveStore.DESKTOP;
    }
    return ResponsiveStore.MOBILE;
  },

  handleWindowResize: function(){
    var newSize = this.getWindowSize();
    if(this.size !== newSize){
      this.size = newSize;
      this.emitChange();
    }
  },

  getState: function(){
    return { size: this.size };
  },

  dehydrate: function () {
    return this.getState();
  },

  rehydrate: function(state) {
    this.size = state.size;
  }
});

ResponsiveStore.MOBILE = 1;
ResponsiveStore.TABLET = 2;
ResponsiveStore.DESKTOP = 3;

module.exports = ResponsiveStore;
