'use strict';

var _ = require('lodash');
var createStore = require('fluxible/utils/createStore');

var MostPopularStore = createStore({
  storeName: 'MostPopularStore',

  handlers: {
    'TOGGLE_FOOTER': 'handleToggle',
    'SEARCH_POPULAR_MAGAZINES_START': 'updateSearch',
    'NEW_ZIP_CODE': 'resetSearch',
    'EXPIRE_CURRENT_LOCATION': 'resetSearch'
  },

  initialize: function(){
    this.hidden = true;
    this.magazines = null;
  },

  handleToggle: function(){
    this.hidden = !this.hidden;
    this.emitChange();
  },

  updateSearch: function(payload){
    this.magazines = payload.results;
    this.magazines.fin(_.bind(function(){ this.emitChange(); }, this));
    this.emitChange();
  },

  resetSearch: function(){
    this.magazines = null;
    this.emitChange();
  },

  getState: function(){
    return {
      hidden: this.hidden,
      magazines: this.magazines
    };
  },

  dehydrate: function () {
    return this.getState();
  },

  rehydrate: function (state) {
    this.hidden = state.hidden;
    this.magazines = state.magazines;
  }
});

module.exports = MostPopularStore;
