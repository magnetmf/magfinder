'use strict';
var createStore = require('fluxible/utils/createStore');
var _ = require('lodash');

var CategoryStore = createStore({
  storeName: 'CategoryStore',

  handlers: {
    'SEARCH_CATEGORIES_START': 'updateSearch',
    'GET_CURRENT_LOCATION_START': 'resetSearch',
    'NEW_ZIP_CODE': 'resetSearch',
    'EXPIRE_CURRENT_LOCATION': 'resetSearch'
  },

  initialize: function (dispatcher) {
    this.categories = null;
  },

  updateSearch: function(search){
    this.categories = search;
    this.categories.fin(_.bind(function(){ this.emitChange(); }, this));
    this.emitChange();
  },

  resetSearch: function(){
    if(this.categories){
      this.categories = null;
      this.emitChange();
    }
  },

  getState: function(){
    return { categories: this.categories };
  },

  getCategory: function(id){
    return _.find(this.categories, { id: id });
  },

  dehydrate: function () {
    return this.getState();
  },

  rehydrate: function (state) {
    this.categories = state.categories;
  }

});

module.exports = CategoryStore;
