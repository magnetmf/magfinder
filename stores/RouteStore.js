'use strict';

var createStore = require('fluxible/utils/createStore');
var _ = require('lodash');

var RouteStore = createStore({
  storeName: 'RouteStore',

  handlers: {
    'CHANGE_ROUTE_SUCCESS': 'handleNavigate',
    'LOAD_ROUTE_COMPONENT_START': 'handleComponentStartLoad',
    'LOAD_ROUTE_COMPONENT_SUCCESS': 'handleComponentLoad'
  },

  initialize: function(dispatcher) {
    this.currentRoute = null;
    this.currentComponent = null;
  },

  handleNavigate: function(route) {
    if (route === this.currentRoute){
      return;
    }
    this.currentRoute = route;
    this.emitChange();
  },

  handleComponentStartLoad: function(payload){
    /* jshint browser:true */
    /* globals ga:false */

    // google analytics
    if(typeof window !== "undefined" && window.ga &&
    !window.location.host.match('localhost')){
      ga('send', 'pageview', payload.route.url);
    }
  },

  handleComponentLoad: function(component){
    this.currentComponent = component;
  },

  getState: function () {
    return { route: this.currentRoute, component: this.currentComponent };
  },

  dehydrate: function () {
    return { route: this.currentRoute };
  },

  rehydrate: function (state) {
    this.currentRoute = state.route;
  }
});

module.exports =  RouteStore;
