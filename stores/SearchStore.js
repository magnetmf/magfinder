'use strict';
var createStore = require('fluxible/utils/createStore');
var _ = require('lodash');

var SearchStore = createStore({
  storeName: 'SearchStore',

  handlers: {
    'SEARCH_MAGAZINES_START': 'updateSearch',
    'NEW_ZIP_CODE': 'resetSearch',
    'EXPIRE_CURRENT_LOCATION': 'resetSearch'
  },

  initialize: function (dispatcher) {
    this.searchStr = null;
    this.results = null;
  },

  updateSearch: function(payload){
    this.searchStr = payload.searchStr;
    this.results = payload.results;
    this.results.fin(_.bind(function(){ this.emitChange(); }, this));
    this.emitChange();
  },

  resetSearch: function(){
    this.searchStr = null;
    this.results = null;
    this.emitChange();
  },

  getState: function(){
    return {
      searchStr: this.searchStr,
      results: this.results
    };
  },

  getMagazine: function(id){
    if(!this.results || this.results.isPending()){ return; }
    return _.find(this.results.valueOf(), function(magazine){
      return magazine.id === id;
    });
  },

  dehydrate: function () {
    return this.getState();
  },

  rehydrate: function (state) {
    this.searchStr = state.searchStr;
    this.results = state.results;
  }
});

module.exports = SearchStore;
