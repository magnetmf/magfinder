'use strict';

var createStore = require('fluxible/utils/createStore');
var _ = require('lodash');
var CryptoJS = require('crypto-js');
var md5 = require('crypto-js/md5');

function serializeAttrs(attrs){
  return md5(JSON.stringify(attrs)).toString();
}

var FeedbackStore = createStore({
  storeName: 'FeedbackStore',

  handlers: {
    'SUBMIT_FEEDBACK_START': 'updateSearch'
  },

  initialize: function (dispatcher) {
    this.feedback = {};
  },

  updateSearch: function(options){
    var key = serializeAttrs(options.attrs);
    this.feedback[key] = options.search;
    this.feedback[key].fin(_.bind(function(){ this.emitChange(); }, this));
    this.emitChange();
  },

  getFeedback: function(attrs){
    var key = serializeAttrs(attrs);
    return this.feedback[key];
  },

  getState: function(){
    return {
      feedback: this.feedback
    };
  },

  dehydrate: function () {
    return this.getState();
  },

  rehydrate: function (state) {
    this.feedback = state.feedback;
  }
});

module.exports = FeedbackStore;
