'use strict';
var createStore = require('fluxible/utils/createStore');
var _ = require('lodash');

var DirectionsStore = createStore({
  storeName: 'DirectionsStore',

  handlers: {
    'GET_DIRECTIONS_START': 'handleGetDirectionsStart',
    'EXPIRE_CURRENT_LOCATION': 'resetDirections'
  },

  initialize: function(){
    this.directions = null;
  },

  handleGetDirectionsStart: function(promise){
    this.directions = promise;
    promise.fin(_.bind(function(){ this.emitChange(); }, this));
    this.emitChange();
  },

  getState: function(){
    return {
      directions: this.directions
    };
  },

  resetDirections: function(){
    if(this.directions){
      this.directions = null;
      this.emitChange();
    }
  },

  dehydrate: function(){
    return this.getState();
  },

  rehydrate: function(state){
    this.directions = state.directions;
  }

});

module.exports = DirectionsStore;
