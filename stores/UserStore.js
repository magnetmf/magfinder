'use strict';

var createStore = require('fluxible/utils/createStore');
var _ = require('lodash');
var Q = require('q');

var UserStore = createStore({
  storeName: 'UserStore',

  handlers: {
    'CREATE_USER_START': 'onNewUserStart',
    'LOGIN_USER_START': 'onNewUserStart',
    'CREATE_USER_SUCCESS': 'onUserLogin',
    'LOGIN_USER_SUCCESS': 'onUserLogin',
    'USER_LOGIN': 'onUserLogin',
    'SEARCH_USER_EMAIL_START': 'onSearchEmail',
    'SEARCH_USER_PHONE_START': 'onSearchPhone',
    'SEARCH_FAVORITE_MAGAZINES_START': 'onSearchFavorites',
    'UPDATE_USER_FAILURE': 'onUpdateFailure',
    'UPDATE_USER_EMAIL_SUCCESS': 'onEmailUpdate',
    'UPDATE_USER_PHONE_SUCCESS': 'onPhoneUpdate',
    'FAVORITE_MAGAZINE_SUCCESS': 'onNewFavoriteMagazine',
    'REMOVE_FAVORITE_MAGAZINE_SUCCESS': 'onRemoveFavoriteMagazine',
    'SEARCH_COMMUNICATION_PREFERENCES_START': 'onSearchCommunicationPrefs',
    'UPDATE_COMMUNICATION_PREFERENCES_SUCCESS': 'onCommunicationPrefsUpdate',
    'VERIFY_PHONE_START': 'onVerifyPhoneStart',
    'VERIFY_PHONE_SUCCESS': 'onVerifyPhoneSuccess'
  },

  initialize: function(dispatcher) {
    _.each(['newUser', 'user', 'email', 'phone', 'favoriteMagazines',
           'communicationPreferences'],
    function(attr){
      this[attr] = null;
    }, this);
    this.phoneVerification = {};
  },

  onNewUserStart: function(promise){
    this.newUser = promise;
    this.newUser.fin(_.bind(function(){ this.emitChange(); }, this));
    this.emitChange();
  },

  onUserLogin: function(user){
    this.user = user;
    this.emitChange();
  },

  onSearchEmail: function(search){
    this.email = search;
    this.email.fin(_.bind(function(){ this.emitChange(); }, this));
    this.emitChange();
  },

  onSearchPhone: function(search){
    this.phone = search;
    this.phone.fin(_.bind(function(){ this.emitChange(); }, this));
    this.emitChange();
  },

  onSearchFavorites: function(search){
    this.favoriteMagazines = search;
    this.favoriteMagazines.fin(_.bind(function(){ this.emitChange(); }, this));
    this.emitChange();
  },

  onSearchCommunicationPrefs: function(search){
    this.communicationPreferences = search;
    this.communicationPreferences.fin(_.bind(function(){ this.emitChange(); }, this));
    this.emitChange();
  },

  onVerifyPhoneStart: function(attrs){
    this.phoneVerification[attrs.verificationCode] = attrs.search;
    attrs.search.fin(_.bind(function(){ this.emitChange(); }, this));
    this.emitChange();
  },

  onVerifyPhoneSuccess: function(payload){
    // update this.phoneVerification
    var deferred = Q.defer();
    deferred.resolve(payload.result);
    this.phoneVerification[payload.verificationCode] = deferred.promise;

    // update this.phone
    this.onPhoneUpdate(payload.result);
  },

  onEmailUpdate: function(result){
    var deferred = Q.defer();
    deferred.resolve(result);
    this.email = deferred.promise;
    this.emitChange();
  },

  onPhoneUpdate: function(result){
    var deferred = Q.defer();
    deferred.resolve(result);
    this.phone = deferred.promise;
    this.emitChange();
  },

  onCommunicationPrefsUpdate: function(result){
    var deferred = Q.defer();
    deferred.resolve(result);
    this.communicationPreferences = deferred.promise;
    this.emitChange();
  },

  onNewFavoriteMagazine: function(magazine){
    var favs;
    if(this.favoriteMagazines && this.favoriteMagazines.isFulfilled()){
      favs = this.favoriteMagazines.valueOf();
    }
    else {
      favs = [];
    }
    favs.push(magazine);

    var deferred = Q.defer();
    deferred.resolve(favs);
    this.favoriteMagazines = deferred.promise;
    this.emitChange();
  },

  onRemoveFavoriteMagazine: function(magazineID){
    if(!this.favoriteMagazines || !this.favoriteMagazines.isFulfilled()){
      return;
    }

    var favs = this.favoriteMagazines.valueOf();
    _.remove(favs, function(magazine){
      return magazine.id === magazineID;
    });

    var deferred = Q.defer();
    deferred.resolve(favs);
    this.favoriteMagazines = deferred.promise;
    this.emitChange();
  },

  onUpdateFailure: function(){
    // force all listeners to update
    this.emitChange();
  },

  getState: function(){
    return _.pick(this, ['user', 'email', 'phone', 'favoriteMagazines',
                        'communicationPreferences']);
  },

  getNewUser: function(){
    return this.newUser;
  },

  getUser: function(){
    return this.user;
  },

  getEmail: function(){
    return this.email;
  },

  getPhone: function(){
    return this.phone;
  },

  getCommunicationPreferences: function(){
    return this.communicationPreferences;
  },

  getFavoriteMagazines: function(){
    return this.favoriteMagazines;
  },

  getPhoneVerification: function(code){
    return this.phoneVerification[code];
  },

  dehydrate: function () {
    return this.getState();
  },

  rehydrate: function(state) {
    _.each(['newUser', 'user', 'email', 'phone', 'favoriteMagazines',
           'communicationPreferences'],
    function(attr){
      this[attr] = state[attr];
    }, this);
  }
});

module.exports =  UserStore;
