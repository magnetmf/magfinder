--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: cube; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS cube WITH SCHEMA public;


--
-- Name: EXTENSION cube; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION cube IS 'data type for multidimensional cubes';


--
-- Name: earthdistance; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS earthdistance WITH SCHEMA public;


--
-- Name: EXTENSION earthdistance; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION earthdistance IS 'calculate great-circle distances on the surface of the Earth';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: dim_dealer; Type: TABLE; Schema: public; Owner: magnet_dbo; Tablespace:
--

CREATE TABLE dim_dealer (
    dealerid integer NOT NULL,
    chainhq character varying(255) NOT NULL,
    chaindiv character varying(255) NOT NULL,
    chainstorenumber character varying(50) NOT NULL,
    dealername character varying(100) NOT NULL,
    address character varying(100) NOT NULL,
    city character varying(50) NOT NULL,
    state character(2) NOT NULL,
    postalcode character varying(10) NOT NULL,
    latitude numeric(18,5),
    longitude numeric(18,5),
    phoneareacode character varying(3),
    phonenumber character varying(7)
);

--
-- Name: dim_title; Type: TABLE; Schema: public; Owner: magnet_dbo; Tablespace:
--

CREATE TABLE dim_title (
    titleid integer NOT NULL,
    bipadname character varying(255),
    bipadtitle character varying(255),
    maxonsale date,
    drawvalue integer,
    coverimageurl character varying(1024),
    publisher character varying(50),
    bipadnumber integer,
    issuetitle character varying(100),
    categoryid integer,
    category character varying(255),
    titlecategory_popindex smallint,
    coverpriceus numeric(10,2),
    coverpriceca numeric(10,2),
    title_description character varying(255),
    is_new_title character(1)
);


--
-- Name: fact_titledealerxr; Type: TABLE; Schema: public; Owner: magnet_dbo; Tablespace:
--

CREATE TABLE fact_titledealerxr (
    dealerid integer NOT NULL,
    titleid integer NOT NULL,
    titlestore_popindex smallint
);

--
-- Name: fact_titledealerxr_pkey; Type: CONSTRAINT; Schema: public; Owner: magnet_dbo; Tablespace:
--

ALTER TABLE ONLY fact_titledealerxr
    ADD CONSTRAINT fact_titledealerxr_pkey PRIMARY KEY (dealerid, titleid);


--
-- Name: location_index; Type: INDEX; Schema: public; Owner: magnet_dbo; Tablespace:
--

CREATE INDEX location_index ON dim_dealer USING gist (ll_to_earth((latitude)::double precision, (longitude)::double precision));


--
-- PostgreSQL database dump complete
--
