'use strict';

// load env variables from .env file
var path = require('path');
require('dotenv').load({ path: path.join(__dirname, '.env') });

// load JSX files and compile on demand
require('node-jsx').install({ extension: '.jsx' });

// ignore required .less and .css files
require.extensions['.less'] = function(){ };
require.extensions['.css'] = function(){ };

var fs = require('fs');
var _ = require('lodash');
var express = require('express');
var navigateAction = require('flux-router-component').navigateAction;
var React = require('react');
var app = require('./fluxible-app');
var layout = fs.readFileSync('./layout.html', {encoding: 'utf-8'});
var config = require('./configs/public');
var privateConfig = require('./configs/private');

var server = express();
server.use('/', express.static(__dirname + '/public/root'));
server.use('/public', express.static(__dirname + '/build'));
server.use('/public', express.static(__dirname + '/public'));

var bodyParser = require('body-parser');
server.use(bodyParser.json());

var session = require('express-session');
server.use(session({ secret: privateConfig.sessionSecret }));

var passport = require('./api/passport');
server.use(passport.initialize());
server.use(passport.session());

var expressState = require('express-state');
expressState.extend(server);

server.use('/api', require('./api/api'));

server.get('/auth/facebook', passport.authenticate('facebook',
                                                   { display: 'popup' }));

server.get('/auth/facebook/callback', passport.authenticate('facebook',
  { successRedirect: '/account', failureRedirect: '/signup' }));

server.get('/auth/twitter', passport.authenticate('twitter'));

server.get('/auth/twitter/callback', passport.authenticate('twitter',
  { failureRedirect: '/signup' }), function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/account');
  });

server.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

server.use(function(req, res, next) {
  var context = app.createContext();

  if(req.user){
    context.getActionContext().dispatch('USER_LOGIN', req.user);
  }

  context.getActionContext().executeAction(navigateAction, {
    url: req.originalUrl
  }, function (err) {
    if (err) {
      if (err.status && err.status === 404) {
        next();
      } else {
        next(err);
      }
      return;
    }

    res.expose(config, 'config');
    res.expose(app.dehydrate(context), 'App');

    var AppComponent = app.getAppComponent();
    var html = layout
      .replace('APP', React.renderToString(new AppComponent({
        context: context.getComponentContext()
      })))
      .replace('GMAPS_API_KEY', config.googleApiKey)
      .replace('UA-XXXX-Y', config.webPropertyID)
      .replace('STATE', res.locals.state);

    res.send(html);
  });
});

module.exports = server;

if(require.main === module){
  // module was called directly (ie not required by another module)

  // start listening on http port
  server.listen(config.port);
  console.log('Listening on port ' + config.port);

  // send new magazine notifications
  require('./tasks/sendNewMagazineNotifications').run({
    cronTime: privateConfig.newMagNotificationTime
  });
}
