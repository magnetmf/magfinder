FROM node:0.12.0
ADD . /app
WORKDIR /app
RUN ["npm", "install"]
RUN ["npm", "run", "webpack"]
CMD ["node", "server.js"]
EXPOSE 80
